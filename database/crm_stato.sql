/*
 * Record structure for table 'user_level'
 */
INSERT INTO user_role
    (role_id, role_name, role_desc)
VALUES
    (0,'regular','non registered user'),
    (1,'registered','registered user'),
    (2,'administrator','the boss of the platform');
     
/*
 * Record structure for table 'user'
 */
INSERT INTO user_profile
    (user_id, user_role, user_username, user_password, user_email)
VALUES
    (0,2,'realnot','0000','realnot@crm.com');


/*
 *  Record structure for table 'customer'
 */
INSERT INTO customer
     (customer_id, customer_name, customer_email, customer_created_by)
VALUES
     (0,'Accenture','info@accenture.com',0),
     (1,'Cisco Systems','info@cisco.com',0);
     
/*
 * Record structure for table 'note'
 */
INSERT INTO note
     (note_id, note_topic, note_desc, note_created_by, note_refered_to)
VALUES
     (0,'consulting request','note description',0,1);

/*
 * Record structure for table 'taxonomy'
 */
INSERT INTO taxonomy
     (tag_id, tag_name)
VALUES
     (0,'networking'),
     (1,'web development'),
     (2,'business consulting'),
     (3,'internet of things');

/*
 * Record structure for table 'meeting'
 */
INSERT INTO meeting
     (meeting_id, meeting_topic, meeting_desc, meeting_created_by,
     meeting_refered_to)
VALUES
     (0,'first meeting','meeting to know the company',0,1);
     
/*
 * Record structure for table 'consulting'
 */
INSERT INTO consulting_service
     (service_id, service_name, service_desc)
VALUES
     (0,'strategy','Tailored solutions that deliver results and achieve...'),
     (1,'organization','Ensuring the entire organization is aligned and...'),
     (2,'digital','Deliver on core strategy, delight customers and...'),
     (3,'information technology','Realizing the full potential of IT...');

/*
 * Record structure for 'business_proposal'
 */
INSERT INTO business_proposal
     (proposal_id, proposal_name, proposal_desc, consulting_service)
VALUES
     (0,'product marketing','Your marketing proposal represents...',0);

/*
 * Record structure for 'customer_to_tag'
 */
INSERT INTO customer_to_tag
     (customer_id, tag_id)
VALUES
     (0,2), (0,1), (1,0), (1,3);

/*
 * Recotrd structure for 'customer_to_tag'
 */
INSERT INTO customer_to_business_proposal
     (customer_id, proposal_id)
VALUES
     (0,0);
