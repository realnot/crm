/*
 * @author: Mauro Crociara
 * @version: 0.302-15
 *
 * To execute this file run: 
 *
 * psql -U realnot -d postgres -f ~/Workspace/crm/database/crm_schema.sql
 */
DROP DATABASE crm;
CREATE DATABASE crm WITH OWNER realnot;
GRANT ALL PRIVILEGES ON DATABASE crm TO realnot;
\c crm

/*
 * Table structure for table 'user_role'
 */
CREATE TABLE user_role (
    role_id SERIAL PRIMARY KEY,
    role_name VARCHAR(32) UNIQUE NOT NULL,
    role_desc VARCHAR(255) NOT NULL
); 

/*
 * Table structure for table 'user'
 */
CREATE TABLE user_profile (
    user_id SERIAL PRIMARY KEY,
    user_role SMALLINT NOT NULL,
    user_username VARCHAR(32) UNIQUE NOT NULL,
    user_password VARCHAR(32) NOT NULL,
    user_email VARCHAR(128) UNIQUE NOT NULL,
    user_name VARCHAR(32),
    user_surname VARCHAR(32),
    user_display_name VARCHAR(64),
    user_address VARCHAR(128),
    user_birthdate VARCHAR(10),
    user_created_on TIMESTAMP with time zone NOT NULL DEFAULT current_timestamp,
    user_last_login TIMESTAMP with time zone,
    CONSTRAINT user_role_id_fkey FOREIGN KEY (user_role)
        REFERENCES user_role (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 *  Table structure for table 'customer'
 */
CREATE TABLE customer (
    customer_id SERIAL PRIMARY KEY,
    customer_name VARCHAR(64) NOT NULL,
    customer_email VARCHAR(128) UNIQUE NOT NULL,
    customer_created_on TIMESTAMP with time zone NOT NULL DEFAULT current_timestamp, 
    customer_created_by INTEGER NOT NULL,
    CONSTRAINT customer_created_by_id_fkey FOREIGN KEY (customer_created_by)
       REFERENCES user_profile (user_id) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Table structure for table 'note'
 */
CREATE TABLE note (
    note_id SERIAL PRIMARY KEY,
    note_topic VARCHAR(255) NOT NULL,
    note_desc VARCHAR(4096) NOT NULL,
    note_created_on TIMESTAMP with time zone NOT NULL DEFAULT current_timestamp,
    note_created_by INTEGER NOT NULL,
    note_refered_to INTEGER NOT NULL,
    CONSTRAINT note_writed_by_id_fkey FOREIGN KEY (NOTE_created_by)
        REFERENCES user_profile (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT note_refered_to_id_fkey FOREIGN KEY (note_refered_to)
        REFERENCES customer (customer_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Table structure for table 'taxonomy'
 */
CREATE TABLE taxonomy (
    tag_id SERIAL PRIMARY KEY,
    tag_name VARCHAR(32) UNIQUE NOT NULL,
    tag_desc VARCHAR(255)
);

/*
 * Table structure for table 'meeting'
 */
CREATE TABLE meeting (
    meeting_id SERIAL PRIMARY KEY,
    meeting_topic VARCHAR(255) NOT NULL,
    meeting_desc VARCHAR(4096) NOT NULL,
    meeting_created_on TIMESTAMP with time zone NOT NULL DEFAULT current_timestamp,
    meeting_created_by INTEGER NOT NULL,
    meeting_refered_to INTEGER NOT NULL,
    CONSTRAINT meeting_created_by_id_fkey FOREIGN KEY (meeting_created_by)
        REFERENCES user_profile (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT meeting_refered_to_id_fkey FOREIGN KEY (meeting_refered_to)
        REFERENCES customer (customer_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Table structure for table 'consulting'
 */
CREATE TABLE consulting_service (
    service_id SERIAL PRIMARY KEY,
    service_name VARCHAR(255) UNIQUE NOT NULL,
    service_desc VARCHAR(1024)
);

/*
 * Table structure for 'business_proposal'
 */
CREATE TABLE business_proposal (
    proposal_id SERIAL PRIMARY KEY,
    proposal_name VARCHAR(255) UNIQUE NOT NULL,
    proposal_desc VARCHAR(1024) NOT NULL,
    consulting_service INTEGER,
    CONSTRAINT consulting_service_id_fkey FOREIGN KEY (consulting_service)
        REFERENCES consulting_service (service_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Table structure for 'customer_to_tag'
 *
 * Note: N to M table that map customer to tag (and vice versa)
 */
CREATE TABLE customer_to_tag (
    customer_id INTEGER NOT NULL,
    tag_id INTEGER NOT NULL,
    PRIMARY KEY (customer_id, tag_id),
    CONSTRAINT customer_to_tag_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customer (customer_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT customer_to_tag_tag_id_fkey FOREIGN KEY (tag_id)
        REFERENCES taxonomy (tag_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Table structure for 'customer_to_tag'
 *
 * Note: N to M table that map customer to business proposal (and vice versa)
 */
CREATE TABLE customer_to_business_proposal (
    customer_id INTEGER NOT NULL,
    proposal_id INTEGER NOT NULL,
    PRIMARY KEY (customer_id, proposal_id),
    CONSTRAINT customer_to_bp_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customer (customer_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT customer_to_bp_proposal_id_fkey FOREIGN KEY (proposal_id)
        REFERENCES business_proposal (proposal_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

/*
 * Populates the DB after creating the tables
 */
\i /home/realnot/Workspace/crm/database/crm_stato.sql
