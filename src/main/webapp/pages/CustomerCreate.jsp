<%-- 
    Document   : AddCustomer
    Created on : May 18, 2015, 10:20:39 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="add-customer" class="grid_8 content">
                    <h3>Add a new customer</h3>
                    <form action="CustomerServlet" method="POST" class="crm-form crm-form-stacked">
                        <table class="">
                            <tr>
                                <td class="">
                                    <fieldset>
                                    <legend>Customer Details</legend>
                                    <div>
                                        <label for="customer-name">Customer Name</label>
                                        <input id="customer-name" name="customer-name" type="text" required>
                                    </div>
                                    <div>
                                        <label for="customer-email">Customer E-Mail</label>
                                        <input id="customer-email" name="customer-email" type="email" required>
                                    </div>
                                    <div>
                                        <label for="customer-type">Customer Type</label>
                                            <select id="assigned-to" name="customer-type">
                                                <c:forEach items="${types}" var="t">
                                                    <option value='<c:out value="${t.customerTypeID}"></c:out>'>
                                                        <c:out value="${t.customerTypeName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    <div>
                                        <label for="product-category">Customer Product Category</label>
                                            <select id="assigned-to" name="product-category">
                                                <c:forEach items="${categories}" var="c">
                                                    <option value='<c:out value="${c.productID}"></c:out>'>
                                                        <c:out value="${c.productName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    <div>
                                        <label for="assigned-to">Consultant</label>
                                            <select id="assigned-to" name="assigned-to">
                                                <c:forEach items="${users}" var="u">
                                                    <option value='<c:out value="${u.userID}"></c:out>'>
                                                        <c:out value="${u.username}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    </fieldset>
                                </td>
                                <td class="">
                                    <fieldset>
                                        <legend>Company Agent</legend>
                                        <div>
                                            <label for="agent-name">Name</label>
                                            <input id="agent-name" name="agent-name" type="text" required>
                                        </div>
                                        <div>
                                            <label for="agent-surname">Surname</label>
                                            <input id="agent-surname" name="agent-surname" type="text" required>
                                        </div>
                                        <div>
                                            <label for="agent-address">Address</label>
                                            <input id="agent-address" name="agent-address" type="text" required>
                                        </div>
                                        <div>
                                            <label for="agent-phone">Phone</label>
                                            <input id="agent-phone" name="agent-phone" type="tel" required>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td class="">
                                    <fieldset>
                                        <legend>Business Proposals</legend>
                                        <div>
                                            <label for="proposals">Proposal name</label>
                                            <select id="proposals" name="proposalID" size="3">
                                                <c:forEach items="${proposals}" var="p">
                                                    <option value='<c:out value="${p.proposalID}"></c:out>'>
                                                        <c:out value="${p.proposalName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </fieldset>
                                </td>
                                <td class="">
                                    <fieldset>
                                        <legend>Consulting Services</legend>
                                        <div>
                                            <label for="services">Service name</label>
                                            <select id="services" name="serviceID" size="3">
                                                <c:forEach items="${services}" var="s">
                                                    <option value='<c:out value="${s.serviceID}"></c:out>'>
                                                        <c:out value="${s.serviceName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <input type="hidden" name="action" value="save">
                                        <input type="hidden" name="created-by" value="<c:out value="${login.userID}" />">
                                        <input type="submit" class="pure-button pure-button-primary" value="Save Customer">
                                        <input type="reset" value="Clear data">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 