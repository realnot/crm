<%-- 
    Document   : CustomerUpdate
    Created on : Jun 24, 2015, 11:19:04 AM
    Author     : realnot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="add-customer" class="grid_8 content">
                    <h3>Add a new customer</h3>
                    <form action="CustomerServlet" method="POST" class="crm-form crm-form-stacked">
                        <table class="">
                            <tr>
                                <td class="">
                                    <fieldset>
                                    <legend>Customer Details</legend>
                                    <div>
                                        <label for="customer-name">Customer Name</label>
                                        <input id="customer-name" name="customer-name" type="text" value="<c:out value="${details.customerName}"></c:out>" required>
                                    </div>
                                    <div>
                                        <label for="customer-email">Customer E-Mail</label>
                                        <input id="customer-email" name="customer-email" type="email" value="<c:out value="${details.customerEmail}"></c:out>" required>
                                    </div>
                                    <div>
                                        <label for="customer-type">Customer Type</label>
                                            <select id="customer-type" name="customer-type">
                                                <option selected="selected" value="<c:out value="${details.customerType}"></c:out>">
                                                    <c:out value="${details.stringCustomerType}"></c:out>
                                                </option>
                                                <c:forEach items="${types}" var="t">
                                                    <option value='<c:out value="${t.customerTypeID}"></c:out>'>
                                                        <c:out value="${t.customerTypeName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    <div>
                                        <label for="product-category">Customer Product Category</label>
                                            <select id="product-category" name="product-category">
                                                <option selected="selected" value="<c:out value="${details.customerCategory}"></c:out>">
                                                    <c:out value="${details.stringCustomerCategory}"></c:out>
                                                </option>
                                                <c:forEach items="${categories}" var="c">
                                                    <option value='<c:out value="${c.productID}"></c:out>'>
                                                        <c:out value="${c.productName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    <div>
                                        <label for="assigned-to">Consultant</label>
                                            <select id="assigned-to" name="assigned-to">
                                                <option selected="selected" value="<c:out value="${details.customerAssignedTo}"></c:out>">
                                                    <c:out value="${details.stringCustomerAssignedTo}"></c:out>
                                                </option>
                                                <c:forEach items="${users}" var="u">
                                                    <option value='<c:out value="${u.userID}"></c:out>'>
                                                        <c:out value="${u.displayName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                    </div>
                                    </fieldset>
                                </td>
                                <td class="">
                                    <fieldset>
                                        <legend>Company Agent</legend>
                                        <div>
                                            <label for="agent-name">Name</label>
                                            <input id="agent-name" name="agent-name" type="text" value="<c:out value="${details.customerContactName}"></c:out>" required>
                                        </div>
                                        <div>
                                            <label for="agent-surname">Surname</label>
                                            <input id="agent-surname" name="agent-surname" type="text" value="<c:out value="${details.customerContactSurname}"></c:out>" required>
                                        </div>
                                        <div>
                                            <label for="agent-address">Address</label>
                                            <input id="agent-address" name="agent-address" type="text" value="<c:out value="${details.customerContactAddress}"></c:out>" required>
                                        </div>
                                        <div>
                                            <label for="agent-phone">Phone</label>
                                            <input id="agent-phone" name="agent-phone" type="tel" value="<c:out value="${details.customerContactPhone}"></c:out>" required>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td class="">
                                    <fieldset>
                                        <legend>Business Proposals</legend>
                                        <div>
                                            <label for="proposals">Proposal name</label>
                                            <select id="proposals" name="proposalID" size="3">
                                                <option selected="selected" value="<c:out value="${ctbp.proposalID}"></c:out>">
                                                    <c:out value="${ctbp.proposalName}"></c:out>
                                                </option>
                                                <c:forEach items="${proposals}" var="p">
                                                    <option value='<c:out value="${p.proposalID}"></c:out>'>
                                                        <c:out value="${p.proposalName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </fieldset>
                                </td>
                                <td class="">
                                    <fieldset>
                                        <legend>Consulting Services</legend>
                                        <div>
                                            <label for="services">Service name</label>
                                            <select id="services" name="serviceID" size="3">
                                                <option selected="selected" value="<c:out value="${ctcs.serviceID}"></c:out>">
                                                    <c:out value="${ctcs.serviceName}"></c:out>
                                                </option>
                                                <c:forEach items="${services}" var="s">
                                                    <option value='<c:out value="${s.serviceID}"></c:out>'>
                                                        <c:out value="${s.serviceName}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <input type="hidden" name="action" value="update">
                                        <input type="hidden" name="created-by" value="<c:out value="${login.userID}" />">
                                        <input type="hidden" name="customerID" value="<c:out value="${details.customerID}" />">
                                        <input type="hidden" name="ctbpID" value="<c:out value="${ctbp.CTBPID}" />">
                                        <input type="hidden" name="ctcsID" value="<c:out value="${ctcs.CTCSID}" />">
                                        <input type="submit" class="pure-button pure-button-primary" value="Update Customer">
                                        <input type="reset" value="Clear data">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 