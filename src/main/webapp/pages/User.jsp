<%-- 
    Document   : User
    Created on : May 27, 2015, 9:02:13 PM
    Author     : realnot
--%>

<%@page contentType="text/html" %>
<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="userlist" class="grid_6 content entry-list">
                    <h3>Recent Users</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Username</th>
                                <th>Role</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${usersList}" var="u">
                        <tr onClick="location.href='UserServlet?page=details&user_id=${u.userID}'">
                            <td><c:out value="${u.userID}" /></td>
                            <td><c:out value="${u.username}" /></td>
                            <td><c:out value="${u.roleName}" /></td>
                        </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='UserServlet?page=create'" class="button add-customer" /></td>
                                <td><button onClick="location.href='UserServlet?page=find'" class="button find-entry" /></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html>
