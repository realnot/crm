<%-- 
    Document   : UserFind
    Created on : May 29, 2015, 2:38:55 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="find-user" class="grid_4 content">
                    <h3>Find an user</h3>
                    <form action="UserServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="user-code">User Code</label>
                            <input id="user-code" name="userCode" type="text" required>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="find">
                            <input type="submit" class="pure-button pure-button-primary" value="Find User">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 