<%-- 
    Document   : BusinessProposalUpdate
    Created on : Jun 11, 2015, 12:48:19 AM
    Author     : realnot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="update-business-proposal" class="grid_8 content">
                    <h3>Update a business proposal</h3>
                    <form action="BusinessProposalServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="proposal-name">Name</label>
                            <input id="proposal-name" name="proposalName" type="text" value="${details.proposalName}" required>
                        </div>
                        <div>
                            <label for="proposal-desc">Description</label>
                            <textarea id="proposal-desc" name="proposalDesc" rows="4" required>${details.proposalDesc}</textarea>
                        </div>
                        <div>
                            <label for="consulting-service">Consulting Service</label>
                            <select id="consulting-service" name="serviceID" required>
                                <c:forEach items="${list}" var="l">
                                    <option value='<c:out value="${l.serviceID}"></c:out>'>
                                        <c:out value="${l.serviceName}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="update">
                            <input type="hidden" name="proposalID" value="${details.proposalID}">
                            <input type="submit" class="pure-button pure-button-primary" value="Update Business Proposal">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

