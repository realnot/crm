<%-- 
    Document   : UserDetails
    Created on : May 29, 2015, 2:35:24 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="user-details" class="grid_5 content">
                    <h3>User Details</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>Code</td>
                                <td><c:out value="${userDetails.userID}" /></td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td><c:out value="${userDetails.username}" /></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><c:out value="${userDetails.email}" /></td>
                            </tr>
                            <tr>
                                <td>Role</td>
                                <td><c:out value="${userDetails.roleName}" /></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><c:out value="${userDetails.name}" /></td>
                            </tr>
                            <tr>
                                <td>Surname</td>
                                <td><c:out value="${userDetails.surname}" /></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><c:out value="${userDetails.address}" /></td>
                            </tr>
                            <tr>
                                <td>Birth Date</td>
                                <td><c:out value="${userDetails.birthdate}" /></td>
                            </tr>
                            <tr>
                                <td>Last Login</td>
                                <td><c:out value="${userDetails.lastLogin}" /></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='UserServlet?page=update&user_id=${userDetails.userID}'" class="button-small update-entry" /> </td>
                                <td>
                                    <form action="UserServlet" method="POST">
                                        <input type="hidden" name="action" value="delete" />
                                        <input type="hidden" name="userID" value="${userDetails.userID}" />
                                        <input type="submit" class="button-submit-small del-entry" />
                                    </form>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

