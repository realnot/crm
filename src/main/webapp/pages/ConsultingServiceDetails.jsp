<%-- 
    Document   : ConsultingServiceDetail
    Created on : May 27, 2015, 3:20:03 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="consulting-service-details" class="grid_12 content">
                    <h3>Consulting Service Details</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>Code</td>
                                <td><c:out value="${details.serviceID}" /></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><c:out value="${details.serviceName}" /></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><c:out value="${details.serviceDesc}" /></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='ConsultingServiceServlet?page=update&service_id=${details.serviceID}'" class="button-small update-entry" /> </td>
                                <td>
                                    <form action="ConsultingServiceServlet" method="POST">
                                        <input type="hidden" name="action" value="delete" />
                                        <input type="hidden" name="serviceID" value="${details.serviceID}" />
                                        <input type="submit" class="button-submit-small del-entry" />
                                    </form>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

