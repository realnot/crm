<%-- 
    Document   : BusinessProposalFind
    Created on : May 26, 2015, 9:42:28 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="find-proposal" class="grid_4 content">
                    <h3>Find a Business Proposal</h3>
                    <form action="BusinessProposalServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="proposal-id">Business Proposal Code</label>
                            <input id="proposal-id" name="proposalID" type="text" required>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="find">
                            <input type="submit" class="pure-button pure-button-primary" value="Find a Business Proposal">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 
