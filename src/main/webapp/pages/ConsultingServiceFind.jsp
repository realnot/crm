<%-- 
    Document   : ConsultingServiceFind
    Created on : May 27, 2015, 3:19:50 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="find-service" class="grid_4 content">
                    <h3>Find a Consulting Service</h3>
                    <form action="ConsultingServiceServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="service-id">Consulting Service Code</label>
                            <input id="service-id" name="serviceID" type="text" required>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="find">
                            <input type="submit" class="pure-button pure-button-primary" value="Find Consulting Service">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

