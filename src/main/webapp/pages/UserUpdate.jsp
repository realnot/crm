<%-- 
    Document   : UserUpdate
    Created on : May 28, 2015, 5:00:43 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="add-customer" class="grid_4 content">
                    <h3>Update an user</h3>
                    <form action="UserServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="username">Username</label>
                            <input id="username" name="username" type="text" value="<c:out value="${userDetails.username}"></c:out>" required>
                        </div>
                        <div>
                            <label for="password">Password</label>
                            <input id="password" name="password" type="text" value="<c:out value="${userDetails.password}"></c:out>" required>
                        </div>
                        <div>
                            <label for="email">E-Mail</label>
                            <input id="email" name="email" type="email" value="<c:out value="${userDetails.email}"></c:out>" required>
                        </div>
                        <div>
                            <label for="role">Role</label>
                            <select id="role" name="roleID" required>
                                <option selected="selected" value="<c:out value="${userDetails.roleID}"></c:out>">
                                    <c:out value="${userDetails.roleName}"></c:out>
                                </option>
                                <c:forEach items="${rolesList}" var="r">
                                    <option value='<c:out value="${r.roleID}"></c:out>'>
                                        <c:out value="${r.roleName}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                        <fieldset/>
                        <fieldset>
                            <legend>Optional Fields</legend>
                            <div>
                                <label for="name">Name</label>
                                <input id="name" name="name" type="text" value="<c:out value="${userDetails.name}"></c:out>">
                            </div>
                            <div>
                                <label for="surname">Surname</label>
                                <input id="surname" name="surname" type="text" value="<c:out value="${userDetails.surname}"></c:out>">
                            </div>
                            <div>
                                <label for="address">Address</label>
                                <input id="address" name="address" type="text" value="<c:out value="${userDetails.address}"></c:out>">
                            </div>
                            <div>
                                <label for="birthdate">Birth date</label>
                                <input id="birthdate" name="birthdate" type="text" value="<c:out value="${userDetails.birthdate}"></c:out>">
                            </div>
                        </fieldset>
                        <div>
                            <input type="hidden" name="lastlogin" value="<c:out value="${userDetails.lastLogin}"></c:out>">
                            <input type="hidden" name="action" value="update">
                            <input type="hidden" name="userID" value="${userDetails.userID}">
                            <input type="submit" class="pure-button pure-button-primary" value="Update User">
                            <input type="reset" value="Clear data">
                        </div>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
                
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 
