<%-- 
    Document   : BusinessProposal
    Created on : May 20, 2015, 1:22:00 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="business-proposals-list" class="grid_12 content entry-list">
                    <h3>Business Proposals</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Proposal</th>
                                <th>Description</th>
                                <th>Service</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${list}" var="l">
                            <tr onClick="location.href='BusinessProposalServlet?page=details&proposal_id=${l.proposalID}'">
                                <td><c:out value="${l.proposalID}" /></td>
                                <td><c:out value="${l.proposalName}" /></td>
                                <td><c:out value="${l.proposalDesc}" /></td>
                                <td><c:out value="${l.serviceName}" /></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='BusinessProposalServlet?page=create'" class="button add-bp" /></td>
                                <td><button onClick="location.href='BusinessProposalServlet?page=find'" class="button find-entry" /></td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </div>
                <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 
