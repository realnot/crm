<%-- 
    Document   : BusinessProposalDetail
    Created on : May 26, 2015, 9:42:53 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="meeting-details" class="grid_12 content">
                    <h3>Business Proposal Details</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>Code</td>
                                <td><c:out value="${details.proposalID}" /></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><c:out value="${details.proposalName}" /></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><c:out value="${details.proposalDesc}" /></td>
                            </tr>
                            <tr>
                                <td>Consulting Service</td>
                                <td><c:out value="${details.serviceName}" /></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='BusinessProposalServlet?page=update&proposal_id=${details.proposalID}'" class="button-small update-entry" /> </td>
                                <td>
                                    <form action="BusinessProposalServlet" method="POST">
                                        <input type="hidden" name="action" value="delete" />
                                        <input type="hidden" name="proposalID" value="${details.proposalID}" />
                                        <input type="submit" class="button-submit-small del-entry" />
                                    </form>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

