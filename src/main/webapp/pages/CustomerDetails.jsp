<%-- 
    Document   : CustomerDetail
    Created on : May 22, 2015, 3:42:40 PM
    Author     : realnot
--%>


<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="details" class="grid_12 content">
                    <h3>Customer Details</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created by</th>
                                <th>Assigned to</th>
                                <th>Created on</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><c:out value="${details.customerID}" /></td>
                                <td><c:out value="${details.customerName}" /></td>
                                <td><c:out value="${details.customerEmail}" /></td>
                                <td><c:out value="${details.stringCustomerCreatedBy}" /></td>
                                <td><c:out value="${details.stringCustomerAssignedTo}" /></td>
                                <td><c:out value="${details.customerCreatedOn}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>Company agents</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Address</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><c:out value="${details.customerContactName}" /></td>
                                <td><c:out value="${details.customerContactSurname}" /></td>
                                <td><c:out value="${details.customerContactAddress}" /></td>
                                <td><c:out value="${details.customerContactPhone}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>Business Proposals</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${ctbp}" var="ctbp">
                            <tr>
                                <td><c:out value="${ctbp.proposalID}" /></td>
                                <td><c:out value="${ctbp.proposalName}" /></td>
                                <td><c:out value="${ctbp.proposalDesc}" /></td>
                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h3>Consulting services purchased</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${ctcs}" var="ctcs">
                            <tr>
                                <td><c:out value="${ctcs.serviceID}" /></td>
                                <td><c:out value="${ctcs.serviceName}" /></td>
                                <td><c:out value="${ctcs.serviceDesc}" /></td>
                            </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='CustomerServlet?page=update&customer_id=${details.customerID}'" class="button-small update-entry" /> </td>
                                <td>
                                    <form action="CustomerServlet" method="POST">
                                        <input type="hidden" name="action" value="delete" />
                                        <input type="hidden" name="customerID" value="${details.customerID}" />
                                        <input type="submit" class="button-submit-small del-entry" />
                                    </form>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 
