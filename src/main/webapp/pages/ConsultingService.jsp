<%-- 
    Document   : ConsultingService
    Created on : May 20, 2015, 4:42:41 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="customer-list" class="grid_12 content entry-list">
                    <h3>Consulting Services</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Service</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${list}" var="l">
                            <tr onClick="location.href='ConsultingServiceServlet?page=details&service_id=${l.serviceID}'">
                                <td><c:out value="${l.serviceID}" /></td>
                                <td><c:out value="${l.serviceName}" /></td>
                                <td><c:out value="${l.serviceDesc}" /></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='ConsultingServiceServlet?page=create'" class="button add-cs" /></td>
                                <td><button onClick="location.href='ConsultingServiceServlet?page=find'" class="button find-entry" /></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </div>
                <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

