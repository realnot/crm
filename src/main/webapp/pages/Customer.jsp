<%-- 
    Document   : Customer
    Created on : May 6, 2015, 3:44:20 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="customer-list" class="grid_7 content entry-list">
                    <h3>Recent Customers</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${list}" var="l">
                        <a href="CustomerDetails.jsp">
                            <tr onClick="location.href='CustomerServlet?page=details&customer_id=${l.customerID}'">
                                <td><c:out value="${l.customerID}" /></td>
                                <td><c:out value="${l.customerName}" /></td>
                                <td><c:out value="${l.customerEmail}" /></td>
                            </tr>
                        </a>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><button onClick="location.href='CustomerServlet?page=create'" class="button add-customer" /></td>
                                <td><button onClick="location.href='CustomerServlet?page=find'" class="button find-entry" /></td>
                                <td colspan="1">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </div>
                <div class='clear'>&nbsp;</div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 