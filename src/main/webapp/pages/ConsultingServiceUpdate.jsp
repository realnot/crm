<%-- 
    Document   : ConsultingServiceUpdate
    Created on : Jun 10, 2015, 9:45:00 PM
    Author     : realnot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="update-consulting-service" class="grid_4 content">
                    <h3>Update a consulting service</h3>
                    <form action="ConsultingServiceServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="service-name">Name</label>
                            <input id="service-name" name="serviceName" type="text" value="${details.serviceName}" required>
                        </div>
                        <div>
                            <label for="service-desc">Description</label>
                            <textarea id="service-desc" name="serviceDesc"  rows="4" required>${details.serviceDesc}</textarea>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="update">
                            <input type="hidden" name="serviceID" value="${details.serviceID}">
                            <input type="submit" class="pure-button pure-button-primary" value="Update Consulting Service">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 

