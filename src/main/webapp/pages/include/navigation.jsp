<%-- 
    Document   : navigation
    Created on : May 27, 2015, 5:44:42 PM
    Author     : realnot
--%>

<div class="menu custom-restricted-width">
    <span class="menu-heading">Commercial Menu</span>
    <ul class="menu-list">
        <li class="menu-item"><a href="<%=response.encodeURL("Dashboard") %>" class="hvr-sweep-to-right">Dashboard</a></li>
        <li class="menu-item"><a href="<%=response.encodeURL("CustomerServlet?page=list") %>" class="hvr-sweep-to-right">Customers</a></li>
        <li class="menu-item"><a href="<%=response.encodeURL("MeetingServlet?page=list") %>" class="hvr-sweep-to-right">Meetings</a></li>
        <li class="menu-item"><a href="<%=response.encodeURL("BusinessProposalServlet?page=list") %>" class="hvr-sweep-to-right">Business Proposals</a></li>
        <li class="menu-item"><a href="<%=response.encodeURL("ConsultingServiceServlet?page=list") %>" class="hvr-sweep-to-right">Consulting Services</a></li>
        <li class="menu-heading">Administrator Services</li>
        <li class="menu-item"><a href="<%=response.encodeURL("UserServlet?page=list") %>" class="hvr-sweep-to-right">Users</a></li>
        <li class="menu-item"><a href="<%=response.encodeURL("LogServlet?page=list") %>" class="hvr-sweep-to-right">Log</a></li>
    </ul>
</div>
