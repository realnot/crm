<%@page import="model.UserProfile"%>
<%
    
// Allow access only if session exists
UserProfile login = null;
if (session.getAttribute("login") == null) {
    response.sendRedirect("index.jsp");
} else {
    login = (UserProfile) session.getAttribute("login");
}

String username = null;
String sessionID = null;
Cookie[] cookies = request.getCookies();

if (cookies !=null){
    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("user")) 
            username = cookie.getValue();
        if (cookie.getName().equals("JSESSIONID")) 
            sessionID = cookie.getValue();
    }
} else {
    sessionID = session.getId();
}

%>
