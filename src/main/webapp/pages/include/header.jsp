<%-- 
    Document   : header.jsp
    Created on : May 27, 2015, 5:57:21 PM
    Author     : realnot
--%>

<div class="cointainer container_16">
<div class="grid_3">
    <div id="logo">
        <h1>CRM</h1>
    </div>
</div>
<div class="grid_11">
    <div id="slogan">
        <span>{ We take care of your customers,</span>
        <span>so you can focus on your business }</span>
    </div>
</div>
<div class="grid_2">
    <div id="meta">
        <span>Welcome <c:out value="${login.username}" /></span>
        <form action="<%=response.encodeURL("LogoutServlet") %>" method="post">
            <input type="submit" value="Logout" >
        </form>
    </div>
</div>
<div class="clear">&ensp;</div>
</div>
