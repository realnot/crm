<%-- 
    Document   : head.jsp
    Created on : May 19, 2015, 6:05:46 PM
    Author     : realnot
--%>

<head>
    <title>CRM - Customer Relationship Management</title> 
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/960.css" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@include file="sessionChecker.jsp" %>
</head>