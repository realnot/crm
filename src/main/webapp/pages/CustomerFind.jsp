<%-- 
    Document   : CustomerFind
    Created on : May 25, 2015, 9:58:30 PM
    Author     : realnot
--%>

<!DOCTYPE html>
<html>
    <%@include file="include/head.jsp" %>
    <body>
        <div id="container">
            <div id="header">
                <%@include file="include/header.jsp" %> 
            </div>
            <div id="page" class="container container_16">
                <div id="nav" class="grid_4">
                    <%@include file="include/navigation.jsp" %>
                </div>
                <div id="add-customer" class="grid_4 content">
                    <h3>Find a </h3>
                    <form action="CustomerServlet" method="POST" class="crm-form crm-form-stacked">
                        <fieldset>
                        <legend>Required Fields</legend>
                        <div>
                            <label for="customer-code">Customer Code</label>
                            <input id="customer-code" name="customerCode" type="text" required>
                        </div>
                        <div>
                            <input type="hidden" name="action" value="find">
                            <input type="submit" class="pure-button pure-button-primary" value="Find Customer">
                            <input type="reset" value="Clear data">
                        </div>
                        </fieldset>
                    </form>
                </div>
                <div class='clear'>&nbsp;</div>
            </div>
        </div>
        <div id="footer">
            <%@include file="include/footer.html" %>
        </div>
    </body>
</html> 
