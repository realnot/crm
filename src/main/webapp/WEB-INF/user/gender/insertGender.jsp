<%-- 
    Document   : insertUserGender
    Created on : Mar 6, 2015, 2:33:45 PM
    Author     : realnot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert Gender</title>
        <style>.error { color: red; } .success { color: green; }</style>
    </head>
    <body>
        <form method="post">
            <h1>Insert a new gender</h1>
            <p>
                <label for="name">Please, insert the gender name</label>
                <input id="name" name="name" value="${fn:escapeXml(param.name)}">
                <span class="error">${messages.name}</span>
            </p>
            <p>
                <input type="submit">
                <span class="success">${messages.success}</span>
            </p>
        </form>
    </body>
</html>