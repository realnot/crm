<%-- 
    Document   : insertUserProfile
    Created on : Apr 27, 2015, 5:24:38 PM
    Author     : realnot
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert UserProfile</title>
        <style>.error { color: red; } .success { color: green; }</style>
    </head>
    <body>
        <form method="post">
            <h1>Insert a new User</h1>
            <p>
                <label for="usrname">Please, insert the username</label>
                <input id="username" name="username" value="${fn:escapeXml(param.username)}">
                <span class="error">${messages.username}</span>
            </p>
            <p>
                <label for="password">Please, insert the password</label>
                <input id="password" name="password" value="${fn:escapeXml(param.password)}">
                <span class="error">${messages.password}</span>
            </p>
            <p>
                <label for="email">Please, insert the email</label>
                <input id="email" name="email" value="${fn:escapeXml(param.email)}">
                <span class="error">${messages.name}</span>
            </p>
            <p>
                <input type="submit">
                <span class="success">${messages.success}</span>
            </p>
        </form>
    </body>
</html>