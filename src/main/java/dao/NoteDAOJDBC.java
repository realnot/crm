/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.NoteDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Note;

/**
 *
 * @author realnot
 */
public class NoteDAOJDBC implements NoteDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM note WHERE note_id = ?";
    private static final String SQL_FIND_BY_TOPIC
            = "SELECT * FROM note WHERE note_topic = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM note ORDER BY note_id";
    private static final String SQL_INSERT
            = "INSERT INTO note (note_topic, note_desc, note_created_by, "
            + "note_refered_to) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE note SET note_topic = ?, note_desc = ?, note_created_by = ?, "
            + "note_refered_to = ? WHERE note_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM note WHERE note_id = ?";
    private static final String SQL_EXIST_NOTE
            = "SELECT * FROM note WHERE note_id = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Note DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Note DAO for.
     */
    NoteDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Note find(Integer note_id) throws DAOException {
        return find(SQL_FIND_BY_ID, note_id);
    }
    
    @Override
    public Note find(String note_topic) throws DAOException {
        return find(SQL_FIND_BY_TOPIC, note_topic);
    }

    /**
     * Returns the note from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The note from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Note find(String sql, Object... values) throws DAOException {
        Note note = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                note = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return note;
    }
    
    @Override
    public void create(Note note)
            throws IllegalArgumentException, DAOException {

        if (note.getNoteID() != null) {
            throw new IllegalArgumentException(
                    "Note is already created, the note ID is not null.");
        }

        Object[] values = {
            note.getNoteTopic(),
            note.getNoteDesc(),
            note.getNoteCreatedBy(),
            note.getNoteReferedTo()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating note failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    note.setNoteID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating note failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Note> list() throws DAOException {
        List<Note> noteList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                noteList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return noteList;
    }

    @Override
    public void update(Note note) throws DAOException {
        if (note.getNoteID() == null) {
            throw new IllegalArgumentException("The note is not created yet, "
                    + "the note ID is null.");
        }

        Object[] values = {
            note.getNoteTopic(),
            note.getNoteDesc(),
            note.getNoteCreatedBy(),
            note.getNoteReferedTo(),
            note.getNoteID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating note failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Note note) throws DAOException {
        Object[] values = {
            note.getNoteID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting note failed, no rows affected.");
            } else {
                note.setNoteID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existNote(Integer note_id) throws DAOException {
        Object[] values = {
            note_id
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NOTE, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Note.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a note.
     * @return The mapped note from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Note map(ResultSet rs) throws SQLException {
        Note note = new Note();
        note.setNoteID(rs.getInt("note_id"));
        note.setNoteTopic(rs.getString("note_topic"));
        note.setNoteDesc(rs.getString("note_desc"));
        note.setNoteCreatedBy(rs.getInt("note_created_by"));
        note.setNoteReferedTo(rs.getInt("note_refered_to"));
        return note;
    }
}
