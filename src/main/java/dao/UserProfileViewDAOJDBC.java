/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.interfaces.UserProfileViewDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.UserProfileView;

/**
 *
 * @author realnot
 */
public class UserProfileViewDAOJDBC implements UserProfileViewDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_USERS_LIST = "SELECT "
        + "user_username AS username, user_email AS email, "
        + "status_name AS status, role_name AS role, "
        + "lang_name AS language, country_name AS country, "
        + "region_name AS region, city_name AS city "
        + "FROM user_profile "
        + "INNER JOIN user_status ON user_profile.user_status = user_status.status_id "
        + "INNER JOIN user_role ON user_profile.user_role = user_role.role_id "
        + "INNER JOIN user_lang ON user_profile.user_lang = user_lang.lang_id "
        + "INNER JOIN country ON user_profile.user_country = country.country_id "
        + "INNER JOIN region ON user_profile.user_region = region.region_id "
        + "INNER JOIN city ON user_profile.user_city = city.city_id "
        + "ORDER BY user_id;";
    
    // Vars -----------------------------------------------------------

    /**
     *
     */
    private final DAOFactory daoFactory;
    
    
    // Constructors ---------------------------------------------------
    /**
     * Construct a UserProfile DAO for the given DAOFactory. Package private so 
     * that it can be constructed inside the DAO package only.
     * @param daoFactory The DAOFactory to construct this UserProfile DAO for.
     */
    UserProfileViewDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    
    @Override
    public List<UserProfileView> list() throws DAOException {
        List<UserProfileView> profileList = new ArrayList<>();

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    conn.prepareStatement(SQL_USERS_LIST);
            ResultSet rs = stmt.executeQuery();
        ) {
            while (rs.next()) {
                profileList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return profileList;
    }
    
    // Helpers --------------------------------------------------------
    /**
     * Map the current row of the given ResultSet to a UserProfile.
     * @param resultSet The ResultSet of which the current row is to be 
     * mapped to a UserProfile.
     * @return The mapped UserProfile from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static UserProfileView map(ResultSet rs) throws SQLException {
        UserProfileView profile = new UserProfileView();
        profile.setStatusName(rs.getString("status"));
        profile.setRoleName(rs.getString("role"));
        profile.setLangName(rs.getString("language"));
        profile.setCountryName(rs.getString("country"));
        profile.setRegionName(rs.getString("region"));
        profile.setCityName(rs.getString("city"));
        profile.setUsername(rs.getString("username"));
        profile.setEmail(rs.getString("email"));
        return profile;
    }
}
