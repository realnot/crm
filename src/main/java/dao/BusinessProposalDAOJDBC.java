/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.BusinessProposalDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.BusinessProposal;

/**
 *
 * @author realnot
 */
public class BusinessProposalDAOJDBC implements BusinessProposalDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM business_proposal "
            + "INNER JOIN consulting_service "
                + "ON business_proposal.consulting_service = consulting_service.service_id "
             + "WHERE proposal_id = ?";
    private static final String SQL_FIND_BY_TOPIC
            = "SELECT * FROM business_proposal WHERE proposal_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM business_proposal "
            + "INNER JOIN consulting_service "
                + "ON business_proposal.consulting_service = consulting_service.service_id "
            + "ORDER BY proposal_id";
    private static final String SQL_INSERT
            = "INSERT INTO business_proposal (proposal_name, proposal_desc, "
            + "consulting_service) VALUES (?, ?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE business_proposal SET proposal_name = ?, proposal_desc = ?, "
            + "consulting_service = ? WHERE proposal_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM business_proposal WHERE proposal_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT proposal_name FROM business_proposal WHERE proposal_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a BusinessP DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this BusinessP DAO for.
     */
    BusinessProposalDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public BusinessProposal find(Integer proposal_id) throws DAOException {
        return find(SQL_FIND_BY_ID, proposal_id);
    }
    
    @Override
    public BusinessProposal find(String proposal_name) throws DAOException {
        return find(SQL_FIND_BY_TOPIC, proposal_name);
    }

    /**
     * Returns the proposal from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The proposal from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private BusinessProposal find(String sql, Object... values) throws DAOException {
        BusinessProposal proposal = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                proposal = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return proposal;
    }
    
    @Override
    public void create(BusinessProposal proposal)
            throws IllegalArgumentException, DAOException {

        if (proposal.getProposalID() != null) {
            throw new IllegalArgumentException(
                    "BusinessP is already created, the proposal ID is not null.");
        }

        Object[] values = {
            proposal.getProposalName(),
            proposal.getProposalDesc(),
            proposal.getConsultingService()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating proposal failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    proposal.setProposalID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating proposal failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<BusinessProposal> list() throws DAOException {
        List<BusinessProposal> proposalList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                proposalList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return proposalList;
    }
   
    @Override
    public void update(BusinessProposal proposal) throws DAOException {
        if (proposal.getProposalID() == null) {
            throw new IllegalArgumentException("The proposal is not created yet, "
                    + "the proposal ID is null.");
        }

        Object[] values = {
            proposal.getProposalName(),
            proposal.getProposalDesc(),
            proposal.getConsultingService(),
            proposal.getProposalID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating proposal failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(BusinessProposal proposal) throws DAOException {
        Object[] values = {
            proposal.getProposalID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting proposal failed, no rows affected.");
            } else {
                proposal.setProposalID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existBusinessProposal(String proposal) throws DAOException {
        Object[] values = {
            proposal
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a BusinessProposal.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a proposal.
     * @return The mapped proposal from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static BusinessProposal map(ResultSet rs) throws SQLException {
        BusinessProposal proposal = new BusinessProposal();
        proposal.setProposalID(rs.getInt("proposal_id"));
        proposal.setProposalName(rs.getString("proposal_name"));
        proposal.setProposalDesc(rs.getString("proposal_desc"));
        proposal.setConsultingService(rs.getInt("consulting_service"));
        proposal.setServiceName(rs.getString("service_name"));
        return proposal;
    }
}
