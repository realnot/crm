package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import dao.interfaces.*;

/**
 * This class represents a DAO factory for a SQL database. You can use 
 * {@link #getInstance(String)} to obtain a new instance for the given 
 * database name. The specific instance returned depends on the properties 
 * file configuration. You can obtain DAO's for the DAO factory instance 
 * using the  DAO getters.
 * <p>
 * This class requires a properties file named 'dao.properties' in the 
 * classpath with among others the following properties:
 * <pre>
 * name.url *
 * name.driver
 * name.username
 * name.password
 * </pre>
 * Those marked with * are required, others are optional and can be left 
 * away or empty. Only the username is required when any password is specified.
 * <ul>
 * <li>The 'name' must represent the database name in {@link #getInstance(String)}.</li>
 * <li>The 'name.url' must represent either the JDBC URL or JNDI name of the database.</li>
 * <li>The 'name.driver' must represent the full qualified class name of the JDBC driver.</li>
 * <li>The 'name.username' must represent the username of the database login.</li>
 * <li>The 'name.password' must represent the password of the database login.</li>
 * </ul>
 * If you specify the driver property, then the url property will be assumed as JDBC URL. If you
 * omit the driver property, then the url property will be assumed as JNDI name. When using JNDI
 * with username/password preconfigured, you can omit the username and password properties as well.
 * <p>
 * Here are basic examples of valid properties for a database with the name 'javabase':
 * <pre>
 * javabase.jdbc.url = jdbc:mysql://localhost:3306/javabase
 * javabase.jdbc.driver = com.mysql.jdbc.Driver
 * javabase.jdbc.username = java
 * javabase.jdbc.password = d$7hF_r!9Y
 * </pre>
 * <pre>
 * javabase.jndi.url = jdbc/javabase
 * </pre>
 * Here is a basic use example:
 * <pre>
 * DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
 * UserDAO userDAO = javabase.getUserDAO();
 * </pre>
 *
 * @author realnot
 */
public abstract class DAOFactory {

    // Constants ------------------------------------------------------
    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";

    // Actions --------------------------------------------------------
    
    /**
     * Returns a new DAOFactory instance for the given database name.
     * @param name The database name to return a new DAOFactory instance for.
     * @return A new DAOFactory instance for the given database name.
     * @throws DAOConfigurationException If the database name is null, 
     * or if the properties file is missing in the classpath or cannot
     * be loaded, or if a required property is missing in the properties
     * file, or if either the driver cannot be loaded or the datasource
     * cannot be found.
     */
    public static DAOFactory getInstance(String name) 
            throws DAOConfigurationException {
        if (name == null) {
            throw new DAOConfigurationException("Database name is null.");
        }

        DAOProperties properties = new DAOProperties(name);
        String url = properties.getProperty(PROPERTY_URL, true);
        String driverClassName = properties.getProperty(PROPERTY_DRIVER, true);
        String password = properties.getProperty(PROPERTY_PASSWORD, true);
        String username = properties.getProperty(PROPERTY_USERNAME, password != null);
        DAOFactory instance = null;

        // If driver is specified, then load it to let it register itself 
        // with DriverManager.
        if (driverClassName != null) {
            try {
                Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                throw new DAOConfigurationException(
                    "Driver class '" + driverClassName + 
                    "' is missing in classpath.", e);
            }
            instance = new DriverManagerDAOFactory(url, username, password);
        }
        
        // The driver is null/not specified
        else {
            throw new DAOConfigurationException(
                "DriverClassName is null, can't continue");  
        }

        return instance;
    }

    /**
     * Returns a connection to the database. Package private so that it can
     * be used inside the DAO
     * package only.
     * @return A connection to the database.
     * @throws SQLException If acquiring the connection fails.
     */
    abstract Connection getConnection() throws SQLException;

    /**
     * Returns the UserProfile DAO associated with the current DAOFactory.
     * @return The User DAO associated with the current DAOFactory.
    */ 
    public UserProfileDAO getUserProfileDAO() {
        return new UserProfileDAOJDBC(this);
    }
    /**
     * Returns the UserProfileView DAO associated with the current DAOFactory.
     * @return The User DAO associated with the current DAOFactory.
     */ 
    public UserProfileViewDAO getUserProfileViewDAO() {
        return new UserProfileViewDAOJDBC(this);
    }
    
    /**
     * Returns the User Role DAO associated with the current DAOFactory.
     * @return The Role DAO associated with the current DAOFactory. 
     */
    public RoleDAO getRoleDAO() {
        return new RoleDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public MeetingDAO getMeetingDAO() {
        return new MeetingDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting View DAO associated with the current DAOFactory.
     * @return The meeting View DAO associated with the current DAOFactory. 
     */
    public MeetingViewDAO getMeetingViewDAO() {
        return new MeetingViewDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public CustomerDAO getCustomerDAO() {
        return new CustomerDAOJDBC(this);
    }  
        
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public BusinessProposalDAO getBusinessProposalDAO() {
        return new BusinessProposalDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public ConsultingServiceDAO getConsultingServiceDAO() {
        return new ConsultingServiceDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public NoteDAOJDBC getNoteDAO() {
        return new NoteDAOJDBC(this);
    }
    
    /**
     * Returns the Log DAO associated with the current DAOFactory.
     * @return The Log DAO associated with the current DAOFactory. 
     */
    public LogDAOJDBC getLogDAO() {
        return new LogDAOJDBC(this);
    }

    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public UserProfileToCustomerDAO getUserProfileToCustomerDAO() {
        return new UserProfileToCustomerDAOJDBC(this);
    }
    
    /**
     * Returns the Meeting DAO associated with the current DAOFactory.
     * @return The meeting DAO associated with the current DAOFactory. 
     */
    public CustomerToBusinessProposalDAO getCustomerToBusinessProposalDAO() {
        return new CustomerToBusinessProposalDAOJDBC(this);
    }

    /**
     * Returns the Customer Type associated with the current DAOFactory.
     * @return The customer type associated with the current DAOFactory. 
     */
    public CustomerTypeDAO getCustomerTypeDAO() {
        return new CustomerTypeDAOJDBC(this);
    }
    
    /**
     * Returns the Product Category DAO associated with the current DAOFactory.
     * @return The product category associated with the current DAOFactory. 
     */
    public ProductCategoryDAO getProductCategoryDAO() {
        return new ProductCategoryDAOJDBC(this);
    }
    
    /**
     * Returns the CustomerToConsultingService DAO associated with the current DAOFactory.
     * @return The CustomerToConsultingService associated with the current DAOFactory. 
     */
    public CustomerToConsultingServiceDAO getCustomerToConsultingServiceDAO() {
        return new CustomerToConsultingServiceDAOJDBC(this);
    }
}
    // You can add more DAO implementation getters here.

// Default DAOFactory implementations ---------------------------------

/**
 * The DriverManager based DAOFactory.
 */
class DriverManagerDAOFactory extends DAOFactory {
    private String url;
    private String username;
    private String password;

    DriverManagerDAOFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}