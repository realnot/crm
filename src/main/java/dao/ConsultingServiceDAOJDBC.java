/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.ConsultingServiceDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ConsultingService;

/**
 *
 * @author realnot
 */
public class ConsultingServiceDAOJDBC implements ConsultingServiceDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM consulting_service WHERE service_id = ?";
    private static final String SQL_FIND_BY_NAME
            = "SELECT * FROM consulting_service WHERE service_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM consulting_service ORDER BY service_id";
    private static final String SQL_INSERT
            = "INSERT INTO consulting_service (service_name, service_desc) "
            + "VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE consulting_service SET service_name = ?, service_desc = ? "
            + "WHERE service_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM consulting_service WHERE service_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT * FROM consulting_service WHERE service_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a BusinessP DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this BusinessP DAO for.
     */
    ConsultingServiceDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public ConsultingService find(Integer cs_id) throws DAOException {
        return find(SQL_FIND_BY_ID, cs_id);
    }
    
    @Override
    public ConsultingService find(String cs_name) throws DAOException {
        return find(SQL_FIND_BY_NAME, cs_name);
    }

    /**
     * Returns the consulting service from the database matching the given SQL 
     * query with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The consulting from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private ConsultingService find(String sql, Object... values) throws DAOException {
        ConsultingService consulting = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                consulting = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return consulting;
    }
    
    @Override
    public void create(ConsultingService consulting)
            throws IllegalArgumentException, DAOException {

        if (consulting.getServiceID() != null) {
            throw new IllegalArgumentException(
                    "BusinessP is already created, the consulting ID is not null.");
        }

        Object[] values = {
            consulting.getServiceName(),
            consulting.getServiceDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating consulting failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    consulting.setServiceID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating consulting failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<ConsultingService> list() throws DAOException {
        List<ConsultingService> consultingList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                consultingList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return consultingList;
    }

    @Override
    public void update(ConsultingService consulting) throws DAOException {
        if (consulting.getServiceID() == null) {
            throw new IllegalArgumentException("The consulting is not created yet, "
                    + "the consulting ID is null.");
        }

        Object[] values = {
            consulting.getServiceName(),
            consulting.getServiceDesc(),
            consulting.getServiceID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating consulting failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(ConsultingService consulting) throws DAOException {
        Object[] values = {
            consulting.getServiceID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting consulting failed, no rows affected.");
            } else {
                consulting.setServiceID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existConsultingService(String consulting) throws DAOException {
        Object[] values = {
            consulting
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a ConsultingService.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a consulting.
     * @return The mapped consulting from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static ConsultingService map(ResultSet rs) throws SQLException {
        ConsultingService consulting = new ConsultingService();
        consulting.setServiceID(rs.getInt("service_id"));
        consulting.setServiceName(rs.getString("service_name"));
        consulting.setServiceDesc(rs.getString("service_desc"));
        return consulting;
    }
}
