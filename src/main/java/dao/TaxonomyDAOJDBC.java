/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.TaxonomyDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Taxonomy;

/**
 *
 * @author realnot
 */
public class TaxonomyDAOJDBC  implements TaxonomyDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM taxonomy WHERE tag_id = ?";
    private static final String SQL_FIND_BY_NAME
            = "SELECT * FROM taxonomy WHERE tag_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM taxonomy ORDER BY tag_id";
    private static final String SQL_INSERT
            = "INSERT INTO taxonomy (tag_name, tag_desc) VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE taxonomy SET tag_name = ?, tag_desc = ? "
            + "WHERE tag_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM taxonomy WHERE tag_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT * FROM taxonomy WHERE tag_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Taxonomy DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Taxonomy DAO for.
     */
    TaxonomyDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Taxonomy find(Integer tag_id) throws DAOException {
        return find(SQL_FIND_BY_ID, tag_id);
    }
    
    @Override
    public Taxonomy find(String tag_name) throws DAOException {
        return find(SQL_FIND_BY_NAME, tag_name);
    }

    /**
     * Returns the tag from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The tag from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Taxonomy find(String sql, Object... values) throws DAOException {
        Taxonomy tag = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                tag = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tag;
    }
    
    @Override
    public void create(Taxonomy tag)
            throws IllegalArgumentException, DAOException {

        if (tag.getTagID() != null) {
            throw new IllegalArgumentException(
                    "Taxonomy is already created, the tag ID is not null.");
        }

        Object[] values = {
            tag.getTagName(),
            tag.getTagDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating tag failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tag.setTagID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating tag failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Taxonomy> list() throws DAOException {
        List<Taxonomy> tagList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                tagList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return tagList;
    }

    @Override
    public void update(Taxonomy tag) throws DAOException {
        if (tag.getTagID() == null) {
            throw new IllegalArgumentException("The tag is not created yet, "
                    + "the tag ID is null.");
        }

        Object[] values = {
            tag.getTagName(),
            tag.getTagDesc(),
            tag.getTagID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating tag failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Taxonomy tag) throws DAOException {
        Object[] values = {
            tag.getTagID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting tag failed, no rows affected.");
            } else {
                tag.setTagID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existTaxonomy(String tag_name) throws DAOException {
        Object[] values = {
            tag_name
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Taxonomy.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a tag.
     * @return The mapped tag from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Taxonomy map(ResultSet rs) throws SQLException {
        Taxonomy tag = new Taxonomy();
        tag.setTagID(rs.getInt("tag_id"));
        tag.setTagName(rs.getString("tag_name"));
        tag.setTagDesc(rs.getString("tag_desc"));
        return tag;
    }
}
