/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.CustomerToTagDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CustomerToTag;

/**
 *
 * @author realnot
 */
public class CustomerToTagDAOJDBC implements CustomerToTagDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM customer_to_tag WHERE ctt_id = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM customer_to_tag ORDER BY ctt_id";
    private static final String SQL_INSERT
            = "INSERT INTO customer_to_tag (customer_id, tag_id) VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer_to_tag SET customer_id = ?, tag_id = ? "
            + "WHERE ctt_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer_to_tag WHERE ctt_id = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a CustomerToTag DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this CustomerToTag DAO for.
     */
    CustomerToTagDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public CustomerToTag find(Integer ctt_id) throws DAOException {
        return find(SQL_FIND_BY_ID, ctt_id);
    }

    /**
     * Returns the tag from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The tag from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private CustomerToTag find(String sql, Object... values) throws DAOException {
        CustomerToTag ctt = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                ctt = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctt;
    }
    
    @Override
    public void create(CustomerToTag ctt)
            throws IllegalArgumentException, DAOException {

        if (ctt.getCTTID() != null) {
            throw new IllegalArgumentException(
                    "CustomerToTag is already created, the ctt ID is not null.");
        }

        Object[] values = {
            ctt.getCustomerID(),
            ctt.getTagID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating ctt failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    ctt.setCTTID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating ctt failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<CustomerToTag> list() throws DAOException {
        List<CustomerToTag> cttList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                cttList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return cttList;
    }

    @Override
    public void update(CustomerToTag ctt) throws DAOException {
        if (ctt.getTagID() == null) {
            throw new IllegalArgumentException("The ctt is not created yet, "
                    + "the ctt ID is null.");
        }

        Object[] values = {
            ctt.getTagID(),
            ctt.getCustomerID(),
            ctt.getCTTID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating ctt failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(CustomerToTag ctt) throws DAOException {
        Object[] values = {
            ctt.getTagID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting ctt failed, no rows affected.");
            } else {
                ctt.setCTTID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a CustomerToTag.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a tag.
     * @return The mapped tag from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static CustomerToTag map(ResultSet rs) throws SQLException {
        CustomerToTag ctt = new CustomerToTag();
        ctt.setTagID(rs.getInt("tag_id"));
        ctt.setCustomerID(rs.getInt("customer_id"));
        ctt.setCTTID(rs.getInt("ctt_id"));
        return ctt;
    }
}
