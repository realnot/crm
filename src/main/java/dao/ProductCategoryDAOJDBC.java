/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.ProductCategoryDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ProductCategory;

/**
 *
 * @author realnot
 */
public class ProductCategoryDAOJDBC implements ProductCategoryDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM product_category WHERE category_id = ?";
    private static final String SQL_FIND_BY_NAME
            = "SELECT * FROM product_category WHERE category_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM product_category ORDER BY category_id";
    private static final String SQL_INSERT
            = "INSERT INTO product_category "
                + "(category_name, category_desc) "
            + "VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE product_category SET "
                + "category_name = ?, category_desc = ? "
            + "WHERE category_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM product_category WHERE category_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT category_name FROM product_category "
            + "WHERE category_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a ProductCategory DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this ProductCategory DAO for.
     */
    ProductCategoryDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public ProductCategory find(Integer product_category_id) throws DAOException {
        return find(SQL_FIND_BY_ID, product_category_id);
    }
    
    @Override
    public ProductCategory find(String product_category_name) throws DAOException {
        return find(SQL_FIND_BY_NAME, product_category_name);
    }

    /**
     * Returns the product_category from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The product_category from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private ProductCategory find(String sql, Object... values) throws DAOException {
        ProductCategory product_category = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                product_category = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return product_category;
    }
    
    @Override
    public void create(ProductCategory product_category)
            throws IllegalArgumentException, DAOException {

        if (product_category.getProductID() != null) {
            throw new IllegalArgumentException(
                    "ProductCategory is already created, the product_category ID is not null.");
        }

        Object[] values = {
            product_category.getProductName(),
            product_category.getProductDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating product_category failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    product_category.setProductID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating product_category failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<ProductCategory> list() throws DAOException {
        List<ProductCategory> product_categoryList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                product_categoryList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return product_categoryList;
    }

    @Override
    public void update(ProductCategory product_category) throws DAOException {
        if (product_category.getProductID() == null) {
            throw new IllegalArgumentException("The product_category is not created yet, "
                    + "the product_category ID is null.");
        }

        Object[] values = {
            product_category.getProductName(),
            product_category.getProductDesc(),
            product_category.getProductID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating product_category failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(ProductCategory product_category) throws DAOException {
        Object[] values = {
            product_category.getProductID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting product_category failed, no rows affected.");
            } else {
                product_category.setProductID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existProductCategory(String product_category_name) throws DAOException {
        Object[] values = {
            product_category_name
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a ProductCategory.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a product_category.
     * @return The mapped product_category from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static ProductCategory map(ResultSet rs) throws SQLException {
        ProductCategory product_category = new ProductCategory();
        product_category.setProductID(rs.getInt("category_id"));
        product_category.setProductName(rs.getString("category_name"));
        product_category.setProductDesc(rs.getString("category_desc"));
        return product_category;
    }
}
