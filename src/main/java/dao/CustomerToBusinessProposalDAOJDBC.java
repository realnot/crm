/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.CustomerToBusinessProposalDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CustomerToBusinessProposal;

/**
 *
 * @author realnot
 */
public class CustomerToBusinessProposalDAOJDBC implements CustomerToBusinessProposalDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM business_proposal "
            + "INNER JOIN customer_to_business_proposal "
                + "ON customer_to_business_proposal.ctbp_proposal_id = business_proposal.proposal_id "
            + "INNER JOIN customer "
                + "ON customer_to_business_proposal.ctbp_customer_id = customer.customer_id "
            + "WHERE customer_to_business_proposal.ctbp_id = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM customer_to_business_proposal ORDER BY ctbp_customer_id";
    private static final String SQL_LIST_WITH_CUSTOMER
            = "SELECT * FROM business_proposal "
            + "INNER JOIN customer_to_business_proposal "
                + "ON business_proposal.proposal_id = customer_to_business_proposal.ctbp_proposal_id "
            + "INNER JOIN customer "
                + "ON customer.customer_id = customer_to_business_proposal.ctbp_customer_id";
    private static final String SQL_LIST_CUSTOMER_BY_ID
            = "SELECT * FROM business_proposal "
            + "INNER JOIN customer_to_business_proposal "
                + "ON customer_to_business_proposal.ctbp_proposal_id = business_proposal.proposal_id "
            + "INNER JOIN customer "
                + "ON customer_to_business_proposal.ctbp_customer_id = customer.customer_id "
            + "WHERE customer.customer_id = ? "
            + "ORDER BY customer_to_business_proposal.ctbp_id";
    private static final String SQL_INSERT
            = "INSERT INTO customer_to_business_proposal (ctbp_customer_id, ctbp_proposal_id) "
            + "VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer_to_business_proposal SET ctbp_customer_id = ?, ctbp_proposal_id = ? "
            + "WHERE ctbp_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer_to_business_proposal WHERE proposal_id = ?";
    private static final String SQL_EXIST_CUSTOMER
            = "SELECT * FROM customer_to_business_proposal WHERE proposal_id = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a CustomerToBusinessProposal DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this CustomerToBusinessProposal DAO for.
     */
    CustomerToBusinessProposalDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    // Actions ---------------------------------------------------------
    @Override
    public CustomerToBusinessProposal find(Integer customer_id) throws DAOException {
        return find(SQL_FIND_BY_ID, customer_id);
    }
   
    /**
     * Returns the ctbp from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The ctbp from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private CustomerToBusinessProposal find(String sql, Object... values) 
            throws DAOException {
        CustomerToBusinessProposal ctbp = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                ctbp = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctbp;
    }
    
    @Override
    public void create(CustomerToBusinessProposal ctbp)
            throws IllegalArgumentException, DAOException {
        
        if (ctbp.getCTBPID() != null) {
            throw new IllegalArgumentException(
                    "CTBP is already created, the CTBP is not null.");
        }
        
        Object[] values = {
            ctbp.getCTBPCustomerID(),
            ctbp.getCTBPProposalID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            System.out.println(stmt);
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating ctbp failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    ctbp.setCustomerID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating ctbp failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<CustomerToBusinessProposal> list() throws DAOException {
        List<CustomerToBusinessProposal> ctbpList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_WITH_CUSTOMER);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            while (rs.next()) {
                ctbpList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctbpList;
    }

    @Override
    public List<CustomerToBusinessProposal> list(Integer ctbp_id) throws DAOException {
        List<CustomerToBusinessProposal> ctcsList = new ArrayList<>();

        if (ctbp_id == null) {
            throw new IllegalArgumentException("The ctcs is null.");
        }
            
        Object[] values = {
            ctbp_id
        };
        
        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                    = prepareStatement(conn, SQL_LIST_CUSTOMER_BY_ID, false, values);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            while (rs.next()) {
                ctcsList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctcsList;
    }
    
    @Override
    public void update(CustomerToBusinessProposal ctbp) throws DAOException {
        if (ctbp.getCTBPID() == null) {
            throw new IllegalArgumentException("The ctbp is not created yet, "
                    + "the ctbp ID is null.");
        }

        Object[] values = {
            ctbp.getCTBPCustomerID(),
            ctbp.getCTBPProposalID(),
            ctbp.getCTBPID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating ctbp failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(CustomerToBusinessProposal ctbp) throws DAOException {
        Object[] values = {
            ctbp.getCustomerID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting ctbp failed, no rows affected.");
            } else {
                ctbp.setCustomerID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existCTBP (Integer customer_id) 
            throws DAOException {
        Object[] values = {
            customer_id
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_CUSTOMER, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a CustomerToBusinessProposal.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a ctbp.
     * @return The mapped ctbp from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static CustomerToBusinessProposal map(ResultSet rs) throws SQLException {
        CustomerToBusinessProposal ctbp = new CustomerToBusinessProposal();
        ctbp.setCTBPID(rs.getInt("ctbp_id"));
        ctbp.setCTBPCustomerID(rs.getInt("ctbp_customer_id"));
        ctbp.setCTBPProposalID(rs.getInt("ctbp_proposal_id"));
        ctbp.setCustomerID(rs.getInt("customer_id"));
        ctbp.setCustomerName(rs.getString("customer_name"));
        ctbp.setCustomerEmail(rs.getString("customer_email"));
        ctbp.setCustomerCreatedBy(rs.getInt("customer_created_by"));
        ctbp.setCustomerAssignedTo(rs.getInt("customer_assigned_to"));
        ctbp.setCustomerCategory(rs.getInt("customer_category"));
        ctbp.setCustomerType(rs.getInt("customer_type"));
        ctbp.setCustomerContactName(rs.getString("customer_contact_name"));
        ctbp.setCustomerContactSurname(rs.getString("customer_contact_surname"));
        ctbp.setCustomerContactAddress(rs.getString("customer_contact_address"));
        ctbp.setCustomerContactPhone(rs.getString("customer_contact_phone"));
        ctbp.setCustomerCreatedOn(rs.getTimestamp("customer_created_on"));
        ctbp.setProposalID(rs.getInt("proposal_id"));
        ctbp.setProposalName(rs.getString("proposal_name"));
        ctbp.setProposalDesc(rs.getString("proposal_desc"));
        ctbp.setConsultingService(rs.getInt("consulting_service"));
        return ctbp;
    }
}
