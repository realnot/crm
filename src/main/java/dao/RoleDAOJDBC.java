/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.RoleDAO;
import model.Role;

/**
 *
 * @author realnot
 */
public class RoleDAOJDBC implements RoleDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM user_role WHERE role_id = ?";
    private static final String SQL_FIND_BY_NAME
            = "SELECT * FROM user_role WHERE role_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM user_role ORDER BY role_id";
    private static final String SQL_INSERT
            = "INSERT INTO user_role (role_name, role_desc) VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE user_role SET role_name = ?, role_desc = ? "
            + "WHERE role_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM user_role WHERE role_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT role_name FROM user_role WHERE role_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Role DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Role DAO for.
     */
    RoleDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Role find(Integer role_id) throws DAOException {
        return find(SQL_FIND_BY_ID, role_id);
    }
    
    @Override
    public Role find(String role_name) throws DAOException {
        return find(SQL_FIND_BY_NAME, role_name);
    }

    /**
     * Returns the role from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The role from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Role find(String sql, Object... values) throws DAOException {
        Role role = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                role = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return role;
    }
    
    @Override
    public void create(Role role)
            throws IllegalArgumentException, DAOException {

        if (role.getRoleID() != null) {
            throw new IllegalArgumentException(
                    "Role is already created, the role ID is not null.");
        }

        Object[] values = {
            role.getRoleName(),
            role.getRoleDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating role failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    role.setRoleID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating role failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Role> list() throws DAOException {
        List<Role> roleList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                roleList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return roleList;
    }

    @Override
    public void update(Role role) throws DAOException {
        if (role.getRoleID() == null) {
            throw new IllegalArgumentException("The role is not created yet, "
                    + "the role ID is null.");
        }

        Object[] values = {
            role.getRoleName(),
            role.getRoleDesc(),
            role.getRoleID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating role failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Role role) throws DAOException {
        Object[] values = {
            role.getRoleID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting role failed, no rows affected.");
            } else {
                role.setRoleID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existRole(String role_name) throws DAOException {
        Object[] values = {
            role_name
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Role.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a role.
     * @return The mapped role from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Role map(ResultSet rs) throws SQLException {
        Role role = new Role();
        role.setRoleID(rs.getInt("role_id"));
        role.setRoleName(rs.getString("role_name"));
        role.setRoleDesc(rs.getString("role_desc"));
        return role;
    }
}
