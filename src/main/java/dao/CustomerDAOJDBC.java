/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.CustomerDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author realnot
 */
public class CustomerDAOJDBC implements CustomerDAO {

    // Constants ------------------------------------------------------
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM customer ORDER BY customer_id";
    private static final String SQL_FIND_BY_ID
            = "SELECT "
            + "customer_id, "
            + "customer_name, "
            + "customer_email, "
            + "authors.user_id AS customer_created_by, "
            + "referenced.user_id AS customer_assigned_to, "
            + "customer_type, "
            + "customer_category, "
            + "customer_contact_name, "
            + "customer_contact_surname, "
            + "customer_contact_address, "
            + "customer_contact_phone, "
            + "customer_created_on "
            + "FROM customer "
            + "INNER JOIN user_profile AS authors "
                + "ON customer.customer_created_by = authors.user_id "
            + "INNER JOIN user_profile AS referenced "
                + "ON customer.customer_assigned_to = referenced.user_id "
            + "INNER JOIN customer_type "
                + "ON customer.customer_type = customer_type.type_id "
            + "INNER JOIN product_category "
                + "ON customer.customer_category = product_category.category_id "
            + "WHERE customer.customer_id = ?";
    private static final String SQL_FIND_BY_EMAIL
            = "SELECT * FROM customer WHERE customer_email LIKE ?";
    private static final String SQL_INSERT
            = "INSERT INTO customer ("
                + "customer_name, "
                + "customer_email, "
                + "customer_created_by, "
                + "customer_assigned_to, "
                + "customer_category, "
                + "customer_type, "
                + "customer_contact_name, "
                + "customer_contact_surname, "
                + "customer_contact_address, "
                + "customer_contact_phone) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer SET "
                + "customer_name = ?, "
                + "customer_email = ?, "
                + "customer_created_by = ?, "
                + "customer_assigned_to = ?, "
                + "customer_category = ?, "
                + "customer_type = ?, "
                + "customer_contact_name = ?, "
                + "customer_contact_surname = ?, "
                + "customer_contact_address = ?, "
                + "customer_contact_phone = ?, "
                + "customer_created_on = ? "
            + "WHERE customer_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer WHERE customer_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT * FROM customer WHERE customer_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Customer DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Customer DAO for.
     */
    CustomerDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Customer find(Integer customer_id) throws DAOException {
        return find(SQL_FIND_BY_ID, customer_id);
    }
    
    @Override
    public Customer find(String customer_name) throws DAOException {
        return find(SQL_FIND_BY_EMAIL, customer_name);
    }

    /**
     * Returns the customer from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The customer from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Customer find(String sql, Object... values) throws DAOException {
        Customer customer = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            if (rs.next()) {
                customer = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return customer;
    }
    
    @Override
    public void create(Customer customer)
            throws IllegalArgumentException, DAOException {

        if (customer.getCustomerID() != null) {
            throw new IllegalArgumentException(
                    "Customer is already created, the customer ID is not null.");
        }

        Object[] values = {
            customer.getCustomerName(),
            customer.getCustomerEmail(),
            customer.getCustomerCreatedBy(),
            customer.getCustomerAssignedTo(),
            customer.getCustomerCategory(),
            customer.getCustomerType(),
            customer.getCustomerContactName(),
            customer.getCustomerContactSurname(),
            customer.getCustomerContactAddress(),
            customer.getCustomerContactPhone()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            System.out.println(stmt);
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating customer failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    customer.setCustomerID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating customer failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Customer> list() throws DAOException {
        List<Customer> customerList = new ArrayList<>();
        
        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                customerList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return customerList;
    }

    @Override
    public void update(Customer customer) throws DAOException {
        if (customer.getCustomerID() == null) {
            throw new IllegalArgumentException("The customer is not created yet, "
                    + "the customer ID is null.");
        }
        
        Object[] values = {
            customer.getCustomerName(),
            customer.getCustomerEmail(),
            customer.getCustomerCreatedBy(),
            customer.getCustomerAssignedTo(),
            customer.getCustomerCategory(),
            customer.getCustomerType(),
            customer.getCustomerContactName(),
            customer.getCustomerContactSurname(),
            customer.getCustomerContactAddress(),
            customer.getCustomerContactPhone(),
            customer.getCustomerCreatedOn(),
            customer.getCustomerID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating customer failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Customer customer) throws DAOException {
        Object[] values = {
            customer.getCustomerID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting customer failed, no rows affected.");
            } else {
                customer.setCustomerID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existCustomer(String customer_name) throws DAOException {
        Object[] values = {
            customer_name
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Customer.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a customer.
     * @return The mapped customer from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Customer map(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setCustomerID(rs.getInt("customer_id"));
        customer.setCustomerName(rs.getString("customer_name"));
        customer.setCustomerEmail(rs.getString("customer_email"));
        customer.setCustomerCreatedBy(rs.getInt("customer_created_by"));
        customer.setCustomerAssignedTo(rs.getInt("customer_assigned_to"));
        customer.setCustomerCategory(rs.getInt("customer_category"));
        customer.setCustomerType(rs.getInt("customer_type"));
        customer.setCustomerContactName(rs.getString("customer_contact_name"));
        customer.setCustomerContactSurname(rs.getString("customer_contact_surname"));
        customer.setCustomerContactAddress(rs.getString("customer_contact_address"));
        customer.setCustomerContactPhone(rs.getString("customer_contact_phone"));
        customer.setCustomerCreatedOn(rs.getTimestamp("customer_created_on"));
        return customer;
    }
}
