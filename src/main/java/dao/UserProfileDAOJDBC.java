/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.interfaces.UserProfileDAO;
import static dao.DAOUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.UserProfile;

/**
 *
 * @author realnot
 */
public class UserProfileDAOJDBC implements UserProfileDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID =
        "SELECT * FROM user_profile "
            + "INNER JOIN user_role "
                + "ON user_profile.user_role = user_role.role_id "
            + "WHERE user_id = ?";
    
    private static final String SQL_FIND_BY_USERNAME =
        "SELECT * FROM user_profile "
            + "INNER JOIN user_role "
                + "ON user_profile.user_role = user_role.role_id "
            + "WHERE user_username = ?";
    
    private static final String SQL_LIST_ORDER_BY_ID =
        "SELECT * FROM user_profile "
            + "INNER JOIN user_role "
                + "ON user_profile.user_role = user_role.role_id "
            + "ORDER BY user_id";
    
    private static final String SQL_INSERT =
        "INSERT INTO user_profile ("
            + "user_role, user_username, user_password, user_email, "
            + "user_name, user_surname, user_address, user_birthdate, "
            + "user_display_name) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_UPDATE =
        "UPDATE user_profile SET "
            + "user_role = ?, user_username = ?, user_password = ?, "
            + "user_email = ?, user_name = ?, user_surname = ?, "
            + "user_address = ?, user_birthdate = ?, user_display_name = ?, "
            + "user_last_login = ? "
            + "WHERE user_id = ?";
    
    private static final String SQL_DELETE =
        "DELETE FROM user_profile WHERE user_id = ?";
    
    private static final String SQL_EXIST_PROFILE =
        "SELECT user_id FROM user_profile WHERE user_id = ?";
    
    private static final String SQL_EXIST_USERNAME =
        "SELECT user_id FROM user_profile WHERE user_username = ?";
    
    private static final String SQL_EXIST_EMAIL =
        "SELECT user_id FROM user_profile WHERE user_email = ?";
    
    // Vars -----------------------------------------------------------

    /**
     *
     */
    private DAOFactory daoFactory;
    
    // Constructors ---------------------------------------------------
    /**
     * Construct a UserProfile DAO for the given DAOFactory. Package private so 
     * that it can be constructed inside the DAO package only.
     * @param daoFactory The DAOFactory to construct this UserProfile DAO for.
     */
    UserProfileDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public UserProfile find(Integer user_id) throws DAOException {
        return find(SQL_FIND_BY_ID, user_id);
    }
    
    @Override
    public UserProfile find(String user_username) throws DAOException {
        return find(SQL_FIND_BY_USERNAME, user_username);
    }
    
    
    /**
     * Returns the user from the database matching the given SQL query with 
     * the given values.
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The user from the database matching the given SQL query with 
     * the given values.
     * @throws DAOException If something fails at database level.
     */
    private UserProfile find(String sql, Object... values) throws DAOException {
        UserProfile profile = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, sql, false, values);
            ResultSet rs = stmt.executeQuery();
        ) {
            if (rs.next()) {
                profile = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return profile;
    }
    
    @Override
    public List<UserProfile> list() throws DAOException {
        List<UserProfile> profileList = new ArrayList<>();

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
            ResultSet rs = stmt.executeQuery();
        ) {
            while (rs.next()) {
                profileList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return profileList;
    }

    @Override
    public void create(UserProfile profile) 
            throws IllegalArgumentException, DAOException {
        
        if (profile.getUserID() != null) {
            throw new IllegalArgumentException(
                "The UserProfile is already created, the ID is not null.");
        }
        
        Object[] values = {
            profile.getRoleID(),
            profile.getUsername(),
            profile.getPassword(),
            profile.getEmail(),
            profile.getName(),
            profile.getSurname(),
            profile.getAddress(),
            profile.getBirthdate(),
            profile.getDisplayName()
        };

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, SQL_INSERT, true, values);
        ) {
            System.out.println(stmt);
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating UserProfile failed, "
                        + "no rows affected.");
            }
            
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    profile.setUserID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating UserProfile failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
    
    @Override
    public void update(UserProfile profile) throws DAOException {
        if (profile.getUserID() == null) {
            throw new IllegalArgumentException("UserProfile is not created yet, "
                    + "The UserID is null.");
        }

        Object[] values = {
            profile.getRoleID(),
            profile.getUsername(),
            profile.getPassword(),
            profile.getEmail(),
            profile.getName(),
            profile.getSurname(),
            profile.getAddress(),
            profile.getBirthdate(),
            profile.getDisplayName(),
            profile.getLastLogin(),
            profile.getUserID()
        };

        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, 
                    SQL_UPDATE, false, values);
        ) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Updating UserProfile failed, "
                        + "no rows affected.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(UserProfile profile) throws DAOException {
        Object[] values = { 
            profile.getUserID()
        };

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, SQL_DELETE, false, values);
        ) {
            System.out.println(stmt);
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Deleting UserProfile failed, "
                        + "no rows affected.");
            } else {
                profile.setUserID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existProfile(Integer user_id) throws DAOException {
        Object[] values = { 
            user_id
        };

        boolean exist = false;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, SQL_EXIST_PROFILE, false, values);
            ResultSet rs = stmt.executeQuery();
        ) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    @Override
    public boolean existUsername(String username) throws DAOException {
        Object[] values = { 
            username
        };

        boolean exist = false;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, SQL_EXIST_USERNAME, false, values);
            ResultSet rs = stmt.executeQuery();
        ) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    @Override
    public boolean existEmail(String email) throws DAOException {
        Object[] values = { 
            email
        };

        boolean exist = false;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt = 
                    prepareStatement(conn, SQL_EXIST_EMAIL, false, values);
            ResultSet rs = stmt.executeQuery();
        ) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    /**
     * Map the current row of the given ResultSet to a UserProfile.
     * @param resultSet The ResultSet of which the current row is to be 
     * mapped to a UserProfile.
     * @return The mapped UserProfile from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static UserProfile map(ResultSet rs) throws SQLException {
        UserProfile profile = new UserProfile();
        profile.setUserID(rs.getInt("user_id"));
        profile.setRoleID(rs.getInt("user_role"));
        profile.setUsername(rs.getString("user_username"));
        profile.setPassword(rs.getString("user_password"));
        profile.setEmail(rs.getString("user_email"));
        profile.setRoleName(rs.getString("role_name"));
        profile.setName(rs.getString("user_name"));
        profile.setSurname(rs.getString("user_surname"));
        profile.setAddress(rs.getString("user_address"));
        profile.setBirthdate(rs.getString("user_birthdate"));
        profile.setLastLogin(rs.getTimestamp("user_last_login"));
        profile.setDisplayName(rs.getString("user_display_name"));
        return profile;
    }
    
    /**
     * 
     * @param rs
     * @throws SQLException 
     */
    private static void PrintResultSet(ResultSet rs) throws SQLException {
    
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = rs.getString(i);
                System.out.print(columnValue + " " + rsmd.getColumnName(i));
            }
            System.out.println("");
        }
    }
}


