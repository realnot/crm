/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.UserProfileToCustomerDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.UserProfileToCustomer;

/**
 *
 * @author realnot
 */
public class UserProfileToCustomerDAOJDBC implements UserProfileToCustomerDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM customer_to_user WHERE customer_id = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM customer_to_user ORDER BY customer_id";
    private static final String SQL_INSERT
            = "INSERT INTO customer_to_user (user_id, customer_id) VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer_to_user SET user_id = ?,  customer_id = ? "
            + "WHERE customer_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer_to_user WHERE customer_id = ?";
    private static final String SQL_EXIST_CUSTOMER
            = "SELECT * FROM customer_to_user WHERE customer_id = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a UserProfileToCustomer DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this UserProfileToCustomer DAO for.
     */
    UserProfileToCustomerDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    // Actions ---------------------------------------------------------
    @Override
    public UserProfileToCustomer find(Integer uptc_id) throws DAOException {
        return find(SQL_FIND_BY_ID, uptc_id);
    }
   
    /**
     * Returns the uptc from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The uptc from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private UserProfileToCustomer find(String sql, Object... values) throws DAOException {
        UserProfileToCustomer uptc = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                uptc = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return uptc;
    }
    
    @Override
    public void create(UserProfileToCustomer uptc)
            throws IllegalArgumentException, DAOException {

        Object[] values = {
            uptc.getUserID(),
            uptc.getCustomerID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating uptc failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    uptc.setCustomerID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating uptc failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<UserProfileToCustomer> list() throws DAOException {
        List<UserProfileToCustomer> uptcList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                uptcList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return uptcList;
    }

    @Override
    public void update(UserProfileToCustomer uptc) throws DAOException {
        if (uptc.getCustomerID() == null) {
            throw new IllegalArgumentException("The uptc is not created yet, "
                    + "the uptc ID is null.");
        }

        Object[] values = {
            uptc.getUserID(),
            uptc.getCustomerID(),
            uptc.getCustomerID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating uptc failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(UserProfileToCustomer uptc) throws DAOException {
        Object[] values = {
            uptc.getCustomerID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting uptc failed, no rows affected.");
            } else {
                uptc.setCustomerID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existUPTC (Integer customer_id) 
            throws DAOException {
        Object[] values = {
            customer_id
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_CUSTOMER, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a UserProfileToCustomer.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a uptc.
     * @return The mapped uptc from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static UserProfileToCustomer map(ResultSet rs) throws SQLException {
        UserProfileToCustomer uptc = new UserProfileToCustomer();
        uptc.setCustomerID(rs.getInt("customer_id"));
        uptc.setUserID(rs.getInt("user_id"));
        return uptc;
    }
}
