/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.CustomerTypeDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CustomerType;

/**
 *
 * @author realnot
 */
public class CustomerTypeDAOJDBC implements CustomerTypeDAO {
     
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM customer_type WHERE type_id = ?";
    private static final String SQL_FIND_BY_NAME
            = "SELECT * FROM customer_type WHERE type_name = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM customer_type ORDER BY type_id";
    private static final String SQL_INSERT
            = "INSERT INTO customer_type "
                + "(type_name, type_desc) "
            + "VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer_type SET "
                + "type_name = ?, type_desc = ? "
            + "WHERE type_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer_type WHERE type_id = ?";
    private static final String SQL_EXIST_NAME
            = "SELECT type_name FROM customer_type "
            + "WHERE type_name = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a CustomerType DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this CustomerType DAO for.
     */
    CustomerTypeDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public CustomerType find(Integer customer_type_id) throws DAOException {
        return find(SQL_FIND_BY_ID, customer_type_id);
    }
    
    @Override
    public CustomerType find(String customer_type_name) throws DAOException {
        return find(SQL_FIND_BY_NAME, customer_type_name);
    }

    /**
     * Returns the customer_type from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The customer_type from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private CustomerType find(String sql, Object... values) throws DAOException {
        CustomerType customer_type = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                customer_type = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return customer_type;
    }
    
    @Override
    public void create(CustomerType customer_type)
            throws IllegalArgumentException, DAOException {

        if (customer_type.getCustomerTypeID() != null) {
            throw new IllegalArgumentException(
                    "CustomerType is already created, the customer_type ID is not null.");
        }

        Object[] values = {
            customer_type.getCustomerTypeName(),
            customer_type.getCustomerTypeDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating customer_type failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    customer_type.setCustomerTypeID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating customer_type failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<CustomerType> list() throws DAOException {
        List<CustomerType> customerTypeList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                customerTypeList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return customerTypeList;
    }

    @Override
    public void update(CustomerType customer_type) throws DAOException {
        if (customer_type.getCustomerTypeID() == null) {
            throw new IllegalArgumentException("The customer_type is not created yet, "
                    + "the customer_type ID is null.");
        }

        Object[] values = {
            customer_type.getCustomerTypeName(),
            customer_type.getCustomerTypeDesc(),
            customer_type.getCustomerTypeID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating customer_type failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(CustomerType customer_type) throws DAOException {
        Object[] values = {
            customer_type.getCustomerTypeID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting customer_type failed, no rows affected.");
            } else {
                customer_type.setCustomerTypeID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existCustomerType(String customer_type_name) throws DAOException {
        Object[] values = {
            customer_type_name
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_NAME, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a CustomerType.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a customer_type.
     * @return The mapped customer_type from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static CustomerType map(ResultSet rs) throws SQLException {
        CustomerType customer_type = new CustomerType();
        customer_type.setCustomerTypeID(rs.getInt("type_id"));
        customer_type.setCustomerTypeName(rs.getString("type_name"));
        customer_type.setCustomerTypeDesc(rs.getString("type_desc"));
        return customer_type;
    }
}
