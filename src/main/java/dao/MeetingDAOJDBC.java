/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.MeetingDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Meeting;

/**
 *
 * @author realnot
 */
public class MeetingDAOJDBC implements MeetingDAO {
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM meeting WHERE meeting_id = ?";
    private static final String SQL_FIND_BY_TOPIC
            = "SELECT * FROM meeting WHERE meeting_topic = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM meeting ORDER BY meeting_id";
    private static final String SQL_LIST_GIVEN_USER_ORDER_BY_ID
            = "SELECT * FROM meeting "
            + "WHERE meeting_created_by = ? "
            + "ORDER BY meeting_created_on";
    private static final String SQL_INSERT
            = "INSERT INTO meeting (meeting_topic, meeting_desc, "
            + "meeting_created_by, meeting_refered_to) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE meeting SET meeting_topic = ?, meeting_desc = ?, "
            + "meeting_created_by = ?, meeting_refered_to = ? WHERE meeting_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM meeting WHERE meeting_id = ?";
    private static final String SQL_EXIST_TOPIC
            = "SELECT meeting_topic FROM meeting WHERE meeting_topic = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Meeting DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Meeting DAO for.
     */
    MeetingDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Meeting find(Integer meeting_id) throws DAOException {
        return find(SQL_FIND_BY_ID, meeting_id);
    }
    
    @Override
    public Meeting find(String meeting_topic) throws DAOException {
        return find(SQL_FIND_BY_TOPIC, meeting_topic);
    }

    /**
     * Returns the meeting from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The meeting from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Meeting find(String sql, Object... values) throws DAOException {
        Meeting meeting = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                meeting = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return meeting;
    }
    
    @Override
    public void create(Meeting meeting)
            throws IllegalArgumentException, DAOException {

        if (meeting.getMeetingID() != null) {
            throw new IllegalArgumentException(
                    "Meeting is already created, the meeting ID is not null.");
        }

        Object[] values = {
            meeting.getMeetingTopic(),
            meeting.getMeetingDesc(),
            meeting.getMeetingCreatedBy(),
            meeting.getMeetingReferedTo()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating meeting failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    meeting.setMeetingID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating meeting failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Meeting> list() throws DAOException {
        List<Meeting> meetingList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                meetingList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return meetingList;
    }
    
    @Override
    public List<Meeting> list(Integer user_id) throws DAOException {
        List<Meeting> meetingList = new ArrayList<>();
        
        Object[] values = {
            user_id
        };
        
        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_LIST_GIVEN_USER_ORDER_BY_ID, false, values);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                meetingList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return meetingList;
    }

    @Override
    public void update(Meeting meeting) throws DAOException {
        if (meeting.getMeetingID() == null) {
            throw new IllegalArgumentException("The meeting is not created yet, "
                    + "the meeting ID is null.");
        }

        Object[] values = {
            meeting.getMeetingTopic(),
            meeting.getMeetingDesc(),
            meeting.getMeetingCreatedBy(),
            meeting.getMeetingReferedTo(),
            meeting.getMeetingID(),
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating meeting failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Meeting meeting) throws DAOException {
        Object[] values = {
            meeting.getMeetingID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting meeting failed, no rows affected.");
            } else {
                meeting.setMeetingID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existMeeting(String meeting_topic) throws DAOException {
        Object[] values = {
            meeting_topic
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_TOPIC, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Meeting.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a meeting.
     * @return The mapped meeting from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Meeting map(ResultSet rs) throws SQLException {
        Meeting meeting = new Meeting();
        meeting.setMeetingID(rs.getInt("meeting_id"));
        meeting.setMeetingTopic(rs.getString("meeting_topic"));
        meeting.setMeetingDesc(rs.getString("meeting_desc"));
        meeting.setMeetingCreatedOn(rs.getDate("meeting_created_on"));
        meeting.setMeetingCreatedBy(rs.getInt("meeting_created_by"));
        meeting.setMeetingReferedTo(rs.getInt("meeting_refered_to"));
        return meeting;
    }
}
