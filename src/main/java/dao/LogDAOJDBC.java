/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.interfaces.LogDAO;
import model.Log;

/**
 *
 * @author realnot
 */
public class LogDAOJDBC implements LogDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_ID
            = "SELECT * FROM log WHERE log_id = ?";
    private static final String SQL_LIST_ORDER_BY_DATE
            = "SELECT log_id, log_refered_to, user_username as log_username, "
            + "log_desc, log_created_on "
            + "FROM log "
            + "INNER JOIN user_profile "
                + "ON user_profile.user_id = log.log_refered_to "
            + "ORDER BY log_created_on DESC";
    private static final String SQL_INSERT
            = "INSERT INTO log (log_refered_to, log_desc) VALUES (?, ?)";
  
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a Log DAO for the given DAOFactory. Package private so that
     * it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this Log DAO for.
     */
    LogDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    // Actions ---------------------------------------------------------
    @Override
    public Log find(Integer log_id) throws DAOException {
        return find(SQL_FIND_BY_ID, log_id);
    }

    /**
     * Returns the log from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The log from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private Log find(String sql, Object... values) throws DAOException {
        Log log = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                log = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return log;
    }
    
    @Override
    public void create(Log log) throws IllegalArgumentException, DAOException {

        Object[] values = {
            log.getReferedTo(),
            log.getLogDesc()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating log failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    log.setLogID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating log failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Log> list() throws DAOException {
        List<Log> logList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_DATE);
                ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                logList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return logList;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a Log.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a log.
     * @return The mapped log from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Log map(ResultSet rs) throws SQLException {
        Log log = new Log();
        log.setLogID(rs.getInt("log_id"));
        log.setReferedTo(rs.getInt("log_refered_to"));
        log.setLogDesc(rs.getString("log_desc"));
        log.setLogCreatedOn(rs.getTimestamp("log_created_on"));
        log.setLogUsername(rs.getString("log_username"));
        return log;
    }
}
