/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.Meeting;

/**
 *
 * @author realnot
 */
public interface MeetingDAO {
    // Actions --------------------------------------------------------
    
    /**
     * Returns the meeting from the database matching the given ID, otherwise null.
     * @param meeting_id The ID of the meeting to be returned.
     * @return the meeting from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Meeting find(Integer meeting_id) throws DAOException;
    
    /**
     * Returns the meeting from the database matching the given name, otherwise null.
     * @param meeting_name The name of the meeting to be returned.
     * @return the meeting from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Meeting find(String meeting_name) throws DAOException;
    
    /**
     * Create the given meeting in the database. The meeting ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given meeting.
     * @param meeting The meeting to be created in the database.
     * @throws IllegalArgumentException If the meeting ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Meeting meeting) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all meetings from the database ordered by meeting ID. 
     * The list is never null and is empty when the database does not
     * contain any meeting.
     * @return A list of all meetings from the database ordered by meeting ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Meeting> list() throws DAOException;
    
    /**
     * Returns a list of all meetings from the database ordered by meeting ID. 
     * The list is never null and is empty when the database does not
     * contain any meeting.
     * @param user_id the meeting refereed to the user_id
     * @return A list of all meetings from the database ordered by meeting ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Meeting> list(Integer user_id) throws DAOException;
    
    /**
     * Update the given meeting in the database. The meeting ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param meeting The meeting to be updated in the database.
     * @throws IllegalArgumentException If the meeting ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(Meeting meeting) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given meeting from the database. After deleting, 
     * the DAO will set the ID of the given meeting to null.
     * @param meeting The meeting to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(Meeting meeting) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given meeting exist in the database.
     * @param meeting The meeting which is to be checked in the database.
     * @return True if the given meeting exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existMeeting(String meeting) throws DAOException;
}
