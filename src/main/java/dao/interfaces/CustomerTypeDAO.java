/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.CustomerType;

/**
 *
 * @author realnot
 */
public interface CustomerTypeDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the customer type from the database matching the given ID, otherwise null.
     * @param customer_type_id
     * @return the customer type from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public CustomerType find(Integer customer_type_id) throws DAOException;
    
    /**
     * Returns the customer type from the database matching the given name, otherwise null.
     * @param customer_type_name
     * @return the customer type from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public CustomerType find(String customer_type_name) throws DAOException;
    
    /**
     * Create the given customer type in the database. The customer type ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given customer type.
     * @param customer_type
     * @throws IllegalArgumentException If the customer type ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(CustomerType customer_type) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by customer type ID. 
     * The list is never null and is empty when the database does not
     * contain any customer type.
     * @return A list of all customer types from the database ordered by customer type ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerType> list() throws DAOException;
    
    /**
     * Update the given customer type in the database. The customer type ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param customer_type
     * @throws IllegalArgumentException If the customer type ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(CustomerType customer_type) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given customer type from the database. After deleting, 
     * the DAO will set the ID of the given customer type to null.
     * @param customer_type
     * @throws DAOException If something fails at database level.
     */
    public void delete(CustomerType customer_type) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given customer type exist in the database.
     * @param customer_type
     * @return True if the given customer type exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existCustomerType(String customer_type) throws DAOException;
}
