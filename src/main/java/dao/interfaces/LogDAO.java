/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.Log;

/**
 *
 * @author realnot
 */
public interface LogDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the log from the database matching the given ID, otherwise null.
     * @param log_id The ID of the log to be returned.
     * @return the log from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Log find(Integer log_id) throws DAOException;
    
    /**
     * Create the given log in the database. The log ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given log.
     * @param log The log to be created in the database.
     * @throws IllegalArgumentException If the log ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Log log) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by log ID. 
     * The list is never null and is empty when the database does not
     * contain any log.
     * @return A list of all logs from the database ordered by log ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Log> list() throws DAOException;
    
}
