/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.CustomerToTag;

/**
 *
 * @author realnot
 */
public interface CustomerToTagDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the ctt from the database matching the given ID, otherwise null.
     * @param ctt_id The ID of the ctt to be returned.
     * @return the ctt from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public CustomerToTag find(Integer ctt_id) throws DAOException;
    
    /**
     * Create the given ctt in the database. The ctt ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given ctt.
     * @param ctt_id The ctt to be created in the database.
     * @throws IllegalArgumentException If the ctt ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(CustomerToTag ctt_id) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by ctt ID. 
     * The list is never null and is empty when the database does not
     * contain any ctt.
     * @return A list of all ctts from the database ordered by ctt ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerToTag> list() throws DAOException;
    
    /**
     * Update the given ctt in the database. The ctt ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param ctt_id The ctt to be updated in the database.
     * @throws IllegalArgumentException If the ctt ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(CustomerToTag ctt_id) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given ctt from the database. After deleting, 
     * the DAO will set the ID of the given ctt to null.
     * @param ctt_id The ctt to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(CustomerToTag ctt_id) throws IllegalArgumentException, DAOException;
    
}
