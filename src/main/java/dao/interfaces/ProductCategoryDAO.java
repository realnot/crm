/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.ProductCategory;

/**
 *
 * @author realnot
 */
public interface ProductCategoryDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the category from the database matching the given ID, otherwise null.
     * @param category_id The ID of the category to be returned.
     * @return the category from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public ProductCategory find(Integer category_id) throws DAOException;
    
    /**
     * Returns the category from the database matching the given name, otherwise null.
     * @param category_name The name of the category to be returned.
     * @return the category from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public ProductCategory find(String category_name) throws DAOException;
    
    /**
     * Create the given category in the database. The category ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given category.
     * @param category The category to be created in the database.
     * @throws IllegalArgumentException If the category ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(ProductCategory category) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by category ID. 
     * The list is never null and is empty when the database does not
     * contain any category.
     * @return A list of all categories from the database ordered by category ID.
     * @throws DAOException If something fails at database level.
     */
    public List<ProductCategory> list() throws DAOException;
    
    /**
     * Update the given category in the database. The category ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param category The category to be updated in the database.
     * @throws IllegalArgumentException If the category ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(ProductCategory category) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given category from the database. After deleting, 
     * the DAO will set the ID of the given category to null.
     * @param category The category to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(ProductCategory category) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given category exist in the database.
     * @param category The category which is to be checked in the database.
     * @return True if the given category exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existProductCategory(String category) throws DAOException;
}
