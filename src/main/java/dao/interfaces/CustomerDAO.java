/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.Customer;

/**
 *
 * @author realnot
 */
public interface CustomerDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the customer from the database matching the given ID, otherwise null.
     * @param customer_id The ID of the customer to be returned.
     * @return the customer from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Customer find(Integer customer_id) throws DAOException;
    
    /**
     * Returns the customer from the database matching the given name, otherwise null.
     * @param customer_name The name of the customer to be returned.
     * @return the customer from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Customer find(String customer_name) throws DAOException;
    
    /**
     * Create the given customer in the database. The customer ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given customer.
     * @param customer The customer to be created in the database.
     * @throws IllegalArgumentException If the customer ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Customer customer) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by customer ID. 
     * The list is never null and is empty when the database does not
     * contain any customer.
     * @return A list of all customers from the database ordered by customer ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Customer> list() throws DAOException;
    
    /**
     * Update the given customer in the database. The customer ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param customer The customer to be updated in the database.
     * @throws IllegalArgumentException If the customer ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(Customer customer) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given customer from the database. After deleting, 
     * the DAO will set the ID of the given customer to null.
     * @param customer The customer to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(Customer customer) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given customer exist in the database.
     * @param customer The customer which is to be checked in the database.
     * @return True if the given customer exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existCustomer(String customer) throws DAOException;
    
}
