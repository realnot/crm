/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.ConsultingService;

/**
 *
 * @author realnot
 */
public interface ConsultingServiceDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the consulting service from the database matching the given ID, otherwise null.
     * @param consulting service_id The ID of the consulting service to be returned.
     * @return the consulting service from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public ConsultingService find(Integer cs_id) throws DAOException;
    
    /**
     * Returns the consulting service from the database matching the given name, otherwise null.
     * @param consulting service_name The name of the consulting service to be returned.
     * @return the consulting service from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public ConsultingService find(String cs_name) throws DAOException;
    
    /**
     * Create the given consulting service in the database. The consulting service ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given consulting service.
     * @param consulting service The consulting service to be created in the database.
     * @throws IllegalArgumentException If the consulting service ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(ConsultingService cs) 
            throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by consulting service ID. 
     * The list is never null and is empty when the database does not
     * contain any consulting service.
     * @return A list of all consulting services from the database ordered by consulting service ID.
     * @throws DAOException If something fails at database level.
     */
    public List<ConsultingService> list() throws DAOException;
    
    /**
     * Update the given consulting service in the database. The consulting service ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param consulting service The consulting service to be updated in the database.
     * @throws IllegalArgumentException If the consulting service ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(ConsultingService cs) 
            throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given consulting service from the database. After deleting, 
     * the DAO will set the ID of the given consulting service to null.
     * @param consulting service The consulting service to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(ConsultingService cs) 
            throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given consulting service exist in the database.
     * @param consulting service The consulting service which is to be checked in the database.
     * @return True if the given consulting service exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existConsultingService(String cs) throws DAOException;
    
}
