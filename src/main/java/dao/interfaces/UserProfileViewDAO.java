/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;

import model.UserProfileView;

/**
 *
 * @author realnot
 */
public interface UserProfileViewDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns a list of all Genders from the database ordered by gender ID. 
     * The list is never null and is empty when the database does not
     * contain any gender.
     * @return A list of all genders from the database ordered by gender ID.
     * @throws DAOException If something fails at database level.
     */
    public List<UserProfileView> list() throws DAOException;
    
}
