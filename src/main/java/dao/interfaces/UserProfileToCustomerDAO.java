/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.UserProfileToCustomer;

/**
 *
 * @author realnot
 */
public interface UserProfileToCustomerDAO {
    
    // Actions --------------------------------------------------------
    /**
     * Returns the user from the database matching the given ID, otherwise null.
     * @param customer_id The ID of the customer & user to be returned.
     * @return The user from the database matching the given ID, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public UserProfileToCustomer find(Integer customer_id) throws DAOException;
    
    /**
     * Create the given uptc in the database. The uptc ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given uptc.
     * @param uptc The uptc to be created in the database.
     * @throws IllegalArgumentException If the uptc ID is not null.
     * @throws DAOException If something fails at database level.
     */
    public void create(UserProfileToCustomer uptc) 
        throws IllegalArgumentException, DAOException;

    /**
     * Returns a list of all Genders from the database ordered by uptc ID. 
     * The list is never null and is empty when the database does not
     * contain any uptc.
     * @return A list of all uptcs from the database ordered by uptc ID.
     * @throws DAOException If something fails at database level.
     */
    public List<UserProfileToCustomer> list() throws DAOException;
    
    /**
     * Update the given uptc in the database. The uptc ID must 
     * not be null, otherwise it will throw IllegalArgumentException.
     * @param uptc The uptc to be updated in the database.
     * @throws IllegalArgumentException If the uptc ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(UserProfileToCustomer uptc) 
        throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given uptc from the database. After deleting, 
     * the DAO will set the ID of the given uptc to null.
     * @param uptc The uptc to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(UserProfileToCustomer uptc) throws DAOException;
    
    /**
     * Returns true if the given uptc exist in the database.
     * @param customer_id The customer_id to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existUPTC(Integer customer_id) throws DAOException;
    
}
