/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;

import model.Role;

/**
 *
 * @author realnot
 */
public interface RoleDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the role from the database matching the given ID, otherwise null.
     * @param role_id The ID of the role to be returned.
     * @return the role from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Role find(Integer role_id) throws DAOException;
    
    /**
     * Returns the role from the database matching the given name, otherwise null.
     * @param role_name The name of the role to be returned.
     * @return the role from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Role find(String role_name) throws DAOException;
    
    /**
     * Create the given role in the database. The role ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given role.
     * @param role The role to be created in the database.
     * @throws IllegalArgumentException If the role ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Role role) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by role ID. 
     * The list is never null and is empty when the database does not
     * contain any role.
     * @return A list of all roles from the database ordered by role ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Role> list() throws DAOException;
    
    /**
     * Update the given role in the database. The role ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param role The role to be updated in the database.
     * @throws IllegalArgumentException If the role ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(Role role) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given role from the database. After deleting, 
     * the DAO will set the ID of the given role to null.
     * @param role The role to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(Role role) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given role exist in the database.
     * @param role The role which is to be checked in the database.
     * @return True if the given role exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existRole(String role) throws DAOException;
    
}
