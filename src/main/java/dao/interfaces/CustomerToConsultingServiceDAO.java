/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.CustomerToConsultingService;

/**
 *
 * @author realnot
 */
public interface CustomerToConsultingServiceDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the customer from the database matching the given ID, otherwise null.
     * @param ctcs_id The ID of the CustomerToConsultingService relationship.
     * @return the customer from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public CustomerToConsultingService find(Integer customer_id) throws DAOException;
    
    /**
     * Create the given ctcs in the database. The ctcs ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given ctcs.
     * @param ctcs The ctcs to be created in the database.
     * @throws IllegalArgumentException If the ctcs ID is not null.
     * @throws DAOException If something fails at database level.
     */
    public void create(CustomerToConsultingService ctcs) 
        throws IllegalArgumentException, DAOException;

    /**
     * Returns a list of all CTBPs from the database ordered by ctcs ID. 
     * The list is never null and is empty when the database does not
     * contain any ctcs.
     * @return A list of all ctcss from the database ordered by ctcs ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerToConsultingService> list() throws DAOException;
    
    /**
     * Returns a list of all CTBPs from the database ordered by ctcs ID. 
     * The list is never null and is empty when the database does not
     * contain any ctcs.
     * @param ctcs the id of to customer you want find
     * @return A list of all ctcss from the database ordered by ctcs ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerToConsultingService> list(Integer ctcs) throws DAOException;
    
    /**
     * Update the given ctcs in the database. The ctcs ID must 
     * not be null, otherwise it will throw IllegalArgumentException.
     * @param ctcs The ctcs to be updated in the database.
     * @throws IllegalArgumentException If the ctcs ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(CustomerToConsultingService ctcs) 
        throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given ctcs from the database. After deleting, 
     * the DAO will set the ID of the given ctcs to null.
     * @param ctcs The ctcs to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(CustomerToConsultingService ctcs) throws DAOException;
    
    /**
     * Returns true if the given ctcs exist in the database.
     * @param ctcs_id The customer_id to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existCTCS(Integer ctcs_id) throws DAOException;
}
