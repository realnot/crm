/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.Taxonomy;

/**
 *
 * @author realnot
 */
public interface TaxonomyDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the tag from the database matching the given ID, otherwise null.
     * @param tag_id The ID of the tag to be returned.
     * @return the tag from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Taxonomy find(Integer tag_id) throws DAOException;
    
    /**
     * Returns the tag from the database matching the given name, otherwise null.
     * @param tag_name The name of the tag to be returned.
     * @return the tag from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Taxonomy find(String tag_name) throws DAOException;
    
    /**
     * Create the given tag in the database. The tag ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given tag.
     * @param tag The tag to be created in the database.
     * @throws IllegalArgumentException If the tag ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Taxonomy tag) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by tag ID. 
     * The list is never null and is empty when the database does not
     * contain any tag.
     * @return A list of all tags from the database ordered by tag ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Taxonomy> list() throws DAOException;
    
    /**
     * Update the given tag in the database. The tag ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param tag The tag to be updated in the database.
     * @throws IllegalArgumentException If the tag ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(Taxonomy tag) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given tag from the database. After deleting, 
     * the DAO will set the ID of the given tag to null.
     * @param tag The tag to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(Taxonomy tag) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given tag exist in the database.
     * @param tag The tag which is to be checked in the database.
     * @return True if the given tag exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existTaxonomy(String tag_name) throws DAOException;
}
