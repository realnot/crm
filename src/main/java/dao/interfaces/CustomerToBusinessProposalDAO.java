/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.CustomerToBusinessProposal;

/**
 *
 * @author realnot
 */
public interface CustomerToBusinessProposalDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the customer from the database matching the given ID, otherwise null.
     * @param query
     * @param id The ID of the CustomerToBusinessProposal relationship.
     * @return the customer from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public CustomerToBusinessProposal find(Integer customer_id) throws DAOException;
    
    
    /**
     * Create the given ctbp in the database. The ctbp ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given ctbp.
     * @param ctbp The ctbp to be created in the database.
     * @throws IllegalArgumentException If the ctbp ID is not null.
     * @throws DAOException If something fails at database level.
     */
    public void create(CustomerToBusinessProposal ctbp) 
        throws IllegalArgumentException, DAOException;

    /**
     * Returns a list of all CTBPs from the database ordered by ctbp ID. 
     * The list is never null and is empty when the database does not
     * contain any ctbp.
     * @return A list of all ctbps from the database ordered by ctbp ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerToBusinessProposal> list() throws DAOException;
    
    /**
     * Returns a list of all CTBPs from the database ordered by ctbp ID. 
     * The list is never null and is empty when the database does not
     * contain any ctbp.
     * @param ctbp_id
     * @return A list of all ctbps from the database ordered by ctbp ID.
     * @throws DAOException If something fails at database level.
     */
    public List<CustomerToBusinessProposal> list(Integer ctbp_id) throws DAOException;
    
    /**
     * Update the given ctbp in the database. The ctbp ID must 
     * not be null, otherwise it will throw IllegalArgumentException.
     * @param ctbp The ctbp to be updated in the database.
     * @throws IllegalArgumentException If the ctbp ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(CustomerToBusinessProposal ctbp) 
        throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given ctbp from the database. After deleting, 
     * the DAO will set the ID of the given ctbp to null.
     * @param ctbp The ctbp to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(CustomerToBusinessProposal ctbp) throws DAOException;
    
    /**
     * Returns true if the given ctbp exist in the database.
     * @param customer_id The customer_id to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existCTBP(Integer ctbp_id) throws DAOException;
}
