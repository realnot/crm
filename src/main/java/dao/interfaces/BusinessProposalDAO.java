/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;

import model.BusinessProposal;

/**
 *
 * @author realnot
 */
public interface BusinessProposalDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the business proposal from the database matching the given ID, otherwise null.
     * @param proposal_id proposal_id The ID of the business proposal to be returned.
     * @return the business proposal from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public BusinessProposal find(Integer proposal_id) throws DAOException;
    
    /**
     * Returns the business proposal from the database matching the given name, otherwise null.
     * @param business proposal_name The name of the business proposal to be returned.
     * @return the business proposal from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public BusinessProposal find(String proposal_name) throws DAOException;
    
    /**
     * Create the given business proposal in the database. The business proposal ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given business proposal.
     * @param business proposal The business proposal to be created in the database.
     * @throws IllegalArgumentException If the business proposal ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(BusinessProposal business_proposal) 
            throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all business proposals from the database ordered by business proposal ID. 
     * The list is never null and is empty when the database does not
     * contain any business proposal.
     * @return A list of all business proposals from the database ordered by business proposal ID.
     * @throws DAOException If something fails at database level.
     */
    public List<BusinessProposal> list() throws DAOException;
    
    /**
     * Update the given business proposal in the database. The business proposal ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param business proposal The business proposal to be updated in the database.
     * @throws IllegalArgumentException If the business proposal ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(BusinessProposal business_proposal) 
            throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given business proposal from the database. After deleting, 
     * the DAO will set the ID of the given business proposal to null.
     * @param business proposal The business proposal to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(BusinessProposal business_proposal) 
            throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given business proposal exist in the database.
     * @param business proposal The business proposal which is to be checked in the database.
     * @return True if the given business proposal exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existBusinessProposal(String business_proposal) throws DAOException;
    
}
