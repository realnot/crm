/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;

import model.UserProfile;

/**
 *
 * @author realnot
 */
public interface UserProfileDAO {

    // Actions --------------------------------------------------------
    /**
     * Returns the user from the database matching the given ID, otherwise null.
     * @param gender_id The ID of the user to be returned.
     * @return The user from the database matching the given ID, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public UserProfile find(Integer user_id) throws DAOException;
    
    /**
     * Returns the gender from the database matching the given gender name,
     * otherwise null.
     * @param gender_name The email of the user to be returned.
     * @return The gender from the database matching the given name
     * otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public UserProfile find(String username) throws DAOException;
    
    /**
     * Create the given gender in the database. The gender ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given gender.
     * @param gender The gender to be created in the database.
     * @throws IllegalArgumentException If the gender ID is not null.
     * @throws DAOException If something fails at database level.
     */
    public void create(UserProfile user_profile) 
        throws IllegalArgumentException, DAOException;

    /**
     * Returns a list of all Genders from the database ordered by gender ID. 
     * The list is never null and is empty when the database does not
     * contain any gender.
     * @return A list of all genders from the database ordered by gender ID.
     * @throws DAOException If something fails at database level.
     */
    public List<UserProfile> list() throws DAOException;
    
    /**
     * Update the given gender in the database. The gender ID must 
     * not be null, otherwise it will throw IllegalArgumentException.
     * @param gender The gender to be updated in the database.
     * @throws IllegalArgumentException If the gender ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(UserProfile user_profile) 
        throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given gender from the database. After deleting, 
     * the DAO will set the ID of the given gender to null.
     * @param gender The gender to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(UserProfile user_profile) throws DAOException;
    
    /**
     * Returns true if the given gender exist in the database.
     * @param gender The email address which is to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existProfile(Integer user_id) throws DAOException;
    
    /**
     * Returns true if the given gender exist in the database.
     * @param gender The email address which is to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existUsername(String username) throws DAOException;
    
    /**
     * Returns true if the given gender exist in the database.
     * @param gender The email address which is to be checked in the database.
     * @return True if the given email address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existEmail(String email) throws DAOException;
}
