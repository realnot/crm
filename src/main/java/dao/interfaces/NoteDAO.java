/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaces;

import dao.DAOException;
import java.util.List;
import model.Note;

/**
 *
 * @author realnot
 */
public interface NoteDAO {
    
    // Actions --------------------------------------------------------
    
    /**
     * Returns the note from the database matching the given ID, otherwise null.
     * @param note_id The ID of the note to be returned.
     * @return the note from the database matching the given ID, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Note find(Integer note_id) throws DAOException;
    
    /**
     * Returns the note from the database matching the given name, otherwise null.
     * @param note_topic The name of the note to be returned.
     * @return the note from the database matching the given name, otherwise null.
     * @exception DAOException If something fails at database level.
     */
    public Note find(String note_topic) throws DAOException;
    
    /**
     * Create the given note in the database. The note ID must be 
     * null, otherwise it will throw IllegalArgumentException. After 
     * creating, the DAO will set the obtained ID in the given note.
     * @param note The note to be created in the database.
     * @throws IllegalArgumentException If the note ID is not null.
     * @throws DAOException If something fails at database level 
     */
    public void create(Note note) throws IllegalArgumentException, DAOException;
    
    /**
     * Returns a list of all Cities from the database ordered by note ID. 
     * The list is never null and is empty when the database does not
     * contain any note.
     * @return A list of all notes from the database ordered by note ID.
     * @throws DAOException If something fails at database level.
     */
    public List<Note> list() throws DAOException;
    
    /**
     * Update the given note in the database. The note ID must not be null, 
     * otherwise it will throw IllegalArgumentException.
     * @param note The note to be updated in the database.
     * @throws IllegalArgumentException If the note ID is null.
     * @throws DAOException If something fails at database level.
     */
    public void update(Note note) throws IllegalArgumentException, DAOException;
    
    /**
     * Delete the given note from the database. After deleting, 
     * the DAO will set the ID of the given note to null.
     * @param note The note to be deleted from the database.
     * @throws DAOException If something fails at database level.
     */
    public void delete(Note note) throws IllegalArgumentException, DAOException;
    
     /**
     * Returns true if the given note exist in the database.
     * @param note_id The note which is to be checked in the database.
     * @return True if the given note exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existNote(Integer note_id) throws DAOException;
}
