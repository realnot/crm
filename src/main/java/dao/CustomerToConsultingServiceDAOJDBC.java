/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.DAOUtil.prepareStatement;
import dao.interfaces.CustomerToConsultingServiceDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CustomerToConsultingService;

/**
 *
 * @author realnot
 */
public class CustomerToConsultingServiceDAOJDBC 
        implements CustomerToConsultingServiceDAO {
    
    // Constants ------------------------------------------------------
    private static final String SQL_FIND_BY_CUSTOMER_ID
            = "SELECT * FROM customer_to_service "
            + "INNER JOIN consulting_service "
                + "ON customer_to_service.ctcs_service_id = consulting_service.service_id "
            + "INNER JOIN customer "
                + "ON customer_to_service.ctcs_customer_id = customer.customer_id "
            + "WHERE customer_to_service.ctcs_customer_id = ?";
    private static final String SQL_LIST_ORDER_BY_ID
            = "SELECT * FROM consulting_service "
            + "INNER JOIN customer_to_service "
                + "ON customer_to_service.ctcs_service_id = consulting_service.service_id "
            + "INNER JOIN customer "
                + "ON customer_to_service.ctcs_customer_id = customer.customer_id "
            + "ORDER BY customer_to_service.ctcs_id";
    private static final String SQL_FIND_LIST_ORDER_BY_ID
            = "SELECT * FROM consulting_service "
            + "INNER JOIN customer_to_service "
                + "ON customer_to_service.ctcs_service_id = consulting_service.service_id "
            + "INNER JOIN customer "
                + "ON customer_to_service.ctcs_customer_id = customer.customer_id "
            + "WHERE customer.customer_id = ? "
            + "ORDER BY customer_to_service.ctcs_id";
    private static final String SQL_INSERT
            = "INSERT INTO customer_to_service(ctcs_customer_id, ctcs_service_id) "
            + "VALUES (?, ?)";
    private static final String SQL_UPDATE
            = "UPDATE customer_to_service SET ctcs_customer_id = ?, ctcs_service_id = ? "
            + "WHERE ctcs_id = ?";
    private static final String SQL_DELETE
            = "DELETE FROM customer_to_service WHERE service_id = ?";
    private static final String SQL_EXIST_CUSTOMER
            = "SELECT * FROM customer_to_service WHERE service_id = ?";
    
    // Vars -----------------------------------------------------------
    private DAOFactory daoFactory;

    // Constructors ---------------------------------------------------
    /**
     * Construct a CustomerToConsultingService DAO for the given DAOFactory. 
     * Package private so that it can be constructed inside the DAO package only.
     *
     * @param daoFactory The DAOFactory to construct this 
     * CustomerToConsultingService DAO for.
     */
    CustomerToConsultingServiceDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    // Actions ---------------------------------------------------------
    @Override
    public CustomerToConsultingService find(Integer customer_id) throws DAOException {
         return find(SQL_FIND_BY_CUSTOMER_ID, customer_id);
    }
   
    /**
     * Returns the ctcs from the database matching the given SQL query 
     * with the given values.
     *
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The ctcs from the database matching the given SQL query 
     * with the given values.
     * @throws DAOException If something fails at database level.
     */
    private CustomerToConsultingService find(String sql, Object... values) 
            throws DAOException {
        CustomerToConsultingService ctcs = null;

        try (
            Connection conn = daoFactory.getConnection();
            PreparedStatement stmt
            = prepareStatement(conn, sql, false, values);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            if (rs.next()) {
                ctcs = map(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctcs;
    }
    
    @Override
    public void create(CustomerToConsultingService ctcs)
            throws IllegalArgumentException, DAOException {
        
        if (ctcs.getCTCSID() != null) {
            throw new IllegalArgumentException(
                    "CTBP is already created, the CTBP is not null.");
        }

        Object[] values = {
            ctcs.getCTCSCustomerID(),
            ctcs.getCTCSServiceID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_INSERT, true, values);) {
            System.out.println(stmt);
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating ctcs failed, "
                        + "no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    ctcs.setCustomerID(generatedKeys.getInt(1));
                } else {
                    throw new DAOException("Creating ctcs failed, "
                            + "no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<CustomerToConsultingService> list() throws DAOException {
        List<CustomerToConsultingService> ctcsList = new ArrayList<>();

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = conn.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            while (rs.next()) {
                ctcsList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctcsList;
    }
    
    @Override
    public List<CustomerToConsultingService> list(Integer ctcs) throws DAOException {
        List<CustomerToConsultingService> ctcsList = new ArrayList<>();

        if (ctcs == null) {
            throw new IllegalArgumentException("The ctcs is null.");
        }
            
        Object[] values = {
            ctcs
        };
        
        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                    = prepareStatement(conn, SQL_FIND_LIST_ORDER_BY_ID, false, values);
                ResultSet rs = stmt.executeQuery();) {
            System.out.println(stmt);
            while (rs.next()) {
                ctcsList.add(map(rs));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return ctcsList;
    }

    @Override
    public void update(CustomerToConsultingService ctcs) throws DAOException {
        if (ctcs.getCTCSID() == null) {
            throw new IllegalArgumentException("The ctcs is not created yet, "
                    + "the ctcs ID is null.");
        }

        Object[] values = {
            ctcs.getCTCSCustomerID(),
            ctcs.getCTCSServiceID(),
            ctcs.getCTCSID()
        };

        try (
                Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection,
                        SQL_UPDATE, false, values);) {
            System.out.println(statement);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Updating ctcs failed, no rows affected!");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(CustomerToConsultingService ctcs) throws DAOException {
        Object[] values = {
            ctcs.getCustomerID()
        };

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_DELETE, false, values);) {
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException(
                        "Deleting ctcs failed, no rows affected.");
            } else {
                ctcs.setCustomerID(null);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean existCTCS (Integer customer_id) 
            throws DAOException {
        
        Object[] values = {
            customer_id
        };

        boolean exist = false;

        try (
                Connection conn = daoFactory.getConnection();
                PreparedStatement stmt
                = prepareStatement(conn, SQL_EXIST_CUSTOMER, false, values);
                ResultSet rs = stmt.executeQuery();) {
            exist = rs.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return exist;
    }
    
    // Helpers --------------------------------------------------------
    
    /**
     * Map the current row of the given ResultSet to a CustomerToConsultingService.
     *
     * @param resultSet The ResultSet of which the current row is to be mapped
     * to a ctcs.
     * @return The mapped ctcs from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static CustomerToConsultingService map(ResultSet rs) throws SQLException {
        CustomerToConsultingService ctcs = new CustomerToConsultingService();
        ctcs.setCTCSID(rs.getInt("ctcs_id"));
        ctcs.setCTCSCustomerID(rs.getInt("ctcs_customer_id"));
        ctcs.setCTCSServiceID(rs.getInt("ctcs_service_id"));
        ctcs.setCustomerID(rs.getInt("customer_id"));
        ctcs.setCustomerName(rs.getString("customer_name"));
        ctcs.setCustomerEmail(rs.getString("customer_email"));
        ctcs.setCustomerCreatedBy(rs.getInt("customer_created_by"));
        ctcs.setCustomerAssignedTo(rs.getInt("customer_assigned_to"));
        ctcs.setCustomerCategory(rs.getInt("customer_category"));
        ctcs.setCustomerType(rs.getInt("customer_type"));
        ctcs.setCustomerContactName(rs.getString("customer_contact_name"));
        ctcs.setCustomerContactSurname(rs.getString("customer_contact_surname"));
        ctcs.setCustomerContactAddress(rs.getString("customer_contact_address"));
        ctcs.setCustomerContactPhone(rs.getString("customer_contact_phone"));
        ctcs.setCustomerCreatedOn(rs.getTimestamp("customer_created_on"));
        ctcs.setServiceID(rs.getInt("service_id"));
        ctcs.setServiceName(rs.getString("service_name"));
        ctcs.setServiceDesc(rs.getString("service_desc"));
        return ctcs;
    }
}
