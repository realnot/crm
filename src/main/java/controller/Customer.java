/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.interfaces.CustomerViewDAO;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author realnot
 */
@WebServlet("/Customer")
public class Customer extends HttpServlet {

    private static final long serialVersionUID = 1L;
        
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
            throws ServletException, IOException {
        
        // Prepare messages.
        Map<String, String> messages = new HashMap<String, String>();
        req.setAttribute("messages", messages);
        
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        if (crm == null) {
            messages.put("dao", "DAOFactory not obtainerd: " + crm);
        }
        
        // Obtain the DAOs
        CustomerViewDAO customerDAO = crm.getCustomerViewDAO();
        if (customerDAO == null) {
             messages.put("dao", "DAO object not obtained!");
        }
        
        List<model.CustomerView> customers = customerDAO.list();       
        req.setAttribute("customersViewList", customers);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/Customer.jsp");
        rd.forward(req, res);
    }
}
