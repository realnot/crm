/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.interfaces.BusinessProposalDAO;
import dao.interfaces.MeetingDAO;
import dao.interfaces.NoteDAO;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.BusinessProposal;
import model.Meeting;
import model.Note;
import model.UserProfile;

/**
 *
 * @author realnot
 */
@WebServlet("/Dashboard")
public class Dashboard extends HttpServlet {

    private static final long serialVersionUID = 1L;
        
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
            throws ServletException, IOException {
        
        // Prepare messages.
        Map<String, String> messages = new HashMap<String, String>();
        req.setAttribute("messages", messages);
        
        // Obtain the login data from the session.
        HttpSession session = req.getSession();
        UserProfile profile = (UserProfile)session.getAttribute("login");

        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        if (crm == null) {
            messages.put("dao", "DAOFactory not obtainerd: " + crm);
        }
        
        // Obtain the DAOs
        MeetingDAO meetingDAO = crm.getMeetingDAO();
        BusinessProposalDAO proposalDAO = crm.getBusinessProposalDAO();
        NoteDAO noteDAO = crm.getNoteDAO();
        
        if (meetingDAO == null || proposalDAO == null || noteDAO == null) {
             messages.put("dao", "DAO object not obtained!");
        }
        
        Integer id = profile.getUserID();
        System.out.println(id);
        
        List<Meeting> meetings = meetingDAO.list(id);
        List<BusinessProposal> proposals = proposalDAO.list();
        List<Note> notes = noteDAO.list();
        
        req.setAttribute("meetingList", meetings);
        req.setAttribute("bpList", proposals);
        req.setAttribute("noteList", notes);
        
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/Dashboard.jsp");
        rd.forward(req, res);
    }
}
