/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.interfaces.BusinessProposalDAO;
import dao.interfaces.ConsultingServiceDAO;
import dao.interfaces.CustomerDAO;
import dao.interfaces.CustomerToBusinessProposalDAO;
import dao.interfaces.CustomerToConsultingServiceDAO;
import dao.interfaces.CustomerTypeDAO;
import dao.interfaces.LogDAO;
import dao.interfaces.ProductCategoryDAO;
import dao.interfaces.UserProfileDAO;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BusinessProposal;
import model.ConsultingService;
import model.Customer;
import model.CustomerToBusinessProposal;
import model.CustomerToConsultingService;
import model.CustomerType;
import model.ProductCategory;
import model.UserProfile;

/**
 *
 * @author realnot
 */
@WebServlet("/CustomerServlet")
public class CustomerServlet extends HttpServlet implements CRUD {

    // Constanst -------------------------------------------------------------
    private static final long serialVersionUID = 1L;
    private final String LIST = "/pages/Customer.jsp";
    private final String CREATE = "/pages/CustomerCreate.jsp";
    private final String UPDATE = "/pages/CustomerUpdate.jsp";
    private final String FIND = "/pages/CustomerFind.jsp";
    private final String DETAILS = "/pages/CustomerDetails.jsp";
    private final DAOFactory crm;
    private final UserProfileDAO userDAO;
    private final CustomerDAO customerDAO;
    private final CustomerTypeDAO ctDAO;
    private final ProductCategoryDAO categoryDAO;
    private final ConsultingServiceDAO serviceDAO;
    private final BusinessProposalDAO proposalDAO;
    private final CustomerToConsultingServiceDAO ctcsDAO;
    private final CustomerToBusinessProposalDAO ctbpDAO;
    private final LogDAO logDAO;
    private Map<String, String> messages;
    private RequestDispatcher rd;
    private String page;
    
    // Obtain the DAOs objects
    public CustomerServlet() {
        super();
        // Prepare messages
        messages = new HashMap<String, String>();
        // Get a CRM instance
        crm = DAOFactory.getInstance("crm.jdbc");
        if (crm == null) {
            messages.put("dao", "DAOFactory not obtainerd: " + crm);
        }
        // Ge a UserProfile instance
        userDAO = crm.getUserProfileDAO();
        if (userDAO == crm.getUserProfileDAO()) {
            messages.put("dao", "UserProfileDAO object not obtained!" + userDAO);
        }
        
        // Get an CustomerDAO instance
        customerDAO = crm.getCustomerDAO();
        if (customerDAO == null) {
            messages.put("dao", "customerDAO object not obtained!" + customerDAO);
        }
        // Get a CustomerTypeDAO instance
        ctDAO = crm.getCustomerTypeDAO();
        if (ctDAO == crm.getCustomerTypeDAO()) {
           messages.put("dao", "ctDAO object not obtained" + ctDAO);
        }
        // Get a CustomerCategory instance
        categoryDAO = crm.getProductCategoryDAO();
        if (categoryDAO == crm.getProductCategoryDAO()) {
            messages.put("dao", "categoryDAO object not obtained" + categoryDAO);
        } 
        // 
        serviceDAO = crm.getConsultingServiceDAO();
        if (serviceDAO == crm.getConsultingServiceDAO()) {
            messages.put("dao", "ctcsDAO object not obtained!" + serviceDAO);
        }
        // Get a CustomerToService instance
        ctcsDAO = crm.getCustomerToConsultingServiceDAO();
        if (ctcsDAO == crm.getCustomerToConsultingServiceDAO()) {
            messages.put("dao", "ctcsDAO object not obtained!" + ctcsDAO);
        }
        //
        proposalDAO = crm.getBusinessProposalDAO();
        if (proposalDAO == crm.getBusinessProposalDAO()) {
            messages.put("dao", "proposalDAO object not obtained!" + proposalDAO);
        }
        // Get a CustomerToBusinessProposal instance
        ctbpDAO = crm.getCustomerToBusinessProposalDAO();
        if (ctbpDAO == crm.getCustomerToBusinessProposalDAO()) {
            messages.put("dao", "ctbpDAO object not obtained!" + ctbpDAO);
        }
        // Get a logDAO instance
        logDAO = crm.getLogDAO();
        if(logDAO == null) {
            messages.put("dao", "logDAO object not obtained" + logDAO);
        }
        page = LIST;
    }
    
    // Requests ---------------------------------------------------------------
    
    /**
     * 
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
            throws ServletException, IOException {
        // Prepare messages
        req.setAttribute("messages", messages);
        // GET Requests Handler
        System.out.println("GET request intercepted, parameter page: ");
        System.out.println(req.getParameter("page"));
        System.out.println(req.getParameter("customer_id"));
        switch (req.getParameter("page")) {
            case "list": viewList(req); break;
            case "details": viewDetails(req); break;
            case "create": viewCreate(req); break;
            case "update": viewUpdate(req); break;
            case "find": viewFind(req); break;
        }
        System.out.println(page);
        // Forwards the request
        rd = getServletContext().getRequestDispatcher(page);
        rd.forward(req, res);
    }
    
    /**
     * 
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) 
            throws ServletException, IOException {
        // Sets the attributes
        req.setAttribute("messages", messages);
        // POST Requests Handler
        switch(req.getParameter("action")) {
            case "save":
            case "update": save (req); break;
            case "find": find (req); break;
            case "delete": delete (req); break;
        }
        // Dispatch the reuqest to the properly page
        rd = getServletContext().getRequestDispatcher(page);
        rd.forward(req, res);
    }

    // GET Actions ------------------------------------------------------------
    
    /**
     *
     * @param req 
     */
    @Override
    public void viewList (HttpServletRequest req) {
        this.page = LIST;
        List<Customer> customers = customerDAO.list();   
        req.setAttribute("list",  customers);
    }
    
    /**
     * The method set the properly page where TO forward the customer and obtains
     * the role list for fill the select/option form.
     * @param req The HttpServletRequest.
     */
    @Override
    public void viewCreate (HttpServletRequest req) {
        // Set the dispatch page
        this.page = CREATE;
        // Obtains the consultants list for fill the form
        List<UserProfile> users = userDAO.list();
        req.setAttribute("users", users);
        // Obtain the CustomerTypeDAO
        List<CustomerType> types = ctDAO.list();
        req.setAttribute("types", types);
        // Obtain the Customer Product Category
        List<ProductCategory> categories = categoryDAO.list();
        req.setAttribute("categories", categories);
        // Obtains the business proposals for fills the form
        List<BusinessProposal> proposals = proposalDAO.list();
        req.setAttribute("proposals", proposals);
        // Obtains the consulting services for fills the form
        List<ConsultingService> services = serviceDAO.list();
        req.setAttribute("services", services);
    }
    
    /**
     * 
     * @param req
     */
    @Override
    public void viewUpdate (HttpServletRequest req) {
        // Sets the forward page
        this.page = UPDATE;
        /**
         * Obtain the Customer object and set the attribute to fills the form.
         */
        int customerID = parseInt(messages, req.getParameter("customer_id"));
        Customer customer = customerDAO.find(customerID);
        // Find the user assigned/created to this customer and get their names
        UserProfile user_assigned = userDAO.find(customer.getCustomerAssignedTo());
        UserProfile user_created_by = userDAO.find(customer.getCustomerCreatedBy());
        customer.setStringCustomerAssignedTo(user_assigned.getDisplayName());
        customer.setStringCustomerCreatedBy(user_created_by.getDisplayName());
        // Find the customer type and get it's name
        CustomerType type = ctDAO.find(customer.getCustomerType());
        customer.setStringCustomerType(type.getCustomerTypeName());
        // Find the customer category and get it's name
        ProductCategory category = categoryDAO.find(customer.getCustomerCategory());
        customer.setStringCustomerCategory(category.getProductName());
        req.setAttribute("details", customer);
        /**
         * Obtains the customer_to_service data (services selected by the user).
         */
        CustomerToConsultingService ctcs = ctcsDAO.find(customerID);
        req.setAttribute("ctcs", ctcs);
        /** 
         * Obtains the customer_to_business_proposal_data (business proposal
         * selected by the user.
        */
        CustomerToBusinessProposal ctbp = ctbpDAO.find(customerID);
        req.setAttribute("ctbp", ctbp);
        /**
         * Obtains the lists.
         */
        // Consultant
        List<UserProfile> users = userDAO.list();
        req.setAttribute("users", users);
        // Obtain the CustomerTypeDAO (customer type)
        List<CustomerType> types = ctDAO.list();
        req.setAttribute("types", types);
        // Obtain the CustomerProductDAO (customer category)
        List<ProductCategory> categories = categoryDAO.list();
        req.setAttribute("categories", categories);
         // Obtains the business proposals for fills the form
        List<BusinessProposal> proposals = proposalDAO.list();
        req.setAttribute("proposals", proposals);
        // Obtains the consulting services for fills the form
        List<ConsultingService> services = serviceDAO.list();
        req.setAttribute("services", services);
        
    }
    
    /**
     * 
     * @param req
     */
    @Override
    public void viewFind(HttpServletRequest req) {
        this.page = FIND;
    }
   
    /**
     * 
     * @param req
     */
    @Override
    public void viewDetails(HttpServletRequest req) {
        this.page = DETAILS;
        int customerID = parseInt(messages, req.getParameter("customer_id"));
        // Obtains the customer data
        Customer customer = customerDAO.find(customerID);
        // Find the user assigned/created to this customer and get their names
        UserProfile user_assigned = userDAO.find(customer.getCustomerAssignedTo());
        UserProfile user_created_by = userDAO.find(customer.getCustomerCreatedBy());
        customer.setStringCustomerAssignedTo(user_assigned.getDisplayName());
        customer.setStringCustomerCreatedBy(user_created_by.getDisplayName());
        req.setAttribute("details", customer);
        // Obtains the customer_to_service data
        List<CustomerToConsultingService> ctcs = ctcsDAO.list(customerID);
        req.setAttribute("ctcs", ctcs);
        // Obtains the customer_to_business_proposal_data
        List<CustomerToBusinessProposal> ctbp = ctbpDAO.list(customerID);
        req.setAttribute("ctbp", ctbp);
        // Registers the action performed by the logged customer.
        String desc = "Request of details for the customer " + customer.getCustomerName();
        logAction(req, logDAO, desc);
    }
    
    // POST Actions -----------------------------------------------------------
    
    /**
     * 
     * @param req 
     */
    @Override
    public void save (HttpServletRequest req) {
        // Variables
        page = LIST;
        String desc = "";
        int customerID,  proposalID, serviceID, ctbpID, ctcsID;
        // Create a new User Object
        Customer searched_customer;
        Customer customer = new Customer();
        CustomerToBusinessProposal ctbp = new CustomerToBusinessProposal();
        CustomerToConsultingService ctcs = new CustomerToConsultingService();
        // Customer personal info
        customer.setCustomerName(req.getParameter("customer-name"));
        customer.setCustomerEmail(req.getParameter("customer-email"));
        customer.setCustomerType(parseInt(messages, req.getParameter("customer-type")));
        customer.setCustomerCategory(parseInt(messages, req.getParameter("product-category")));
        customer.setCustomerCreatedBy(parseInt(messages, req.getParameter("created-by")));
        customer.setCustomerAssignedTo(parseInt(messages, req.getParameter("assigned-to")));
        // Customer Agent data
        customer.setCustomerContactName(req.getParameter("agent-name"));
        customer.setCustomerContactSurname(req.getParameter("agent-surname"));
        customer.setCustomerContactAddress(req.getParameter("agent-address"));
        customer.setCustomerContactPhone(req.getParameter("agent-phone"));
        
        proposalID = parseInt(messages, req.getParameter("proposalID"));
        serviceID = parseInt(messages, req.getParameter("serviceID"));
        // Intercepts the properly request
        switch(req.getParameter("action")) {
            case "save":
                // Save the customer object into the DB
                customerDAO.create(customer);
                // Find the customer just created and get its ID
                searched_customer = customerDAO.find(req.getParameter("customer-email"));
                customerID = searched_customer.getCustomerID();
                // Maps a CustomerID to BusinessProposalID
                ctbp.setCTBPCustomerID(customerID);
                ctbp.setCTBPProposalID(proposalID);
                ctbpDAO.create(ctbp);
                // Update the CustomerToConsultingServices
                ctcs.setCTCSCustomerID(customerID);
                ctcs.setCTCSServiceID(serviceID);
                ctcsDAO.create(ctcs);
                // Set the log description
                desc = "Created the customer " + customer.getCustomerName();
                break;
            case "update":
                customerID = parseInt(messages, req.getParameter("customerID"));
                ctbpID = parseInt(messages, req.getParameter("ctbpID"));
                ctcsID = parseInt(messages, req.getParameter("ctcsID"));
                searched_customer = customerDAO.find(customerID);
                customer.setCustomerCreatedOn(searched_customer.getCustomerCreatedOn());
                customer.setCustomerID(searched_customer.getCustomerID());
                customerDAO.update(customer);
                
                // Update the CustomerToBusinessProposalTable
                CustomerToBusinessProposal searched_ctbp = ctbpDAO.find(ctbpID);
                System.out.println("CustomerID: " + searched_ctbp.getCTBPCustomerID());
                System.out.println("ProposalID: " + searched_ctbp.getCTBPProposalID());
                System.out.println("ctbpID: " + searched_ctbp.getCTBPID());
                ctbp.setCTBPCustomerID(customerID);
                ctbp.setCTBPProposalID(proposalID);
                ctbp.setCTBPID(ctbpID);
                ctbpDAO.update(ctbp);
                
                // Update the CustomerToConsultingServices
                CustomerToConsultingService searched = ctcsDAO.find(ctcsID);
                ctcs.setCTCSCustomerID(customerID);
                ctcs.setCTCSServiceID(serviceID);
                ctcs.setCTCSID(ctcsID);
                ctcsDAO.update(ctcs);
                
                desc = "Updated the customer " + customer.getCustomerName();
                //
                break;
        }
        // Obtain the updated list
        List<Customer> customers = customerDAO.list();
        req.setAttribute("list", customers);
        // Register the action performed by the logged customer.
        logAction(req, logDAO, desc);
    }
    
    /**
     * 
     * @param req
     */
    @Override
    public void find (HttpServletRequest req) {
        // Set the dispatch page
        page = DETAILS;
        int customerID = parseInt(messages, req.getParameter("customerCode"));
        // Obtains the customer data
        Customer customer = customerDAO.find(customerID);
        // Find the user assigned/created to this customer and get their names
        UserProfile user_assigned = userDAO.find(customer.getCustomerAssignedTo());
        UserProfile user_created_by = userDAO.find(customer.getCustomerCreatedBy());
        customer.setStringCustomerAssignedTo(user_assigned.getDisplayName());
        customer.setStringCustomerCreatedBy(user_created_by.getDisplayName());
        req.setAttribute("details", customer);
        // Obtains the customer_to_service data
        List<CustomerToConsultingService> ctcs = ctcsDAO.list(customerID);
        req.setAttribute("ctcs", ctcs);
        // Obtains the customer_to_business_proposal_data
        List<CustomerToBusinessProposal> ctbp = ctbpDAO.list(customerID);
        req.setAttribute("ctbp", ctbp);
        // Register the action performed by the logged customer.
        String desc = "Searched the customer id " + customer.getCustomerID();
        logAction(req, logDAO, desc);
    }
    
    /**
     * 
     * @param req 
     */
    @Override
    public void delete (HttpServletRequest req) {
        this.page = LIST;
        int customerID = parseInt(messages, req.getParameter("customerID"));
        // Find the meeting to delete & delete it
        Customer customer = customerDAO.find(customerID);
        customerDAO.delete(customer);
        // Obtain the updated list
        List<Customer> customers = customerDAO.list();
        req.setAttribute("list", customers);
        // Register the action performed by the logged customer.
        String desc = "Deleted the customer " + customer.getCustomerName();
        logAction(req, logDAO, desc);
    }
}