/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.interfaces.LogDAO;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Log;
import model.UserProfile;

/**
 *
 * @author realnot
 */
public interface CRUD {
    
    // Actions --------------------------------------------------------
    
    /**
     * 
     * @param req 
     */
    public void save (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void find (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void delete (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void viewList (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void viewDetails (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void viewCreate (HttpServletRequest req);
    
    /**
     * 
     * @param req 
     */
    public void viewUpdate (HttpServletRequest req);
    
    /**
     * @param req 
     */
    public void viewFind (HttpServletRequest req);
    
    /**
     * 
     * @param req
     * @param logDAO
     * @param logDesc 
     */
    default void logAction(HttpServletRequest req, LogDAO logDAO, String logDesc) {
        // Get the logged user
        HttpSession session = req.getSession();
        UserProfile login = (UserProfile) session.getAttribute("login");
        // Register the action performed by the logged user.
        Log log = new Log();
        log.setReferedTo(login.getUserID());
        log.setLogDesc(logDesc);
        logDAO.create(log);
    }
    
    /**
     * 
     * @param messages
     * @param value
     * @return 
     */
    default Timestamp parseTimestamp(Map<String, String> messages, String value) {
        Timestamp timestamp = null;
        
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(value);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        } catch(Exception e) {
            // this generic but you can control another types of exception
            System.out.println("I can\'t parse the timestamp string.");
        }
        return timestamp;
    }
    
    /**
    * Convert a String to int
    * @param value the value to be converted
    * @param messages the error message in case of failure
    * @return the converted value or -1
    */
    default int parseInt(Map<String, String> messages, String value) {
        int convertedValue = -1;

        try {
            convertedValue = Integer.parseInt(value);
        } catch(NumberFormatException e) {
            messages.put("conversion", "Impossible to convert the String!" + e);
        }
        return convertedValue;
    }
}