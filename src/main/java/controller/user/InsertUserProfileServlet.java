/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.interfaces.*;
import dao.DAOFactory;
import model.UserProfile;
import model.Role;

/**
 *
 * @author realnot
 */
@WebServlet("/user/insertUserProfile")
public class InsertUserProfileServlet extends HttpServlet {
    
    private final String URL = "/WEB-INF/user/insertUserProfile.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) 
                throws ServletException, IOException {
        // Preprocess request: we actually don't need to do any business 
        // stuff, so just display JSP.
        req.getRequestDispatcher(URL).forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) 
                throws ServletException, IOException {
        // Postprocess request: gather and validate submitted data 
        // and display result in same JSP.

        // Prepare messages.
        Map<String, String> messages = new HashMap<String, String>();
        req.setAttribute("messages", messages);
        
        // Get the user profile parameters.
        String prm_username = req.getParameter("username");
        String prm_password = req.getParameter("password");
        String prm_email = req.getParameter("email");
        String prm_role = "regular";
        
        // tmp variables
        int role_id = -1;
        
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        if (crm == null) {
            messages.put("dao", "DAOFactory not obtainerd: " + crm);
        }
        
        // Obtain UserProfileDAO.
        UserProfileDAO profileDAO = crm.getUserProfileDAO();
        if (profileDAO == null) {
            messages.put("dao", "UserProfileDAO not obtained: " + profileDAO);
        }
        
        // Check if the email exists.
        if(profileDAO.existUsername(prm_username)) {
            messages.put("username", "The username " + prm_username 
                    + " already exists.");        
        } 
        // Check if the username exists.
        if(profileDAO.existEmail(prm_email)) {
            messages.put("email", "The  email " + prm_email 
                    + " already exists.");
        }
        
        // Obtain RoleDAO.
        RoleDAO roleDAO = crm.getRoleDAO();
        if (roleDAO == null) {
            messages.put("dao", "RoleDAO not obtained: " + roleDAO);
        } else {
            // Get the RoleID from a the role parameter given by user
            Role obj = roleDAO.find(prm_role);
            if(obj == null) {
                messages.put("role", "Attempt to retrive RoleID Failed!");
            } else {
                role_id = obj.getRoleID();
            }
        }
       
        // No validation errors? Do the business job!
        if (messages.isEmpty()) {
            // Create UserProfile.
            UserProfile profile = new UserProfile();
            // Set the parameter given 
            profile.setUsername(prm_username);
            profile.setPassword(prm_password);
            profile.setEmail(prm_email);
            profile.setRoleID(role_id);
            profileDAO.create(profile);
            messages.put("success", prm_username + " successfully created!");
        }

        req.getRequestDispatcher(URL).forward(req, res);
    }
    
    // Helpers
    
    /**
     * 
     * @param messages
     * @param username
     * @return 
     * Thanks to mkyong.com: http://bit.ly/1yTnoDQ
     */
    public boolean usernameChecker(Map messages, String username) {
        String user_rgx = "^[a-z0-9_-]{3,15}$";
        
        if (username == null || username.trim().isEmpty()) {
            messages.put("username", "Please enter the username");
        } else if (!username.matches(user_rgx)) {
            messages.put("username", "The username is not valid! "
                    + "The username can contains characters, symbols in the "
                    + "list, a-z, 0-9, underscore, hyphen. The length at least "
                    + "3 characters and maximum length of 15.");
        } else 
            return true;
        return false;
    }
    
    /**
     * 
     * @param messages
     * @param password
     * @return 
     * 
     * Thanks to mkyong.com: http://bit.ly/1yTnoDQ
     */
    public boolean passwordChecker(Map messages, String password) {
        String psw_rgx = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
        if (password == null || password.trim().isEmpty()) {
            messages.put("password", "Please enter the password");
        } else if (!password.matches(psw_rgx)) {
            messages.put("password", "The password is not valid! "
                    + "The password must cotains: \n"
                    + "must contains one digit from 0-9\n"
                    + "must contains one lowercase characters\n"
                    + "must contains one uppercase characters\n"
                    + "must contains one special symbols in the list \"@#$%\"\n"
                    + "length at least 6 characters and maximum of 20\n");
        } else 
            return true;
        return false;
    }
    
    /**
     * 
     * @param messages
     * @param email
     * @return 
     */
    public boolean emailChecker(Map messages, String email) {
        String email_rgx = "^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
                + "(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";
        if (email == null || email.trim().isEmpty()) {
            messages.put("email", "Please enter the email");
        } else if (email.matches(email_rgx)) {
            messages.put("email", "The email is not valid!");
        } else 
            return true;
        return false;
    }
}    
