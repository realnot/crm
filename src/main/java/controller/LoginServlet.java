package controller;
 
import dao.DAOFactory;
import dao.interfaces.UserProfileDAO;
import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.UserProfile;
 
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) 
            throws ServletException, IOException {
 
        // get request parameters for userID and password
        String user = req.getParameter("user");
        String pass = req.getParameter("pwd");
        
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain UserProfileDAO.
        UserProfileDAO profileDAO = crm.getUserProfileDAO();
        System.out.println("UserProfileDAO successfully obtained: " + profileDAO);
        
        // Check if the user exists in the DB
        UserProfile login = profileDAO.find(user);
        //System.out.println(login.getUsername());
        
        // if exists, verify the username and password
        if(login != null) {
            if(user.equals(login.getUsername()) && 
               pass.equals(login.getPassword())) {
                HttpSession session = req.getSession();
                session.setAttribute("login", login);
                //setting session to expiry in 30 mins
                session.setMaxInactiveInterval(30*60);
                Cookie username = new Cookie("username", login.getUsername());
                res.addCookie(username);
                //Get the encoded URL string
                String encodedURL = res.encodeRedirectURL("/Dashboard");
                res.sendRedirect(encodedURL);
            } else {
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
                PrintWriter out = res.getWriter();
                out.println("<font color=red>Either username or password is wrong.</font>");
                rd.include(req, res);
            }
        }
    }
}