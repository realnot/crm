/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class UserProfileToCustomer {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer user_id;
    private Integer customer_id;
    
    // Getters/setters ------------------------------------------------
    public Integer getUserID() { return this.user_id; }
    public Integer getCustomerID() { return this.customer_id; }
    
    public void setUserID(Integer value) { this.user_id = value; }
    public void setCustomerID(Integer value) { this.customer_id = value; }
    
}
