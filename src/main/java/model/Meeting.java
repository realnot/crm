/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author realnot
 */
public class Meeting implements Serializable {
        
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer meeting_id;
    private String meeting_topic;
    private String meeting_desc;
    private Date meeting_created_on;
    private Integer meeting_created_by;
    private Integer meeting_refered_to;
    
    // Getters/setters ------------------------------------------------
    public Integer getMeetingID() { return this.meeting_id; }
    public String getMeetingTopic() { return this.meeting_topic; }
    public String getMeetingDesc() { return this.meeting_desc; }
    public Date getMeetingCreatedOn() { return this.meeting_created_on; }
    public Integer getMeetingCreatedBy() { return this.meeting_created_by; }
    public Integer getMeetingReferedTo() { return this.meeting_refered_to; }
    
    public void setMeetingID(Integer value) { this.meeting_id = value; }
    public void setMeetingTopic(String value) { this.meeting_topic = value; }
    public void setMeetingDesc(String value) { this.meeting_desc = value; }
    public void setMeetingCreatedOn(Date value) { this.meeting_created_on = value; }
    public void setMeetingCreatedBy(Integer value) { this.meeting_created_by = value; }
    public void setMeetingReferedTo(Integer value) { this.meeting_refered_to = value; }
}
