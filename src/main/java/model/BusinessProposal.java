/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author realnot
 */
public class BusinessProposal implements Serializable {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer proposal_id;
    private String proposal_name;
    private String proposal_desc;
    private Integer consulting_service;
    private String service_name; 
    
    // Getters/setters ------------------------------------------------
    public Integer getProposalID() { return this.proposal_id; }
    public String getProposalName() { return this.proposal_name; }
    public String getProposalDesc() { return this.proposal_desc; }
    public Integer getConsultingService() { return this.consulting_service; }
    public String getServiceName() { return this.service_name; }
    
    public void setProposalID(Integer value) { this.proposal_id = value; }
    public void setProposalName(String value) { this.proposal_name = value; }
    public void setProposalDesc(String value) { this.proposal_desc = value; }
    public void setConsultingService(Integer value) { this.consulting_service = value; }
    public void setServiceName(String value) { this.service_name = value; }
   
}
