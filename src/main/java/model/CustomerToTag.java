/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class CustomerToTag {

      
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer ctt_id;
    private Integer customer_id;
    private Integer tag_id;
    
    // Getters/setters ------------------------------------------------
    public Integer getCTTID() { return this.ctt_id; }
    public Integer getCustomerID() { return this.customer_id; }
    public Integer getTagID() { return this.tag_id; }
    
    public void setCTTID(Integer value) { this.ctt_id = value; }
    public void setCustomerID(Integer value) { this.customer_id = value; }
    public void setTagID(Integer value) { this.tag_id = value; }
}
