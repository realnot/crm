/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class UserProfileView extends UserProfile {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private String status_name;
    private String role_name;
    private String lang_name;
    private String gender_name;
    private String country_name;
    private String region_name;
    private String city_name;
    
    // Getters/setters ------------------------------------------------
    
    public String getStatusName() { return status_name; }
    public String getRoleName() { return role_name; }
    public String getLangName() { return lang_name; }
    public String getGenderName() { return gender_name; }
    public String getCountryName() { return country_name; }
    public String getRegionName() { return region_name; }    
    public String getCityName() { return city_name; }
    
    public void setStatusName(String value) { this.status_name = value; }
    public void setRoleName(String value) { this.role_name = value; }
    public void setLangName(String value) { this.lang_name = value; }
    public void setGenderName(String value) { this.gender_name = value; }
    public void setCountryName(String value) { this.country_name = value; }
    public void setRegionName(String value) { this.region_name = value; }
    public void setCityName(String value) { this.city_name = value; } 
}
