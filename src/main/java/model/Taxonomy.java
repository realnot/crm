/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class Taxonomy {
 
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer tag_id;
    private String tag_name;
    private String tag_desc;
    
    // Getters/setters ------------------------------------------------
    public Integer getTagID() { return this.tag_id; }
    public String getTagName() { return this.tag_name; }
    public String getTagDesc() { return this.tag_desc; }
    
    public void setTagID(Integer value) { this.tag_id = value; }
    public void setTagName(String value) { this.tag_name = value; }
    public void setTagDesc(String value) { this.tag_desc = value; }
}
