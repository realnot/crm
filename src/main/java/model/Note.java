/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author realnot
 */
public class Note {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer note_id;
    private String note_topic;
    private String note_desc;
    private Date note_created_on;
    private Integer note_created_by;
    private Integer note_refered_to;
    
    // Getters/setters ------------------------------------------------
    public Integer getNoteID() { return this.note_id; }
    public String getNoteTopic() { return this.note_topic; }
    public String getNoteDesc() { return this.note_desc; }
    public Date getNoteCreatedOn() { return this.note_created_on; }
    public Integer getNoteCreatedBy() { return this.note_created_by; }
    public Integer getNoteReferedTo() { return this.note_refered_to; }
    
    public void setNoteID(Integer value) { this.note_id = value; }
    public void setNoteTopic(String value) { this.note_topic = value; }
    public void setNoteDesc(String value) { this.note_desc = value; }
    public void setNoteCreatedOn(Date value) { this.note_created_on = value; }
    public void setNoteCreatedBy(Integer value) { this.note_created_by = value; }
    public void setNoteReferedTo(Integer value) { this.note_refered_to = value; }
    
}
 