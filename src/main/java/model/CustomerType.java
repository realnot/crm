/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class CustomerType {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer customer_type_id;
    private String customer_type_name;
    private String customer_type_desc;
    
    // Getters/setters ------------------------------------------------
    public Integer getCustomerTypeID() { return this.customer_type_id; }
    public String getCustomerTypeName() { return this.customer_type_name; }
    public String getCustomerTypeDesc() { return this.customer_type_desc; }
    
    public void setCustomerTypeID(Integer value) { this.customer_type_id = value; }
    public void setCustomerTypeName(String value) { this.customer_type_name = value; }
    public void setCustomerTypeDesc(String value) { this.customer_type_desc = value; }
}
