/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author realnot
 */
public class ProductCategory {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer product_id;
    private String product_name;
    private String product_desc;
    
    // Getters/setters ------------------------------------------------
    public Integer getProductID() { return this.product_id; }
    public String getProductName() { return this.product_name; }
    public String getProductDesc() { return this.product_desc; }
    
    public void setProductID(Integer value) { this.product_id = value; }
    public void setProductName(String value) { this.product_name = value; }
    public void setProductDesc(String value) { this.product_desc = value; }
}
