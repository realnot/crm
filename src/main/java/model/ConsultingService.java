/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author realnot
 */
public class ConsultingService {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer service_id;
    private String service_name;
    private String service_desc;
    
    // Getters/setters ------------------------------------------------
    public Integer getServiceID() { return this.service_id; }
    public String getServiceName() { return this.service_name; }
    public String getServiceDesc() { return this.service_desc; }
    
    public void setServiceID(Integer value) { this.service_id = value; }
    public void setServiceName(String value) { this.service_name = value; }
    public void setServiceDesc(String value) { this.service_desc = value; }
}

