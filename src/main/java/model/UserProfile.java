/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author realnot
 */
public class UserProfile {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer user_id;
    private Integer role_id;
    private String username;
    private String password;
    private String email;
    private Integer lang_id;
    private String name;
    private String surname;
    private String display_name;
    private Integer gender_id;
    private Integer country_id;
    private Integer region_id;
    private Integer city_id;
    private String address;
    private String birthdate;
    private Timestamp created_on;
    private Timestamp last_login;
    private String role_name;
    
    // Getters/setters ------------------------------------------------
    
    public Integer getUserID() { return user_id; }
    public void setUserID(Integer value) { this.user_id = value; }
    
    public Integer getRoleID() { return role_id; }
    public void setRoleID(Integer value) { this.role_id = value; }
    
    public Integer getLangID() { return lang_id; }
    public void setLangID(Integer value) { this.lang_id = value; }
    
    public Integer getGenderID() { return gender_id; }
    public void setGenderID(Integer value) { this.gender_id = value; }
    
    public Integer getCountryID() { return country_id; }
    public void setCountryID(Integer value) { this.country_id = value; }
    
    public Integer getRegionID() { return region_id; }
    public void setRegionID(Integer value) { this.region_id = value; }
    
    public Integer getCityID() { return city_id; }
    public void setCityID(Integer value) { this.city_id = value; } 
    
    // The username can't be modified
    public String getUsername() { return username; }
    public void setUsername(String value) { this.username = value; }
    
    public String getPassword() { return password; }
    public void setPassword(String value) { this.password = value; }
    
    public String getEmail() { return email; }
    public void setEmail(String value) { this.email = value; }
    
    public String getName() { return name; }
    public void setName(String value) { this.name = value; }
    
    public String getSurname() {return surname; }
    public void setSurname(String value) { this.surname = value; }
    
    public String getDisplayName() { return display_name; }
    public void setDisplayName(String value) { this.display_name = value; }
    
    public String getAddress() { return address; }
    public void setAddress(String value) { this.address = value; }
    
    public String getBirthdate() { return birthdate; }
    public void setBirthdate(String value) { this.birthdate = value; }
    
    // The creation date of account can't be modified
    public Date getCreatedOn() { return created_on; }
    public void setCreatedOn(Timestamp value) { this.created_on = value; }
    
    // The last login date can't be modified
    public Date getLastLogin() { return last_login; }
    public void setLastLogin(Timestamp value) { this.last_login = value; } 
    
    public String getRoleName() { return role_name; }
    public void setRoleName(String value) { this.role_name = value; }
}
