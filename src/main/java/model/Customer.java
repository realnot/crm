/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author realnot
 */
public class Customer {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer customer_id;
    private String customer_name;
    private String customer_email;
    private Integer customer_type;
    private Integer customer_category;
    private Date customer_created_on;
    private Integer customer_created_by;
    private Integer customer_assigned_to;
    private String customer_contact_name;
    private String customer_contact_surname;
    private String customer_contact_address;
    private String customer_contact_phone;
    
    private String string_customer_created_by;
    private String string_customer_assigned_to;
    private String string_customer_type;
    private String string_customer_category;
    
    // Getters/setters ------------------------------------------------
    public Integer getCustomerID() { return this.customer_id; }
    public String getCustomerName() { return this.customer_name; }
    public String getCustomerEmail() { return this.customer_email; }
    public Integer getCustomerCategory() { return this.customer_category; }
    public Integer getCustomerType() { return this.customer_type; }
    public Date getCustomerCreatedOn() { return this.customer_created_on; }
    public Integer getCustomerCreatedBy() { return this.customer_created_by; }
    public Integer getCustomerAssignedTo() { return this.customer_assigned_to; }
    public String getCustomerContactName() { return this.customer_contact_name; }
    public String getCustomerContactSurname() { return this.customer_contact_surname; }
    public String getCustomerContactAddress() { return this.customer_contact_address; }
    public String getCustomerContactPhone() { return this.customer_contact_phone; }
    public String getStringCustomerCategory() { return this.string_customer_category; }
    public String getStringCustomerType() { return this.string_customer_type; }
    public String getStringCustomerCreatedBy() { return this.string_customer_created_by; }
    public String getStringCustomerAssignedTo() { return this.string_customer_assigned_to; }
    
    public void setCustomerID(Integer value) { this.customer_id = value; }
    public void setCustomerName(String value) { this.customer_name = value; }
    public void setCustomerEmail(String value) { this.customer_email = value; }
    public void setCustomerCategory(Integer value) { this.customer_category = value; }
    public void setCustomerType(Integer value) { this.customer_type = value; }
    public void setCustomerCreatedOn(Date value) { this.customer_created_on = value; }
    public void setCustomerCreatedBy(Integer value) { this.customer_created_by = value; }
    public void setCustomerAssignedTo(Integer value) { this.customer_assigned_to = value; }
    public void setCustomerContactName(String value) { this.customer_contact_name = value; }
    public void setCustomerContactSurname(String value) { this.customer_contact_surname = value; }
    public void setCustomerContactAddress(String value) { this.customer_contact_address = value; }
    public void setCustomerContactPhone(String value) { this.customer_contact_phone = value; }
    public void setStringCustomerCategory(String value) { this.string_customer_category = value; }
    public void setStringCustomerType(String value) { this.string_customer_type = value; }
    public void setStringCustomerCreatedBy(String value) { this.string_customer_created_by = value; }
    public void setStringCustomerAssignedTo(String value) { this.string_customer_assigned_to = value; }

    
}
