/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author realnot
 */
public class Role implements Serializable {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer role_id;
    private String role_name;
    private String role_desc;
    
    // Getters/setters ------------------------------------------------
    public Integer getRoleID() { return this.role_id; }
    public String getRoleName() { return this.role_name; }
    public String getRoleDesc() { return this.role_desc; }
    
    public void setRoleID(Integer value) { this.role_id = value; }
    public void setRoleName(String value) { this.role_name = value; }
    public void setRoleDesc(String value) { this.role_desc = value; }
}
