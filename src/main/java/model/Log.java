/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author realnot
 */
public class Log implements Serializable {
    
    // Constants ------------------------------------------------------
    private static final long serialVersionUID = 1L;
    
    // Properties -----------------------------------------------------
    private Integer log_id;
    private Integer log_refered_to;
    private String log_desc;
    private Date log_created_on;
    
    private String log_username;
    
    // Getters/setters ------------------------------------------------
    public int getLogID() { return this.log_id; }
    public int getReferedTo() { return this.log_refered_to; }
    public String getLogDesc() { return this.log_desc; }
    public Date getLogCreatedOn() { return this.log_created_on; }
    public String getLogUsername() { return this.log_username; }
    
    public void setLogID(Integer value) { this.log_id = value; }
    public void setReferedTo(Integer value) { this.log_refered_to = value; }
    public void setLogDesc(String value) { this.log_desc = value; }
    public void setLogCreatedOn(Date value) { this.log_created_on = value; }
    public void setLogUsername(String value) { this.log_username = value; }
}
