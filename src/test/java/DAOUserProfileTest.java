/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;

import dao.interfaces.UserProfileDAO;
import dao.interfaces.UserProfileViewDAO;
import dao.DAOFactory;
import model.UserProfile;
import model.UserProfileView;

/**
 *
 * @author realnot
 */
public class DAOUserProfileTest {
    
    public static void main(String args[]) {
        
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain UserProfileDAO.
        UserProfileDAO profileDAO = crm.getUserProfileDAO();
        System.out.println("UserProfileDAO successfully obtained: " + profileDAO);

        // Create a UserProfile (required fields)
        UserProfile profile1 = new UserProfile();
        profile1.setRoleID(2); // Administrator
        profile1.setUsername("realnot");
        profile1.setPassword("12345");
        profile1.setEmail("realnot@crm.com");
        profile1.setName("Mauro");
        profile1.setSurname("Crociara");
        profile1.setAddress("Via giacomini 54");
        profile1.setBirthdate("30-01-1989");
        profile1.setDisplayName("Mauro Crociara");
        profileDAO.create(profile1);
        System.out.println("UserProfile successfully created: " + profile1);
        
        // Create a UserProfile (required fields)
        UserProfile profile2 = new UserProfile();
        profile2.setRoleID(2); // Administrator
        profile2.setUsername("astharot");
        profile2.setPassword("12345");
        profile2.setEmail("astharot@crm.com");
        profile2.setName("Andrea");
        profile2.setSurname("Roncato");
        profile2.setAddress("Via amerigo 3b");
        profile2.setBirthdate("24-05-1978");
        profile2.setDisplayName("Andrea Roncato");
        profileDAO.create(profile2);
        System.out.println("UserProfile successfully created: " + profile2);
        
        // Update the UserProfile (with required and optional fields)
        profile1.setRoleID(1); // Registered
        profile1.setUsername("UserTestUpdated");
        profile1.setPassword("87654321");
        profile1.setEmail("usertestupdated@crm.com");
        // Optional fields
        profile1.setName("User");
        profile1.setSurname("Test");
        profile1.setAddress("Via Gaspari 80a");
        profile1.setBirthdate("1980-05-24");
        profile1.setDisplayName("UserTest Rocks");
        profileDAO.update(profile1);
        System.out.println("UserProfile successfully updated: " + profile1);
        
        // Check if profile exists.
        boolean exist_one = profileDAO.existProfile(profile1.getUserID());
        System.out.println("This profile should exist, "
                + "so this should print true: " + exist_one);
        
        // List all profiles.
        List<UserProfile> profiles = profileDAO.list();
        System.out.println("List of profiles successfully queried: " 
                + profiles);
        System.out.println("Thus, amount of profiles in database is: " 
                + profiles.size());

        // Delete the profiles
        profileDAO.delete(profile1);
        System.out.println("UserProfile successfully deleted: " + profile1);
        
        // Check if profile exists.
        boolean exist_two = profileDAO.existProfile(profile1.getUserID());
        System.out.println("This profile should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user profiles again
        profiles = profileDAO.list();
        System.out.println("List of profiles successfully queried: " + profiles);
        System.out.println("Thus, amount of profiles in database is: " 
                + profiles.size());
        
    }
}
