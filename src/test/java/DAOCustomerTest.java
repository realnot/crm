
import dao.DAOFactory;
import dao.interfaces.CustomerDAO;
import java.util.List;
import model.Customer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOCustomerTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain CustomerDAO.
        CustomerDAO customerDAO = crm.getCustomerDAO();
        System.out.println("CustomerDAO successfully obtained: " + customerDAO);

        // Create a Customer (required fields)
        Customer customer = new Customer();
        customer.setCustomerName("Cisco");
        customer.setCustomerEmail("contacts@cisco.com");
        customer.setCustomerCreatedBy(0); // Administrator
        customerDAO.create(customer);
        System.out.println("Customer successfully created: " + customer);
        
        // Update the Customer (with required and optional fields)
        customer.setCustomerName("Cisco Updated");
        customer.setCustomerEmail("constact_updated@cisco.com");
        customer.setCustomerCreatedBy(0); // Administrator
        customerDAO.update(customer);
        System.out.println("Customer successfully updated: " + customer);
        
        // Check if customer exists.
        boolean exist_one = customerDAO.existCustomer(customer.getCustomerName());
        System.out.println("This customer should exist, "
                + "so this should print true: " + exist_one);
        
        // List all customers.
        List<Customer> customers = customerDAO.list();
        System.out.println("List of customers successfully queried: " 
                + customers);
        System.out.println("Thus, amount of customers in database is: " 
                + customers.size());

        // Delete the customer
        customerDAO.delete(customer);
        System.out.println("Customer successfully deleted: " + customer);
        
        // Check if customer exists.
        boolean exist_two = customerDAO.existCustomer(customer.getCustomerName());
        System.out.println("This customer should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user customers again
        customers = customerDAO.list();
        System.out.println("List of customers successfully queried: " + customer);
        System.out.println("Thus, amount of customers in database is: " 
                + customers.size());
                
    }
}
