
import dao.DAOFactory;
import dao.interfaces.BusinessProposalDAO;
import java.util.List;
import model.BusinessProposal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOBusinessProposalTest {
    
    
    public static void main(String args[]) {
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain ProposalDAO.
        BusinessProposalDAO proposalDAO = crm.getBusinessProposalDAO();
        System.out.println("BusinessProposalDAO successfully obtained: " + proposalDAO);

        // Create a BusinessProposal (required fields)
        BusinessProposal proposal = new BusinessProposal();
        proposal.setBusinessPName("BusinessProposal Namre");
        proposal.setBusinessPDesc("BusinessProposal Desc");
        proposal.setConsultingService(0); // author
        proposalDAO.create(proposal);
        System.out.println("BusinessProposal successfully created: " + proposal);
        
        // Update the Proposal (with required and optional fields)
        proposal.setBusinessPName("BusinessProposal Name Updated");
        proposal.setBusinessPDesc("BusinessProposal Desc Updated");
        proposal.setConsultingService(0); // author
        proposalDAO.update(proposal);
        System.out.println("BusinessProposal successfully updated: " + proposal);
        
        // Check if proposal exists.
        boolean exist_one = proposalDAO.existBusinessP(proposal.getBusinessPName());
        System.out.println("This proposal should exist, "
                + "so this should print true: " + exist_one);
        
        // List all proposals.
        List<BusinessProposal> proposals = proposalDAO.list();
        System.out.println("List of proposals successfully queried: " 
                + proposals);
        System.out.println("Thus, amount of proposals in database is: " 
                + proposals.size());
        
        // Delete the proposal
        proposalDAO.delete(proposal);
        System.out.println("BusinessProposal successfully deleted: " + proposal);
        
        // Check if proposal exists.
        boolean exist_two = proposalDAO.existBusinessP(proposal.getBusinessPName());
        System.out.println("This proposal should not exist anymore, "
                + "so this should print false: " + exist_two);
        
        // List all proposals.
        proposals = proposalDAO.list();
        System.out.println("List of proposals successfully queried: " 
                + proposals);
        System.out.println("Thus, amount of proposals in database is: " 
                + proposals.size());
    }
}
