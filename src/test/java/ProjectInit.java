
import dao.DAOFactory;
import dao.interfaces.*;
import model.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author realnot
 */
public class ProjectInit {

    public static void main(String args[]) {

        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        /**
         *
         * ROLES
         *
         */
        RoleDAO roleDAO = crm.getRoleDAO();
        System.out.println("RoleDAO successfully obtained: " + roleDAO);

        // Create a Role (required fields)
        Role role1 = new Role();
        role1.setRoleName("regular");
        role1.setRoleDesc("A non registered user.");
        roleDAO.create(role1);
        System.out.println("Role successfully created: " + role1);

        Role role2 = new Role();
        role2.setRoleName("registered");
        role2.setRoleDesc("A registered user.");
        roleDAO.create(role2);
        System.out.println("Role successfully created: " + role2);

        Role role3 = new Role();
        role3.setRoleName("administrator");
        role3.setRoleDesc("The boss of the platform.");
        roleDAO.create(role3);
        System.out.println("Role successfully created: " + role3);

        /**
         *
         * USER PROFILE
         *
         */
        // Obtain UserProfileDAO.
        UserProfileDAO profileDAO = crm.getUserProfileDAO();
        System.out.println("UserProfileDAO successfully obtained: " + profileDAO);

        // Create a UserProfile (required fields)
        UserProfile profile1 = new UserProfile();
        profile1.setRoleID(2); // Administrator
        profile1.setUsername("realnot");
        profile1.setPassword("12345");
        profile1.setEmail("realnot@crm.com");
        profile1.setName("Mauro");
        profile1.setSurname("Crociara");
        profile1.setAddress("Via giacomini 54");
        profile1.setBirthdate("30-01-1989");
        profile1.setDisplayName("Mauro Crociara");
        profileDAO.create(profile1);
        System.out.println("UserProfile successfully created: " + profile1);

        // Create a UserProfile (required fields)
        UserProfile profile2 = new UserProfile();
        profile2.setRoleID(2); // Administrator
        profile2.setUsername("astharot");
        profile2.setPassword("12345");
        profile2.setEmail("astharot@crm.com");
        profile2.setName("Andrea");
        profile2.setSurname("Roncato");
        profile2.setAddress("Via amerigo 3b");
        profile2.setBirthdate("24-05-1978");
        profile2.setDisplayName("Andrea Roncato");
        profileDAO.create(profile2);
        System.out.println("UserProfile successfully created: " + profile2);

        /**
         *
         * CUSTOMERS
         *
         */
        CustomerDAO customerDAO = crm.getCustomerDAO();
        System.out.println("CustomerDAO successfully obtained: " + customerDAO);

        // Create a Customer (required fields)
        Customer customer1 = new Customer();
        customer1.setCustomerName("Cisco");
        customer1.setCustomerEmail("contacts@cisco.com");
        customer1.setCustomerCreatedBy(1); // Administrator
        customerDAO.create(customer1);
        System.out.println("Customer successfully created: " + customer1);

        Customer customer2 = new Customer();
        customer2.setCustomerName("Accenture");
        customer2.setCustomerEmail("contacts@accenture.com");
        customer2.setCustomerCreatedBy(1); // Administrator
        customerDAO.create(customer2);
        System.out.println("Customer successfully created: " + customer2);

        Customer customer3 = new Customer();
        customer3.setCustomerName("Microsoft");
        customer3.setCustomerEmail("contacts@microsoft.com");
        customer3.setCustomerCreatedBy(1); // Administrator
        customerDAO.create(customer3);
        System.out.println("Customer successfully created: " + customer3);

        Customer customer4 = new Customer();
        customer4.setCustomerName("San Paolo");
        customer4.setCustomerEmail("contacts@sanpaolo.com");
        customer4.setCustomerCreatedBy(1); // Administrator
        customerDAO.create(customer4);
        System.out.println("Customer successfully created: " + customer4);

        Customer customer5 = new Customer();
        customer5.setCustomerName("Unicredit");
        customer5.setCustomerEmail("contacts@unicredit.com");
        customer5.setCustomerCreatedBy(1); // Administrator
        customerDAO.create(customer5);
        System.out.println("Customer successfully created: " + customer5);

        /**
         *
         * NOTES
         *
         */
        // Obtain NoteDAO.
        NoteDAO noteDAO = crm.getNoteDAO();
        System.out.println("NoteDAO successfully obtained: " + noteDAO);

        // Create a Note (required fields)
        Note note1 = new Note();
        note1.setNoteTopic("Product changed ");
        note1.setNoteDesc("The product of the last meeting is changed.");
        note1.setNoteCreatedBy(1); // admin
        note1.setNoteReferedTo(2); // customer 0
        noteDAO.create(note1);
        System.out.println("Note successfully created: " + note1);

        // Create a Note (required fields)
        Note note2 = new Note();
        note2.setNoteTopic("Call Cisco Inc.");
        note2.setNoteDesc("Call the Cisco Inc. to organize a new meeting");
        note2.setNoteCreatedBy(1); // admin
        note2.setNoteReferedTo(1); // customer 0
        noteDAO.create(note2);
        System.out.println("Note successfully created: " + note2);

        // Create a Note (required fields)
        Note note3 = new Note();
        note3.setNoteTopic("Add another customer service");
        note3.setNoteDesc("The Customer want to enlarge the portfolio services");
        note3.setNoteCreatedBy(1); // admin
        note3.setNoteReferedTo(1); // customer 0
        noteDAO.create(note3);
        System.out.println("Note successfully created: " + note3);

        /**
         *
         * MEETINGS
         *
         */
        // Obtain MeetingDAO.
        MeetingDAO meetingDAO = crm.getMeetingDAO();
        System.out.println("MeetingDAO successfully obtained: " + meetingDAO);

        // Create five Meetings (required fields)
        // # 1
        Meeting meeting1 = new Meeting();
        meeting1.setMeetingTopic("Cisco Customers Plan");
        meeting1.setMeetingDesc("Meeting with Cisco to disccus about the"
                + "new company business plan");
        meeting1.setMeetingCreatedBy(1);
        meeting1.setMeetingReferedTo(1);
        meetingDAO.create(meeting1);
        System.out.println("Meeting successfully created: " + meeting1);

        // # 2
        Meeting meeting2 = new Meeting();
        meeting2.setMeetingTopic("Developer Team");
        meeting2.setMeetingDesc("Meeting with the CRM developer team to discuss"
                + "about a new project");
        meeting2.setMeetingCreatedBy(1);
        meeting2.setMeetingReferedTo(1);
        meetingDAO.create(meeting2);
        System.out.println("Meeting successfully created: " + meeting2);

        // # 3
        Meeting meeting3 = new Meeting();
        meeting3.setMeetingTopic("Marketing D. (EMEA campaign)");
        meeting3.setMeetingDesc("Meeting with the marketing team to disccuss"
                + "about the new CRM capaign in EMEA.");
        meeting3.setMeetingCreatedBy(1);
        meeting3.setMeetingReferedTo(1);
        meetingDAO.create(meeting3);
        System.out.println("Meeting successfully created: " + meeting3);

        // # 4
        Meeting meeting4 = new Meeting();
        meeting4.setMeetingTopic("Meeting with John Fisherman");
        meeting4.setMeetingDesc("Meeting with John Fisherman to discuss "
                + "about a possible fusion between the CRM and Accenture.");
        meeting4.setMeetingCreatedBy(1);
        meeting4.setMeetingReferedTo(1);
        meetingDAO.create(meeting4);
        System.out.println("Meeting successfully created: " + meeting4);

        // # 5
        Meeting meeting5 = new Meeting();
        meeting5.setMeetingTopic("The Verge Interview");
        meeting5.setMeetingDesc("Meeting with Robert Tresort to release an"
                + "inteview about the company story and it's future.");
        meeting5.setMeetingCreatedBy(1);
        meeting5.setMeetingReferedTo(1);
        meetingDAO.create(meeting5);
        System.out.println("Meeting successfully created: " + meeting5);

        /**
         *
         * CONSULTING SERVICES
         *
         */
        ConsultingServiceDAO serviceDAO = crm.getConsultingServiceDAO();
        System.out.println("ConsultingServiceDAO successfully obtained: " + serviceDAO);

        // Create a ConsultingService (required fields)
        ConsultingService service1 = new ConsultingService();
        service1.setServiceName("Strategy");
        service1.setServiceDesc("Tailored solutions that deliver results and "
                + "achieve sustained growth");
        serviceDAO.create(service1);
        System.out.println("ConsultingService successfully created: " + service1);

        // Create a ConsultingService (required fields)
        ConsultingService service2 = new ConsultingService();
        service2.setServiceName("Performance Improvement");
        service2.setServiceDesc("Enabling companies to grow revenue, improve "
                + "margins and reposition quickly");
        serviceDAO.create(service2);
        System.out.println("ConsultingService successfully created: " + service2);

        // Create a ConsultingService (required fields)
        ConsultingService service3 = new ConsultingService();
        service3.setServiceName("Organization");
        service3.setServiceDesc("Ensuring the entire organization is aligned and "
                + "set up to successfully deliver on the company's objectives");
        serviceDAO.create(service3);
        System.out.println("ConsultingService successfully created: " + service3);

        // Create a ConsultingService (required fields)
        ConsultingService service4 = new ConsultingService();
        service4.setServiceName("Advanced Analytics");
        service4.setServiceDesc("Strategy experience and analytical expertise "
                + "combine to enable decision making and create value");
        serviceDAO.create(service4);
        System.out.println("ConsultingService successfully created: " + service4);

        // Create a ConsultingService (required fields)
        ConsultingService service5 = new ConsultingService();
        service5.setServiceName("Information Technology");
        service5.setServiceDesc("Realizing the full potential of IT resources, "
                + "investments and assets");
        serviceDAO.create(service5);
        System.out.println("ConsultingService successfully created: " + service5);

        // Create a ConsultingService (required fields)
        ConsultingService service6 = new ConsultingService();
        service6.setServiceName("Full Potential Transformation");
        service6.setServiceDesc("A cross-functional effort to alter the "
                + "financial, operational and strategic trajectory of a business");
        serviceDAO.create(service6);
        System.out.println("ConsultingService successfully created: " + service6);

        // Create a ConsultingService (required fields)
        ConsultingService service7 = new ConsultingService();
        service7.setServiceName("Private Equity");
        service7.setServiceDesc("Advising investors across the entire investment "
                + "life cycle");
        serviceDAO.create(service7);
        System.out.println("ConsultingService successfully created: " + service7);

        // Create a ConsultingService (required fields)
        ConsultingService service8 = new ConsultingService();
        service8.setServiceName("Customer Strategy & Marketing");
        service8.setServiceDesc("In-depth customer insights combined with "
                + "economic and operational fundamentals");
        serviceDAO.create(service8);
        System.out.println("ConsultingService successfully created: " + service8);

        // Create a ConsultingService (required fields)
        ConsultingService service9 = new ConsultingService();
        service9.setServiceName("Mergers & Acquisitions");
        service9.setServiceDesc("An integrated approach linking acquisition "
                + "strategy, due diligence and merger integration");
        serviceDAO.create(service9);
        System.out.println("ConsultingService successfully created: " + service9);

        // Create a ConsultingService (required fields)
        ConsultingService service10 = new ConsultingService();
        service10.setServiceName("Digital");
        service10.setServiceDesc("Deliver on core strategy, delight customers and "
                + "operate smarter and faster");
        serviceDAO.create(service10);
        System.out.println("ConsultingService successfully created: " + service10);

        // Create a ConsultingService (required fields)
        ConsultingService service11 = new ConsultingService();
        service11.setServiceName("Results Delivery");
        service11.setServiceDesc("Predicting, measuring and managing risk "
                + "associated with change management");
        serviceDAO.create(service11);
        System.out.println("ConsultingService successfully created: " + service11);

        /**
         *
         * BUSINESS PROPOSALS.
         *
         */
        // Obtain BusinessProposalsDAO.
        BusinessProposalDAO proposalDAO = crm.getBusinessProposalDAO();
        System.out.println("BusinessProposalDAO successfully obtained: " + proposalDAO);

        // Create 4 BusinessProposal (required fields)
        BusinessProposal proposal1 = new BusinessProposal();
        proposal1.setBusinessPName("CRM Registered Partner");
        proposal1.setBusinessPDesc("You are new to CRM exclusive Value Proposition "
                + "and want to know more about our products and services? "
                + "You want to receive information on new releases and access "
                + "our exclusive campaign builder tool?\n "
                + "Then the Registered Partner category is meant for you. "
                + "Just register on the Portal by submitting the requested "
                + "information, accepting the T&Cs and completing the mandatory "
                + "trainings. You will be able to join the CRM Community in a "
                + "matter of days.");
        proposal1.setConsultingService(1); //
        proposalDAO.create(proposal1);
        System.out.println("BusinessProposal successfully created: " + proposal1);

        BusinessProposal proposal2 = new BusinessProposal();
        proposal2.setBusinessPName("CRM Certified Partner");
        proposal2.setBusinessPDesc("A CRM Certified Partner is a high capable "
                + "Solutions provider, often in a specific geography within "
                + "their country or with a specific vertical solution or "
                + "specialist area expertise. He is a recognized Technology "
                + "Specialist, assuring recognized and credible servers, storage "
                + "and client computing capabilities.\n Certified Partners do "
                + "not hold a direct contract with CRM and buy their CRM "
                + "portfolios from a CRM Authorised Distributor.");
        proposal2.setConsultingService(1); //
        proposalDAO.create(proposal2);
        System.out.println("BusinessProposal successfully created: " + proposal1);

        // Create 4 BusinessProposal (required fields)
        BusinessProposal proposal3 = new BusinessProposal();
        proposal3.setBusinessPName("CRM Certified Enterprise Architecture Partner");
        proposal3.setBusinessPDesc("Our CRM Certified Enterprise Architecture "
                + "Partners are high calibre corporate focused Systems Integrator "
                + "and Solutions Providers, typically in the Top 10 resellers in "
                + "the country they operate in. They prove to employ highly "
                + "skilled sales-force able to sell full range of CRM commercial "
                + "products, are experts in large projects, roll-outs and staging, "
                + "demonstrate server, storage and client computing capabilities, "
                + "trusted by large corporate & government customers. Audits will "
                + "be conducted to ensure capabilities are sustainable.");
        proposal3.setConsultingService(1); //
        proposalDAO.create(proposal3);
        System.out.println("BusinessProposal successfully created: " + proposal3);

        // Create 4 BusinessProposal (required fields)
        BusinessProposal proposal4 = new BusinessProposal();
        proposal4.setBusinessPName("CRM Authorized Distributor");
        proposal4.setBusinessPDesc("Authorised Distributors are highly capable "
                + "in logistics, channel sales & marketing, and technical support "
                + "for their resellers. They do not sell to end customers and "
                + "they are typically amongst the top 5 software distributors "
                + "in their country or region of operation. In addition, Authorised "
                + "Distributors have an exemplary credit & payment history and "
                + "are experts in inventory management.  ");
        proposal4.setConsultingService(1); //
        proposalDAO.create(proposal4);
        System.out.println("BusinessProposal successfully created: " + proposal4);

        /**
         *
         * USER PROFILE TO CUSTOMERS (each user can handle 0 - N customers.
         *
         */
        // Obtain UserProfileToCustomerDAO.
        UserProfileToCustomerDAO uptcDAO = crm.getUserProfileToCustomerDAO();
        System.out.println("UserProfileToCustomerDAO successfully obtained: " + uptcDAO);

        // Create a UserProfileToCustomer (map user 1 to customer 1)
        UserProfileToCustomer uptc1 = new UserProfileToCustomer();
        uptc1.setUserID(1);
        uptc1.setCustomerID(1);
        uptcDAO.create(uptc1);
        System.out.println("UserProfileToCustomer successfully created: " + uptc1);

        // Create a UserProfileToCustomer (map user 1 to customer 2)
        UserProfileToCustomer uptc2 = new UserProfileToCustomer();
        uptc2.setUserID(1);
        uptc2.setCustomerID(2);
        uptcDAO.create(uptc2);
        System.out.println("UserProfileToCustomer successfully created: " + uptc2);

        // Create a UserProfileToCustomer (ma user 1 to customer 3)
        UserProfileToCustomer uptc3 = new UserProfileToCustomer();
        uptc3.setUserID(1);
        uptc3.setCustomerID(3);
        uptcDAO.create(uptc3);
        System.out.println("UserProfileToCustomer successfully created: " + uptc3);

        /**
         *
         * CUSTOMER TO BUSINESS PROPOSALS (each customer has 0 - M bp).
         *
         */
        
        // Obtain CustomerToBusinessProposalDAO.
        CustomerToBusinessProposalDAO ctbpDAO = crm.getCustomerToBusinessProposalDAO();
        System.out.println("CustomerToBusinessProposalDAO successfully obtained: " + ctbpDAO);
        
        // Create a UserProfileToCuctbpDAOstomer (required fields)
        CustomerToBusinessProposal ctbp1 = new CustomerToBusinessProposal();
        ctbp1.setCustomerID(1);
        ctbp1.setProposalID(1);
        ctbpDAO.create(ctbp1);
        System.out.println("CustomerToBusinessProposal successfully created: " + ctbp1);
        
        // Create a UserProfileToCuctbpDAOstomer (required fields)
        CustomerToBusinessProposal ctbp2 = new CustomerToBusinessProposal();
        ctbp2.setCustomerID(1);
        ctbp2.setProposalID(2);
        ctbpDAO.create(ctbp2);
        System.out.println("CustomerToBusinessProposal successfully created: " + ctbp2);
        
        // Create a UserProfileToCuctbpDAOstomer (required fields)
        CustomerToBusinessProposal ctbp3 = new CustomerToBusinessProposal();
        ctbp3.setCustomerID(1);
        ctbp3.setProposalID(3);
        ctbpDAO.create(ctbp3);
        System.out.println("CustomerToBusinessProposal successfully created: " + ctbp3);
        
        /**
         * 
         * TAXONOMY.
         * 
         */
        // Obtain TaxonomyDAO.
        TaxonomyDAO tagDAO = crm.getTaxonomyDAO();
        System.out.println("TaxonomyDAO successfully obtained: " + tagDAO);

        // Create a Taxonomy (required fields)
        Taxonomy tag1 = new Taxonomy();
        tag1.setTagName("business");
        tagDAO.create(tag1);
        System.out.println("Taxonomy successfully created: " + tag1);
        
        Taxonomy tag2 = new Taxonomy();
        tag2.setTagName("IT");
        tagDAO.create(tag2);
        System.out.println("Taxonomy successfully created: " + tag2);
        
        Taxonomy tag3 = new Taxonomy();
        tag3.setTagName("networking");
        tagDAO.create(tag3);
        System.out.println("Taxonomy successfully created: " + tag3);
        
        Taxonomy tag4 = new Taxonomy();
        tag4.setTagName("comunication");
        tagDAO.create(tag4);
        System.out.println("Taxonomy successfully created: " + tag4);
        
        Taxonomy tag5 = new Taxonomy();
        tag5.setTagName("bank");
        tagDAO.create(tag5);
        System.out.println("Taxonomy successfully created: " + tag5);
        
        Taxonomy tag6 = new Taxonomy();
        tag6.setTagName("credit");
        tagDAO.create(tag6);
        System.out.println("Taxonomy successfully created: " + tag6);
        
        Taxonomy tag7 = new Taxonomy();
        tag7.setTagName("financial");
        tagDAO.create(tag7);
        System.out.println("Taxonomy successfully created: " + tag7);
        
        Taxonomy tag8 = new Taxonomy();
        tag8.setTagName("consulting");
        tagDAO.create(tag8);
        System.out.println("Taxonomy successfully created: " + tag8);
        
        Taxonomy tag9 = new Taxonomy();
        tag9.setTagName("windows");
        tagDAO.create(tag9);
        System.out.println("Taxonomy successfully created: " + tag9);
        
        Taxonomy tag10 = new Taxonomy();
        tag10.setTagName("outsourcing");
        tagDAO.create(tag10);
        System.out.println("Taxonomy successfully created: " + tag10);
        
        /**
         * 
         * CUSTOMER TO TAG.
         * 
         */
        // Obtain CustomerToTagDAO.
        CustomerToTagDAO cttDAO = crm.getCustomerToTagDAO();
        System.out.println("CustomerToTagDAO successfully obtained: " + cttDAO);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt1 = new CustomerToTag();
        ctt1.setCustomerID(1);
        ctt1.setTagID(2);
        cttDAO.create(ctt1);
        System.out.println("CustomerToTag successfully created: " + ctt1);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt2 = new CustomerToTag();
        ctt2.setCustomerID(1);
        ctt2.setTagID(3);
        cttDAO.create(ctt2);
        System.out.println("CustomerToTag successfully created: " + ctt2);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt3 = new CustomerToTag();
        ctt3.setCustomerID(1);
        ctt3.setTagID(4);
        cttDAO.create(ctt3);
        System.out.println("CustomerToTag successfully created: " + ctt3);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt4 = new CustomerToTag();
        ctt4.setCustomerID(2);
        ctt4.setTagID(1);
        cttDAO.create(ctt4);
        System.out.println("CustomerToTag successfully created: " + ctt4);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt5 = new CustomerToTag();
        ctt5.setCustomerID(2);
        ctt5.setTagID(8);
        cttDAO.create(ctt5);
        System.out.println("CustomerToTag successfully created: " + ctt5);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt6 = new CustomerToTag();
        ctt6.setCustomerID(2);
        ctt6.setTagID(10);
        cttDAO.create(ctt6);
        System.out.println("CustomerToTag successfully created: " + ctt6);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt7 = new CustomerToTag();
        ctt7.setCustomerID(3);
        ctt7.setTagID(1);
        cttDAO.create(ctt7);
        System.out.println("CustomerToTag successfully created: " + ctt7);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt8 = new CustomerToTag();
        ctt8.setCustomerID(3);
        ctt8.setTagID(2);
        cttDAO.create(ctt8);
        System.out.println("CustomerToTag successfully created: " + ctt8);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt9 = new CustomerToTag();
        ctt9.setCustomerID(3);
        ctt9.setTagID(9);
        cttDAO.create(ctt9);
        System.out.println("CustomerToTag successfully created: " + ctt9);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt10 = new CustomerToTag();
        ctt10.setCustomerID(4);
        ctt10.setTagID(1);
        cttDAO.create(ctt10);
        System.out.println("CustomerToTag successfully created: " + ctt10);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt11 = new CustomerToTag();
        ctt11.setCustomerID(4);
        ctt11.setTagID(5);
        cttDAO.create(ctt11);
        System.out.println("CustomerToTag successfully created: " + ctt11);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt12 = new CustomerToTag();
        ctt12.setCustomerID(4);
        ctt12.setTagID(6);
        cttDAO.create(ctt12);
        System.out.println("CustomerToTag successfully created: " + ctt12);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt13 = new CustomerToTag();
        ctt13.setCustomerID(4);
        ctt13.setTagID(7);
        cttDAO.create(ctt13);
        System.out.println("CustomerToTag successfully created: " + ctt13);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt14 = new CustomerToTag();
        ctt14.setCustomerID(5);
        ctt14.setTagID(1);
        cttDAO.create(ctt14);
        System.out.println("CustomerToTag successfully created: " + ctt14);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt15 = new CustomerToTag();
        ctt15.setCustomerID(5);
        ctt15.setTagID(5);
        cttDAO.create(ctt15);
        System.out.println("CustomerToTag successfully created: " + ctt15);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt16 = new CustomerToTag();
        ctt16.setCustomerID(5);
        ctt16.setTagID(6);
        cttDAO.create(ctt16);
        System.out.println("CustomerToTag successfully created: " + ctt16);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt17 = new CustomerToTag();
        ctt17.setCustomerID(5);
        ctt17.setTagID(7);
        cttDAO.create(ctt17);
        System.out.println("CustomerToTag successfully created: " + ctt17);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt18 = new CustomerToTag();
        ctt18.setCustomerID(5);
        ctt18.setTagID(10);
        cttDAO.create(ctt18);
        System.out.println("CustomerToTag successfully created: " + ctt18);
        
    }
}
