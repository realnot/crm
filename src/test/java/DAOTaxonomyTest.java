
import dao.DAOFactory;
import dao.interfaces.TaxonomyDAO;
import java.util.List;
import model.Taxonomy;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOTaxonomyTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain TaxonomyDAO.
        TaxonomyDAO tagDAO = crm.getTaxonomyDAO();
        System.out.println("TaxonomyDAO successfully obtained: " + tagDAO);

        // Create a Taxonomy (required fields)
        Taxonomy tag = new Taxonomy();
        tag.setTagName("test_name");
        tag.setTagDesc("the_tag_desc");
        tagDAO.create(tag);
        System.out.println("Taxonomy successfully created: " + tag);
        
        // Update the Taxonomy (with required and optional fields)
        tag.setTagName("test_name_updated");
        tag.setTagDesc("the_tag_desc_updated");
        tagDAO.update(tag);
        System.out.println("Taxonomy successfully updated: " + tag);
        
        // Check if tag exists.
        boolean exist_one = tagDAO.existTaxonomy(tag.getTagName());
        System.out.println("This tag should exist, "
                + "so this should print true: " + exist_one);
        
        // List all tags.
        List<Taxonomy> tags = tagDAO.list();
        System.out.println("List of tags successfully queried: " 
                + tags);
        System.out.println("Thus, amount of tags in database is: " 
                + tags.size());

        // Delete the tag
        tagDAO.delete(tag);
        System.out.println("Taxonomy successfully deleted: " + tag);
        
        // Check if tag exists.
        boolean exist_two = tagDAO.existTaxonomy(tag.getTagName());
        System.out.println("This tag should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user tags again
        tags = tagDAO.list();
        System.out.println("List of tags successfully queried: " + tag);
        System.out.println("Thus, amount of tags in database is: " 
                + tags.size());
    }
}
