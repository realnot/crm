
import dao.DAOFactory;
import dao.interfaces.CustomerToTagDAO;
import java.util.List;
import model.CustomerToTag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOCustomerToTagTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);
        
        // Obtain CustomerToTagDAO.
        CustomerToTagDAO cttDAO = crm.getCustomerToTagDAO();
        System.out.println("CustomerToTagDAO successfully obtained: " + cttDAO);
        
        // Create a CustomerToTag (required fields)
        CustomerToTag ctt = new CustomerToTag();
        ctt.setCustomerID(1);
        ctt.setTagID(1);
        cttDAO.create(ctt);
        System.out.println("CustomerToTag successfully created: " + ctt);
        
        // Update the CustomerToTag (with required and optional fields)
        ctt.setCustomerID(1);
        ctt.setTagID(1);
        cttDAO.update(ctt);
        System.out.println("CustomerToTag successfully updated: " + ctt);
        
        // List all ctts.
        List<CustomerToTag> ctts = cttDAO.list();
        System.out.println("List of ctts successfully queried: " 
                + ctts);
        System.out.println("Thus, amount of ctts in database is: " 
                + ctts.size());

        // Delete the ctt
        cttDAO.delete(ctt);
        System.out.println("CustomerToTag successfully deleted: " + ctt);
        
        // List all ctts again
        ctts = cttDAO.list();
        System.out.println("List of ctts successfully queried: " + ctt);
        System.out.println("Thus, amount of ctts in database is: " 
                + ctts.size());
    }
}
