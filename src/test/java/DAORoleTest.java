
import dao.DAOFactory;
import dao.interfaces.RoleDAO;
import java.util.List;
import model.Role;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAORoleTest {

    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain RoleDAO.
        RoleDAO roleDAO = crm.getRoleDAO();
        System.out.println("RoleDAO successfully obtained: " + roleDAO);

        // Create a Role (required fields)
        Role role = new Role();
        role.setRoleName("test_name");
        role.setRoleDesc("the_role_desc");
        roleDAO.create(role);
        System.out.println("Role successfully created: " + role);
        
        // Update the Role (with required and optional fields)
        role.setRoleName("test_name_updated");
        role.setRoleDesc("the_role_desc_updated");
        roleDAO.update(role);
        System.out.println("Role successfully updated: " + role);
        
        // Check if role exists.
        boolean exist_one = roleDAO.existRole(role.getRoleName());
        System.out.println("This role should exist, "
                + "so this should print true: " + exist_one);
        
        // List all roles.
        List<Role> roles = roleDAO.list();
        System.out.println("List of roles successfully queried: " 
                + roles);
        System.out.println("Thus, amount of roles in database is: " 
                + roles.size());

        // Delete the role
        roleDAO.delete(role);
        System.out.println("Role successfully deleted: " + role);
        
        // Check if role exists.
        boolean exist_two = roleDAO.existRole(role.getRoleName());
        System.out.println("This role should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user roles again
        roles = roleDAO.list();
        System.out.println("List of roles successfully queried: " + role);
        System.out.println("Thus, amount of roles in database is: " 
                + roles.size());
    }
}