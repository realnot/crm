import java.util.List;

import dao.interfaces.UserGenderDAO;
import dao.DAOFactory;
import model.UserGender;

/*
 * @author realnot
 */
public class DAOUserGenderTest {

    public static void main(String[] args) throws Exception {
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain UserGenderDAO.
        UserGenderDAO genderDAO = crm.getUserGenderDAO();
        System.out.println("UserGenderDAO successfully obtained: " + genderDAO);

        // Create gender.
        UserGender gender = new UserGender();
        gender.setName("male");
        genderDAO.create(gender);
        System.out.println("Gender successfully created: " + gender);

        // Create another gender.
        UserGender anotherGender = new UserGender();
        anotherGender.setName("female");
        genderDAO.create(anotherGender);
        System.out.println("Another gender successfully created: " + anotherGender);

        // Update gender.
        gender.setName("other1");
        genderDAO.update(gender);
        System.out.println("Gender successfully updated: " + gender);

        // Update gender.
        gender.setName("other2");
        genderDAO.update(gender);
        System.out.println("Gender successfully updated: " + gender);

        // List all genders.
        List<UserGender> genders = genderDAO.list();
        System.out.println("List of genders successfully queried: " + genders);
        System.out.println("Thus, amount of genders in database is: " + genders.size());

        // Delete gender.
        genderDAO.delete(gender);
        System.out.println("Gender successfully deleted: " + gender);

        // Check if gender exists.
        boolean exist = genderDAO.existGender("male");
        System.out.println("This gender should not exist anymore, so this should print false: " + exist);
        
        // Delete another gender.
        genderDAO.delete(anotherGender);
        System.out.println("Another gender successfully deleted: " + anotherGender);

        // List all genders again.
        genders = genderDAO.list();
        System.out.println("List of genders successfully queried: " + genders);
        System.out.println("Thus, amount of genders in database is: " + genders.size());
    }
}