
import dao.DAOFactory;
import dao.interfaces.MeetingDAO;
import java.util.List;
import model.Meeting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOMeetingTest {
    
    public static void main(String args[]) {
        
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain RoleDAO.
        MeetingDAO meetingDAO = crm.getMeetingDAO();
        System.out.println("MeetingDAO successfully obtained: " + meetingDAO);

        // Create a Meeting (required fields)
        Meeting meeting = new Meeting();
        meeting.setMeetingTopic("Meeting Topic");
        meeting.setMeetingDesc("Meeting Desc");
        meeting.setMeetingCreatedBy(0); // author
        meeting.setMeetingReferedTo(1); 
        meetingDAO.create(meeting);
        System.out.println("Meeting successfully created: " + meeting);
        
        // Update the Role (with required and optional fields)
        meeting.setMeetingTopic("Meeting Topic Updated");
        meeting.setMeetingDesc("Meeting Desc Updated");
        meeting.setMeetingCreatedBy(0); // author
        meeting.setMeetingReferedTo(1); 
        meetingDAO.update(meeting);
        System.out.println("Meeting successfully updated: " + meeting);
        
        // Check if meeting exists.
        boolean exist_one = meetingDAO.existMeeting(meeting.getMeetingTopic());
        System.out.println("This meeting should exist, "
                + "so this should print true: " + exist_one);
        
        // List all meetings.
        List<Meeting> meetings = meetingDAO.list();
        System.out.println("List of meetings successfully queried: " 
                + meetings);
        System.out.println("Thus, amount of meetings in database is: " 
                + meetings.size());
        
        // Delete the meeting
        meetingDAO.delete(meeting);
        System.out.println("Meeting successfully deleted: " + meeting);
        
        // Check if meeting exists.
        boolean exist_two = meetingDAO.existMeeting(meeting.getMeetingTopic());
        System.out.println("This meeting should not exist anymore, "
                + "so this should print false: " + exist_two);
        
        // List all meetings.
        meetings = meetingDAO.list();
        System.out.println("List of meetings successfully queried: " 
                + meetings);
        System.out.println("Thus, amount of meetings in database is: " 
                + meetings.size());
    }
}
