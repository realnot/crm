
import dao.DAOFactory;
import dao.interfaces.NoteDAO;
import java.util.List;
import model.Note;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAONoteTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain NoteDAO.
        NoteDAO noteDAO = crm.getNoteDAO();
        System.out.println("NoteDAO successfully obtained: " + noteDAO);

        // Create a Note (required fields)
        Note note = new Note();
        note.setNoteTopic("test_name");
        note.setNoteDesc("the_note_desc");
        note.setNoteCreatedBy(0); // admin
        note.setNoteReferedTo(0); // customer 0
        noteDAO.create(note);
        System.out.println("Note successfully created: " + note);
        
        // Update the Note (with required and optional fields)
        note.setNoteTopic("test_name_updated");
        note.setNoteDesc("the_note_desc_updated");
        note.setNoteCreatedBy(0); // user 0
        note.setNoteReferedTo(0); // customer 0
        noteDAO.update(note);
        System.out.println("Note successfully updated: " + note);
        
        // Check if note exists.
        boolean exist_one = noteDAO.existNote(note.getNoteID());
        System.out.println("This note should exist, "
                + "so this should print true: " + exist_one);
        
        // List all notes.
        List<Note> notes = noteDAO.list();
        System.out.println("List of notes successfully queried: " 
                + notes);
        System.out.println("Thus, amount of notes in database is: " 
                + notes.size());

        // Delete the note
        noteDAO.delete(note);
        System.out.println("Note successfully deleted: " + note);
        
        // Check if note exists.
        boolean exist_two = noteDAO.existNote(note.getNoteID());
        System.out.println("This note should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user notes again
        notes = noteDAO.list();
        System.out.println("List of notes successfully queried: " + note);
        System.out.println("Thus, amount of notes in database is: " 
                + notes.size());
    }
}
