
import dao.DAOFactory;
import dao.interfaces.CustomerToBusinessProposalDAO;
import java.util.List;
import model.CustomerToBusinessProposal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOCustomerToBusinessProposalTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);
        
        // Obtain CustomerToBusinessProposalDAO.
        CustomerToBusinessProposalDAO ctbpDAO = crm.getCustomerToBusinessProposalDAO();
        System.out.println("CustomerToBusinessProposalDAO successfully obtained: " + ctbpDAO);
        
        // Create a UserProfileToCuctbpDAOstomer (required fields)
        CustomerToBusinessProposal ctbp = new CustomerToBusinessProposal();
        ctbp.setCustomerID(1);
        ctbp.setProposalID(1);
        ctbpDAO.create(ctbp);
        System.out.println("CustomerToBusinessProposal successfully created: " + ctbp);
        
        // Update the CustomerToBusinessProposal (with required and optional fields)
        ctbp.setCustomerID(1);
        ctbp.setProposalID(1);
        ctbpDAO.update(ctbp);
        System.out.println("CustomerToBusinessProposal successfully updated: " + ctbp);
        
        // Check if ctbp exists.
        boolean exist_one = ctbpDAO.existCTBP(1);
        System.out.println("This ctbp should exist, "
                + "so this should print true: " + exist_one);
        
        // List all ctbps.
        List<CustomerToBusinessProposal> ctbps = ctbpDAO.list();
        System.out.println("List of ctbps successfully queried: " 
                + ctbps);
        System.out.println("Thus, amount of ctbps in database is: " 
                + ctbps.size());

        // Delete the ctbp
        ctbpDAO.delete(ctbp);
        System.out.println("CustomerToBusinessProposal successfully deleted: " + ctbp);
        
        // Check if ctbp exists.
        boolean exist_two = ctbpDAO.existCTBP(1);
        System.out.println("This ctbp should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user ctbps again
        ctbps = ctbpDAO.list();
        System.out.println("List of ctbps successfully queried: " + ctbp);
        System.out.println("Thus, amount of ctbps in database is: " 
                + ctbps.size());
    }
}
