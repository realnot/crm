
import dao.DAOFactory;
import dao.interfaces.UserProfileToCustomerDAO;
import java.util.List;
import model.UserProfileToCustomer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOUserProfileToCustomerTest {
    
    public static void main(String args[]) {    
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);
        
        // Obtain UserProfileToCustomerDAO.
        UserProfileToCustomerDAO uptcDAO = crm.getUserProfileToCustomerDAO();
        System.out.println("UserProfileToCustomerDAO successfully obtained: " + uptcDAO);
        
        // Create a UserProfileToCuuptcDAOstomer (required fields)
        UserProfileToCustomer uptc = new UserProfileToCustomer();
        uptc.setUserID(1);
        uptc.setCustomerID(1);
        uptcDAO.create(uptc);
        System.out.println("UserProfileToCustomer successfully created: " + uptc);
        
        // Update the UserProfileToCustomer (with required and optional fields)
        uptc.setUserID(1);
        uptc.setCustomerID(1);
        uptcDAO.update(uptc);
        System.out.println("UserProfileToCustomer successfully updated: " + uptc);
        
        // Check if uptc exists.
        boolean exist_one = uptcDAO.existUPTC(1);
        System.out.println("This uptc should exist, "
                + "so this should print true: " + exist_one);
        
        // List all uptcs.
        List<UserProfileToCustomer> uptcs = uptcDAO.list();
        System.out.println("List of uptcs successfully queried: " 
                + uptcs);
        System.out.println("Thus, amount of uptcs in database is: " 
                + uptcs.size());

        // Delete the uptc
        uptcDAO.delete(uptc);
        System.out.println("UserProfileToCustomer successfully deleted: " + uptc);
        
        // Check if uptc exists.
        boolean exist_two = uptcDAO.existUPTC(1);
        System.out.println("This uptc should not exist anymore, "
                + "so this should print false: " + exist_two);

        // List all user uptcs again
        uptcs = uptcDAO.list();
        System.out.println("List of uptcs successfully queried: " + uptc);
        System.out.println("Thus, amount of uptcs in database is: " 
                + uptcs.size());
    }
}
