
import dao.DAOFactory;
import dao.interfaces.ConsultingServiceDAO;
import java.util.List;
import model.ConsultingService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author realnot
 */
public class DAOConsultingServiceTest {
    
    public static void main(String args[]) {
    
        // Obtain DAOFactory.
        DAOFactory crm = DAOFactory.getInstance("crm.jdbc");
        System.out.println("DAOFactory successfully obtained: " + crm);

        // Obtain ProposalDAO.
        ConsultingServiceDAO serviceDAO = crm.getConsultingServiceDAO();
        System.out.println("ConsultingServiceDAO successfully obtained: " + serviceDAO);

        // Create a ConsultingService (required fields)
        ConsultingService service = new ConsultingService();
        service.setServiceName("ConsultingService Name");
        service.setServiceDesc("ConsultingService Desc");
        serviceDAO.create(service);
        System.out.println("ConsultingService successfully created: " + service);
        
        // Update the Proposal (with required and optional fields)
        service.setServiceName("ConsultingService Name Updated");
        service.setServiceDesc("ConsultingService Desc Updated");
        serviceDAO.update(service);
        System.out.println("ConsultingService successfully updated: " + service);
        
        // Check if service exists.
        boolean exist_one = serviceDAO.existConsultingService(service.getServiceName());
        System.out.println("This service should exist, "
                + "so this should print true: " + exist_one);
        
        // List all services.
        List<ConsultingService> services = serviceDAO.list();
        System.out.println("List of services successfully queried: " 
                + services);
        System.out.println("Thus, amount of services in database is: " 
                + services.size());
        
        // Delete the service
        serviceDAO.delete(service);
        System.out.println("ConsultingService successfully deleted: " + service);
        
        // Check if service exists.
        boolean exist_two = serviceDAO.existConsultingService(service.getServiceName());
        System.out.println("This service should not exist anymore, "
                + "so this should print false: " + exist_two);
        
        // List all services.
        services = serviceDAO.list();
        System.out.println("List of services successfully queried: " 
                + services);
        System.out.println("Thus, amount of services in database is: " 
                + services.size());
    }
}
